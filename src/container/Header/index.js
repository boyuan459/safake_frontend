import React, { Component } from 'react';
import { Icon, Select, Spin } from 'antd';
import { Link } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import IntlMessage from '../../component/utility/intlMessage';
import { connect } from 'react-redux';
import * as searchActions from '../../redux/search/actions';
import { citySearch } from '../../services/geolocation';
import debounce from 'lodash/debounce';
import Dialog from '@material-ui/core/Dialog';
import MUIButton from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Radio from '@material-ui/core/Radio';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import PersonIcon from '@material-ui/icons/Person';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Divider from '@material-ui/core/Divider';
import SortIcon from '@material-ui/icons/SwapVert';
import CreateIcon from '@material-ui/icons/Create';
import ListIcon from '@material-ui/icons/List';
import ExitIcon from '@material-ui/icons/ExitToApp';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Avatar from '@material-ui/core/Avatar';
import classNames from 'classnames';

const Option = Select.Option;

const styles = theme => ({
    root: {
        flexGrow: 1,
        position: 'fixed',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 1030,
    },
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 5,
    },
    list: {
        width: 200,
    },
    header: {
        minHeight: 56,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        margin: 0,
    },
    bigAvatar: {
        width: 56,
        height: 56,
    }
});

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cities: [],
            city: props.city,
            collapsed: true,
            fetching: false,
            sortOpen: false,
            sort: props.sort,
            drawerOpen: false,
        };
        this.lastFetchId = 0;
        this.fetchCity = debounce(this.fetchCity, 300);
    }

    onToggle = collapsed => {
        console.log(collapsed);
        this.setState({
            collapsed: !collapsed
        });
    }

    handleLogout = event => {
        event.preventDefault();
        const { logout } = this.props;
        this.onToggle(false);
        logout();
    }

    fetchCity = value => {
        if (!value) {
            return;
        }
        this.lastFetchId += 1;
        const fetchId = this.lastFetchId;
        this.setState({
            cities: [],
            fetching: true
        });
        citySearch(value)
            .then(result => {
                console.log(result);
                if (fetchId !== this.lastFetchId) {
                    return;
                }
                this.setState({
                    cities: result,
                    fetching: false
                })
            }).catch(error => {
                console.log(error);
                this.setState({
                    cities: [],
                    fetching: false
                })
            });

    }

    handleCityChange = value => {
        console.log(value);
        if (!value) {
            value = {
                key: null,
                label: null
            }
        }
        const { cityChange } = this.props;
        this.setState({
            city: value,
            cities: [],
            fetching: false
        }, () => cityChange(value));
    }

    handleSort = event => {
        event.preventDefault();
        this.onToggle(false);
        this.setState({
            sortOpen: true
        });
    }

    handleSortClose = () => {
        const { sort } = this.props.search;
        this.setState({ sortOpen: false, sort: sort });
    }

    handleSortSave = () => {
        const { sortChange } = this.props;
        this.setState({ sortOpen: false }, () => sortChange(this.state.sort));
    }

    handleSortChange = event => {
        console.log(event.target.value);
        this.setState({ sort: event.target.value });
    }

    handleSortChange2 = value => {
        this.setState({ sort: value });
    }

    toggleDrawer = (open) => {
        this.setState({
            drawerOpen: open
        });
    }

    handleDrawerOpen = () => {
        this.setState({
            drawerOpen: true
        })
    }

    render() {
        const { auth, app } = this.props;
        const isLoggedIn = !!auth.oauth;
        const { city, collapsed, fetching, cities, sortOpen, sort, addOpen, drawerOpen } = this.state;
        const { intl, classes } = this.props;
        const suffix = city ? <Icon type="close-circle" /> : null;
        return (
            <div className={classes.root}>
                {
                    auth.tab === "me" ?
                        null
                        :
                        <AppBar position="static">
                            <Toolbar>
                                <IconButton className={classes.menuButton} color="inherit" aria-label="Menu"
                                    onClick={this.handleDrawerOpen}>
                                    <MenuIcon />
                                </IconButton>
                                <Select
                                    allowClear={city&&city.key ? true : false}
                                    showSearch
                                    labelInValue
                                    size="large"
                                    showArrow={false}
                                    placeholder={intl.formatMessage({ id: "topbar.city" })}
                                    notFoundContent={fetching ? <Spin size="small" /> : null}
                                    filterOption={false}
                                    onChange={this.handleCityChange}
                                    onSearch={this.fetchCity}
                                    style={{ width: '100%', paddingRight: 5 }}
                                >
                                    {
                                        cities.map(city => <Option key={city.value}>{city.label}</Option>)
                                    }
                                </Select>
                            </Toolbar>
                        </AppBar>
                }
                <SwipeableDrawer 
                    open={drawerOpen}
                    onClose={() => this.toggleDrawer(false)}
                    onOpen={() => this.toggleDrawer(true)}
                    >
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={() => this.toggleDrawer(false)}
                        onKeyDown={() => this.toggleDrawer(false)}>
                        <div className={classes.header}>
                        {
                            auth.user && auth.user.avatar ? 
                            <Avatar alt="test" src={auth.user.avatar} className={classNames(classes.avatar)} />
                            :intl.formatMessage({ id: "app.name" })
                        }
                            
                        </div>
                        <Divider />
                        <div className={classes.list}>
                            <List>
                                <ListItem button onClick={this.handleSort}>
                                    <ListItemIcon>
                                        <SortIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Sort" />
                                </ListItem>
                                <ListItem button component={Link} to="/dashboard/publish">
                                    <ListItemIcon>
                                        <CreateIcon />
                                    </ListItemIcon>
                                    <ListItemText primary={<IntlMessage id="topbar.host" />} />
                                </ListItem>
                                {
                                    isLoggedIn ? 
                                    <ListItem button component={Link} to="/dashboard/hosts">
                                        <ListItemIcon>
                                            <ListIcon />
                                        </ListItemIcon>
                                        <ListItemText primary={<IntlMessage id="topbar.hosts" />} />
                                    </ListItem>
                                    : null
                                }
                                {
                                    isLoggedIn ? 
                                    <ListItem button component={Link} to="/dashboard/myfavorite">
                                        <ListItemIcon>
                                            <FavoriteIcon color="secondary" />
                                        </ListItemIcon>
                                        <ListItemText primary={<IntlMessage id="topbar.myfavorite" />} />
                                    </ListItem>
                                    : null
                                }
                            </List>
                            <Divider/>
                            <List>
                                {
                                    isLoggedIn ? 
                                    <ListItem button onClick={this.handleLogout}>
                                        <ListItemIcon>
                                            <ExitIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Logout" />
                                    </ListItem>
                                    :
                                    <React.Fragment>
                                    <ListItem button component={Link} to="/signup">
                                        <ListItemIcon>
                                            <PersonAddIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Sign Up" />
                                    </ListItem>
                                    <ListItem button component={Link} to="/login">
                                        <ListItemIcon>
                                            <PersonIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Login" />
                                    </ListItem>
                                    </React.Fragment>
                                }
                            </List>
                        </div>
                    </div>
                </SwipeableDrawer>
                <Dialog
                    fullScreen
                    open={sortOpen}
                    onClose={this.handleSortClose}
                    TransitionComponent={Transition}>
                    <AppBar className={classes.appBar}>
                        <Toolbar>
                            <IconButton color="inherit" onClick={this.handleSortClose}>
                                <CloseIcon />
                            </IconButton>
                            <Typography variant="title" color="inherit" className={classes.flex}>
                                Sort
                            </Typography>
                            <MUIButton color="inherit" onClick={this.handleSortSave}>
                                Save
                            </MUIButton>
                        </Toolbar>
                    </AppBar>

                    <List>
                        <ListItem
                            onClick={() => this.handleSortChange2("price-desc")}
                            role={undefined}
                            button
                            key="price-desc">
                            <Radio value="price-desc" checked={sort === "price-desc" ? true : false} onChange={this.handleSortChange} />
                            <ListItemText primary={`Price (High - Low)`} />
                        </ListItem>
                        <ListItem
                            onClick={() => this.handleSortChange2("price-asc")}
                            button
                            role={undefined}
                            key="price-asc">
                            <Radio value="price-asc" checked={sort === "price-asc" ? true : false} onChange={this.handleSortChange} />
                            <ListItemText primary={`Price (Low - High)`} />
                        </ListItem>
                        <ListItem
                            onClick={() => this.handleSortChange2("date-desc")}
                            button
                            role={undefined}
                            key="date-desc">
                            <Radio value="date-desc" checked={sort === "date-desc" ? true : false} onChange={this.handleSortChange} />
                            <ListItemText primary={`Date (Latest - Oldest)`} />
                        </ListItem>
                    </List>

                </Dialog>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        auth: state.Auth.toJS(),
        search: state.Search.toJS(),
        app: state.App.toJS(),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        cityChange(city) {
            dispatch(searchActions.cityChange(city));
        },
        sortChange(sort) {
            dispatch(searchActions.sortChange(sort));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(injectIntl(Header)));