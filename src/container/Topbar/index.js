import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { Icon, Select, Spin } from 'antd';
import Logo from '../../component/Logo';
import NavbarToggle from '../../component/NavbarToggle';
import TopbarHolder from './topbar.style';
import { Link } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import IntlMessage from '../../component/utility/intlMessage';
import { connect } from 'react-redux';
import * as searchActions from '../../redux/search/actions';
import { citySearch } from '../../services/geolocation';
import debounce from 'lodash/debounce';
import Dialog from '@material-ui/core/Dialog';
import MUIButton from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Radio from '@material-ui/core/Radio';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import PersonAddIcon from '@material-ui/icons/PersonAdd';

const Option = Select.Option;

const styles = {
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
      },
};

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class Topbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cities: [],
            city: props.city,
            collapsed: true,
            fetching: false,
            sortOpen: false,
            sort: props.sort,
        };
        this.lastFetchId = 0;
        this.fetchCity = debounce(this.fetchCity, 300);
    }

    onToggle = collapsed => {
        console.log(collapsed);
        this.setState({
            collapsed: !collapsed
        });
    }

    handleLogout = event => {
        event.preventDefault();
        const { logout } = this.props;
        this.onToggle(false);
        logout();
    }

    fetchCity = value => {
        if (!value) {
            return;
        }
        this.lastFetchId += 1;
        const fetchId = this.lastFetchId;
        this.setState({
            cities: [],
            fetching: true
        });
        citySearch(value)
            .then(result => {
                console.log(result);
                if (fetchId !== this.lastFetchId) {
                    return;
                }
                this.setState({
                    cities: result,
                    fetching: false
                })
            }).catch(error => {
                console.log(error);
                this.setState({
                    cities: [],
                    fetching: false
                })
            });

    }

    handleCityChange = value => {
        const { cityChange } = this.props;
        this.setState({
            city: value,
            cities: [],
            fetching: false
        }, () => cityChange(value));
    }

    handleSort = event => {
        event.preventDefault();
        this.onToggle(false);
        this.setState({
            sortOpen: true
        });
    }

    handleSortClose = () => {
        const { sort } = this.props.search;
        this.setState({ sortOpen: false, sort: sort });
    }

    handleSortSave = () => {
        const { sortChange } = this.props;
        this.setState({ sortOpen: false }, () => sortChange(this.state.sort));
    }

    handleSortChange = event => {
        console.log(event.target.value);
        this.setState({ sort: event.target.value });
    }

    handleSortChange2 = value => {
        this.setState({ sort: value });
    }

    render() {
        const { auth, app } = this.props;
        const isLoggedIn = !!auth.oauth;
        const { city, collapsed, fetching, cities, sortOpen, sort, addOpen } = this.state;
        const { intl, classes } = this.props;
        const suffix = city ? <Icon type="close-circle" /> : null;
        return (
            <TopbarHolder>
                {
                    app.tab === "me" ?
                        null
                        :
                        <Navbar fluid onToggle={this.onToggle} fixedTop expanded={!collapsed}>
                            <Navbar.Header>
                                <Navbar.Brand>
                                    <Logo />
                                </Navbar.Brand>
                                <NavbarToggle collapsed={collapsed} />
                                {/* <Input
                            placeholder={intl.formatMessage({id: "topbar.city"})}
                            prefix={<Icon type="search" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            suffix={suffix}
                            value={city}
                            style={{ flex: 1 }}
                            className="form-control"
                        /> */}
                                <Select
                                    showSearch
                                    labelInValue
                                    size="large"
                                    value={city}
                                    showArrow={false}
                                    placeholder={intl.formatMessage({ id: "topbar.city" })}
                                    notFoundContent={fetching ? <Spin size="small" /> : null}
                                    filterOption={false}
                                    onChange={this.handleCityChange}
                                    onSearch={this.fetchCity}
                                    style={{ width: '100%', paddingRight: 5 }}
                                >
                                    {
                                        cities.map(city => <Option key={city.value}>{city.label}</Option>)
                                    }
                                </Select>
                            </Navbar.Header>
                            <Navbar.Collapse>
                                <Nav pullRight>
                                    <NavItem eventKey={5} href="/dashboard/hosts" onClick={this.handleSort}>
                                        Sort
                            </NavItem>
                                    <NavItem eventKey={1} href="/dashboard/publish" onClick={event => this.onToggle(false)} componentClass={Link} to="/dashboard/publish">
                                        <IntlMessage id="topbar.host" />
                                    </NavItem>
                                    {
                                        isLoggedIn ?
                                            <React.Fragment>
                                                <NavItem eventKey={5} href="/dashboard/hosts" onClick={event => this.onToggle(false)} componentClass={Link} to="/dashboard/hosts">
                                                    <IntlMessage id="topbar.hosts" />
                                                </NavItem>
                                                <NavItem eventKey={2} href="/login" onClick={this.handleLogout}>
                                                    Logout
                                    </NavItem>
                                            </React.Fragment>
                                            :
                                            <React.Fragment>
                                                <NavItem eventKey={3} href="/signup" componentClass={Link} to="/signup">
                                                    Sign Up
                                    </NavItem>
                                                <NavItem eventKey={4} href="/login" componentClass={Link} to="/login">
                                                    Login
                                    </NavItem>
                                            </React.Fragment>
                                    }
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>
                }

                <Dialog
                    fullScreen
                    open={sortOpen}
                    onClose={this.handleSortClose}
                    TransitionComponent={Transition}>
                    <AppBar className={classes.appBar}>
                        <Toolbar>
                            <IconButton color="inherit" onClick={this.handleSortClose}>
                                <CloseIcon />
                            </IconButton>
                            <Typography variant="title" color="inherit" className={classes.flex}>
                                Sort
                            </Typography>
                            <MUIButton color="inherit" onClick={this.handleSortSave}>
                                Save
                            </MUIButton>
                        </Toolbar>
                    </AppBar>

                    <List>
                        <ListItem
                            onClick={() => this.handleSortChange2("price-desc")}
                            role={undefined}
                            button
                            key="price-desc">
                            <Radio value="price-desc" checked={sort === "price-desc" ? true : false} onChange={this.handleSortChange} />
                            <ListItemText primary={`Price (High - Low)`} />
                        </ListItem>
                        <ListItem
                            onClick={() => this.handleSortChange2("price-asc")}
                            button
                            role={undefined}
                            key="price-asc">
                            <Radio value="price-asc" checked={sort === "price-asc" ? true : false} onChange={this.handleSortChange} />
                            <ListItemText primary={`Price (Low - High)`} />
                        </ListItem>
                        <ListItem
                            onClick={() => this.handleSortChange2("date-desc")}
                            button
                            role={undefined}
                            key="date-desc">
                            <Radio value="date-desc" checked={sort === "date-desc" ? true : false} onChange={this.handleSortChange} />
                            <ListItemText primary={`Date (Latest - Oldest)`} />
                        </ListItem>
                    </List>

                </Dialog>
            </TopbarHolder>
        );
    }
}

function mapStateToProps(state) {
    return {
        auth: state.Auth.toJS(),
        search: state.Search.toJS(),
        app: state.App.toJS(),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        cityChange(city) {
            dispatch(searchActions.cityChange(city));
        },
        sortChange(sort) {
            dispatch(searchActions.sortChange(sort));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(injectIntl(Topbar)));