import styled from 'styled-components';

const TopbarHolder = styled.div`
    .navbar-header {
        display: flex;
        align-items: center;

        .form-control {
            width: 350px;

            @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
                flex: 1;
            }
        }
    }
`;

export default TopbarHolder;