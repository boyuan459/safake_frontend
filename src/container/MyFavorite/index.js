import React, { Component } from 'react';
import { List } from 'antd';
import { connect } from 'react-redux';
import Item from '../../component/List/Item';
import { withStyles } from '@material-ui/core/styles';
import HostDetail from '../../component/HostDetail';
import { getFavorites } from '../../selectors';
import * as favoriteActions from '../../redux/favorite/actions';

const styles = theme => ({
    root: {
        marginTop: 20,
    },
    loader: {
        position: 'absolute',
        bottom: 50,
        width: '100%',
        textAlign: 'center',
    }
});
class GridList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            detailOpen: false,
            host: null,
        }
    }

    handleItemClick = host => {
        console.log(host);
        this.setState({
            host,
            detailOpen: true,
        })
    }

    handleDetailClose = () => {
        // const { history, tabChange } = this.props;
        // history.push('/dashboard/me')
        // tabChange("me");
        this.setState({
            detailOpen: false,
            host: null
        });
    }

    handleFavorite = (host_id) => {
        const { favorite } = this.props;
        favorite(host_id);
    }

    handleUnfavorite = (host_id) => {
        const { unfavorite } = this.props;
        unfavorite(host_id);
    }

    render() {
        const { classes, auth, favorites } = this.props;
        const { host, detailOpen } = this.state;

        return (
            <div className={classes.root}>
                <List
                    grid={{ gutter: 16, xs: 1, sm: 2, md: 2, lg: 3, xl: 4 }}
                    dataSource={favorites}
                    renderItem={item => (
                        <List.Item>
                            <Item key={item._id} data={item} onClick={this.handleItemClick}
                                favorites={favorites}
                                handleFavorite={this.handleFavorite}
                                handleUnfavorite={this.handleUnfavorite} />
                        </List.Item>
                    )}>
                </List>
                {
                    host ?
                        <HostDetail auth={auth} host={host} handleDetailClose={this.handleDetailClose} detailOpen={detailOpen} />
                        : null
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        auth: state.Auth.toJS(),
        favorites: getFavorites(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        favorite(host_id) {
            dispatch(favoriteActions.favoriteCreate(host_id));
        },
        unfavorite(host_id) {
            dispatch(favoriteActions.favoriteDestroy(host_id));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(GridList));