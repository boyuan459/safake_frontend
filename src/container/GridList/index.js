import React, { Component } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { List, Spin, message } from 'antd';
import { connect } from 'react-redux';
import { searchHost } from '../../services/host';
import Item from '../../component/List/Item';
import { withStyles } from '@material-ui/core/styles';
import HostDetail from '../../component/HostDetail';
import { getFavorites, getHosts, getHostLoading, getHostError, getHostTotalRows, getHostPage, getHostHasMore, getLikes } from '../../selectors';
import * as favoriteActions from '../../redux/favorite/actions';
import * as hostsActions from '../../redux/host/actions';
import Comment from '../Comment';

const styles = theme => ({
    root: {
        marginTop: 20,
    },
    loader: {
        position: 'absolute',
        bottom: 50,
        width: '100%',
        textAlign: 'center',
    }
});
class GridList extends Component {

    constructor(props) {
        super(props);

        this.state = {  
            city: props.search.get('city').key,
            sort: props.search.get('sort'),
            detailOpen: false,
            host: null,
            commentOpen: false,
            commentHost: null,
        }
    }

    componentDidMount() {
        this.fetchData();
        const { auth, category, likesFetch } = this.props;
        if (auth.oauth != null) {
            likesFetch(category);
        }
    }

    fetchData = () => {
        const { category = 'host', page, hostsSearch } = this.props;
        const { city, sort } = this.state;
        hostsSearch({ page, city, sort, category });
        // let nextPage = page + 1;
        // searchHost({ page, city, sort, category })
        //     .then(result => {
        //         console.log(result);
        //         const data = result.data;
        //         let list = this.state.list.concat(data);
        //         if (data.length) {
        //             this.setState({
        //                 list: list,
        //                 totalRows: result.total,
        //                 loading: false,
        //                 page: nextPage,
        //                 hasMore: list.length < result.total
        //             })
        //         }
        //     })
        //     .catch(error => {
        //         console.log(error);
        //         this.setState({
        //             loading: false
        //         });
        //     });
    }

    handleInfiniteOnLoad = () => {
        console.log("loading...");
        const { hosts, totalRows } = this.props;
        
        if (hosts.length > totalRows) {
            message.warning('No more data');
            return;
        }
        this.fetchData();
    }

    handleItemClick = host => {
        console.log(host);
        this.setState({
            host,
            detailOpen: true,
        })
    }

    handleDetailClose = () => {
        // const { history, tabChange } = this.props;
        // history.push('/dashboard/me')
        // tabChange("me");
        this.setState({
            detailOpen: false,
            host: null
        });
    }

    handleFavorite = (host_id) => {
        const { favorite } = this.props;
        favorite(host_id);
    }

    handleUnfavorite = (host_id) => {
        const { unfavorite } = this.props;
        unfavorite(host_id);
    }

    handleLike = (host_id) => {
        const { likeHost } = this.props;
        likeHost(host_id);
    }

    handleDislike = (host_id) => {
        const { dislikeHost } = this.props;
        dislikeHost(host_id);
    }

    handleCommentShow = (host) => {
        this.setState({
            commentOpen: true,
            commentHost: host
        })
    }

    handleCommentClose = () => {
        this.setState({
            commentOpen: false,
            commentHost: null,
        })
    }

    handleComment = (comment) => {
        const { commentHost } = this.state;
        const { commentOnHost } = this.props;
        console.log("Save comment", commentHost, comment);
        commentOnHost({host_id: commentHost._id, comment});
        this.handleCommentClose();
    }

    render() {
        const { classes, auth, favorites, hosts, loading, hasMore, likes } = this.props;
        const { host, detailOpen, commentOpen } = this.state;
        const isLoggedIn = !!auth.oauth;
        return (
            <div className={classes.root}>
            <InfiniteScroll
                initialLoad={false}
                pageStart={1}
                loadMore={this.handleInfiniteOnLoad}
                hasMore={!loading && hasMore}
                >
                <List
                    grid={{ gutter: 16, xs: 1, sm: 2, md: 2, lg: 3, xl: 4 }}
                    dataSource={hosts}
                    renderItem={item => (
                        <List.Item>
                        <Item key={item._id} data={item} onClick={this.handleItemClick} 
                            isLoggedIn={isLoggedIn}
                            likes={likes}
                            favorites={favorites} 
                            handleLike={this.handleLike}
                            handleDislike={this.handleDislike}
                            handleFavorite={this.handleFavorite} 
                            handleUnfavorite={this.handleUnfavorite}
                            handleComment={this.handleCommentShow}/>
                        </List.Item>
                    )}>
                    {
                        loading && hasMore && (
                            <div key="loader" className={classes.loader}><Spin /></div>
                        )
                    }
                </List>
            </InfiniteScroll>
            {
                    host ?
                        <HostDetail auth={auth} host={host} handleDetailClose={this.handleDetailClose} detailOpen={detailOpen} />
                        : null
                }
            <Comment open={commentOpen} handleClose={this.handleCommentClose} handleComment={this.handleComment}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        search: state.Search,
        auth: state.Auth.toJS(),
        favorites: getFavorites(state),
        hosts: getHosts(state),
        loading: getHostLoading(state),
        error: getHostError(state),
        totalRows: getHostTotalRows(state),
        hasMore: getHostHasMore(state),
        page: getHostPage(state),
        likes: getLikes(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        favorite(host_id) {
            dispatch(favoriteActions.favoriteCreate(host_id));
        },
        unfavorite(host_id) {
            dispatch(favoriteActions.favoriteDestroy(host_id));
        },
        hostsSearch(params) {
            dispatch(hostsActions.hostsSearch(params));
        },
        likesFetch(category) {
            dispatch(hostsActions.likesFetch(category));
        },
        likeHost(host_id) {
            dispatch(hostsActions.hostLike(host_id));
        },
        dislikeHost(host_id) {
            dispatch(hostsActions.hostDislike(host_id));
        },
        commentOnHost(comment) {
            dispatch(hostsActions.hostComment(comment));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(GridList));