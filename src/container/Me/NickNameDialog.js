import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class NickNameDialog extends React.Component {
    handleAdd = () => {
        const { username, handleSubscribeBack, handleClose } = this.props;
        if (!this.state.nickname) {
            return;
        }
        handleSubscribeBack(username, this.state.nickname);
        handleClose();
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        // console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    render() {
        const { open, handleClose } = this.props;
        return (
            <Dialog
                disableBackdropClick={true}
                disableEscapeKeyDown={true}
                open={open}
                onClose={() => handleClose()}
                aria-labelledby="nickname-dialog-title">
                <DialogTitle id="nickname-dialog-title">Add Friend</DialogTitle>
                <DialogContent>
                    <TextField 
                        autoFocus
                        margin="dense"
                        id="nickname"
                        label="Nick Name"
                        name="nickname"
                        type="text"
                        onChange={this.handleUserInput}
                        fullWidth/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleAdd} color="primary">
                        Add
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default NickNameDialog;