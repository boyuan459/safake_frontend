import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ChevronLeft';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Transition from '../../component/Transition';
import PropTypes from 'prop-types';
import red from '@material-ui/core/colors/red';
import { Upload, Icon, message } from 'antd';
import * as Constants from '../../constant/common';
import { connect } from 'react-redux';
import * as authActions from '../../redux/auth/actions';

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
        marginLeft: -48,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    container: {
        padding: 0,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
    avatar: {
        backgroundColor: red[300],
    },
    image: {
        width: '100%',
        height: 300,
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundColor: '#e9e9e9',
    },
    uploader: {
        display: 'flex',
        justifyContent: 'center',
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        }
    }
};

const UploadConfig = {
    baseUrl: process.env.REACT_APP_API_BASE_URL,
}
class UploadAvatarDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            image: props.auth.user.avatar,
        }
    }
    
    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        // console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    handleClose = () => {
        const { handleClose } = this.props;
        if (handleClose != null) {
            handleClose();
        }
    }


    handleGroupChatClose = () => {
        this.setState({
            groupChatOpen: false,
            group: null,
        })
    }

    beforeUpload = file => {
        const isJPG = Constants.VALID_IMAGE_TYPES.includes(file.type);
        if (!isJPG) {
            message.error("You can only upload image file!");
        }
        const isLt10M = file.size/1024/1024/10;
        if (!isLt10M) {
            message.error("Image must be smaller than 5MB!");
        }
        return isJPG && isLt10M;
    }

    handleChange = info => {
        if (info.file.status === 'uploading') {
            this.setState({
                loading: true
            });
            return;
        }
        if (info.file.status === 'done') {
            console.log(info);
            const url = info.file.response.url;
            const { changeAvatar } = this.props;
            this.setState({
                loading: false,
                image: url
            }, () => changeAvatar(url));
        }

    }

    render() {
        const { open, classes, auth } = this.props;
        const { loading, image } = this.state;

        const uploadConfig = {
            name: 'images',
            action: UploadConfig.baseUrl + '/api/upload/avatar',
            listType: 'picture-card',
            showUploadList: false,
            onChange: this.handleChange,
            beforeUpload: this.beforeUpload,
            className: classes.uploader,
            headers: {
                authorization: 'Bearer ' + auth.oauth.access_token,
            }
        }

        const uploadButton = (
            <div>
                <Icon type={loading ? 'loading' : 'plus'} />
                <div>Upload</div>
            </div>
        );
        return (
        <Dialog
            fullScreen
            open={open}
            onClose={this.handleClose}
            TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton color="inherit" onClick={this.handleClose}>
                        <BackIcon />
                    </IconButton>
                    <Typography variant="title" color="inherit" className={classes.flex}>
                        Upload Avatar
                    </Typography>
                    
                </Toolbar>
            </AppBar>

            <div className={classes.container}>
                <div style={{backgroundImage: image ? `url(${image})` : null}} className={classes.image}>

                </div>
                <Upload {...uploadConfig}>
                    {
                        uploadButton
                    }
                </Upload>
            </div>
        </Dialog>);
    }
}

UploadAvatarDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    return {
        auth: state.Auth.toJS(),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        changeAvatar(avatar) {
            dispatch(authActions.avatarChange(avatar));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, {withTheme: true})(UploadAvatarDialog));