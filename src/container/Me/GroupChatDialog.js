import React, { Component } from 'react';
import { xml2json, xml2js } from 'xml-js';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import red from '@material-ui/core/colors/red';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import BackIcon from '@material-ui/icons/ChevronLeft';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MUIButton from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import SendIcon from '@material-ui/icons/Send';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import VoiceIcon from '@material-ui/icons/KeyboardVoice';
import ImageIcon from '@material-ui/icons/Image';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Transition from '../../component/Transition';
import PersonIcon from '@material-ui/icons/Person';

const styles = theme => ({
    avatar: {
        backgroundColor: red[500],
    },
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
    },
    appBarRightButton: {
        minWidth: 56,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
    chatArea: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
    messageList: {
        flex: 1,
        paddingBottom: 100,
        overflow: 'scroll',
    },
    messageSendContainer: {
        minHeight: 100,
        display: 'flex',
        flexDirection: 'column',
        position: 'fixed',
        bottom: 0,
        left: 0,
        width: '100%',
        zIndex: 1000,
        background: 'rgba(249,249,249, 0.9)',
    },
    messageText: {
        flex: 1,
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
    },
    messageButtons: {
        height: 42,
        
    },
    rightIcon: {
        marginLeft: theme.spacing.unit
    },
    messageItemMe: {
        flexDirection: 'row-reverse',
    },
    messageBox: {
        margin: '0 15px 0 15px',
        display: 'flex',
    },
    messageBoxFriendComposing: {
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
        minHeight: 40,
        height: 'auto',
        backgroundColor: '#f9f9f9',
        borderRadius: 6,
        position: 'relative',
        fontFamily: 'Roboto',
        wordWrap: 'break-word',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        '&::before': {
            content: '""',
            width: 0,
            height: 0,
            position: 'absolute',
            borderWidth: 10,
            borderStyle: 'solid',
            borderColor: 'transparent #f9f9f9 transparent transparent',
            left: -19,
            top: '30%',
        }
    },
    messageBoxFriend: {
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
        minHeight: 40,
        height: 'auto',
        backgroundColor: '#ccc',
        borderRadius: 6,
        position: 'relative',
        fontFamily: 'Roboto',
        wordWrap: 'break-word',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        '&::before': {
            content: '""',
            width: 0,
            height: 0,
            position: 'absolute',
            borderWidth: 10,
            borderStyle: 'solid',
            borderColor: 'transparent #ccc transparent transparent',
            left: -19,
            top: '30%',
        }
    },
    messageBoxMe: {
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
        minHeight: 40,
        height: 'auto',
        backgroundColor: 'blue',
        borderRadius: 6,
        position: 'relative',
        fontFamily: 'Roboto',
        color: '#fff',
        wordWrap: 'break-word',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        '&::after': {
            content: '""',
            width: 0,
            height: 0,
            position: 'absolute',
            borderWidth: 10,
            borderStyle: 'solid',
            borderColor: 'transparent transparent transparent blue',
            right: -19,
            bottom: '30%',
        }
    },
    message: {
        margin: 0,
    },
    header: {
        minHeight: 56,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    list: {
        width: 200,
    }
});

class GroupChatDialog extends Component {
    state = {
        message: '',
    }
    handleClose = () => {
        const { handleClose } = this.props;
        if (handleClose != null) {
            handleClose();
        }
    }

    handleInputMessage = e => {
        const name = e.target.name;
        const value = e.target.value;
        console.log(name, value);
        this.setState({
            [name]: value
        })
        //send composing message
        // this.handleMessageComposing(value);
    }

    sendMessage = (e) => {
        e.preventDefault();
        const { sendGroupMessage, group } = this.props;
        const { message } = this.state;
        if (!message || !sendGroupMessage) {
            return;
        }
        console.log(message);
        sendGroupMessage(group._attributes.jid, message);
        this.setState({
            message: ''
        })
    }

    render() {
        const { open = false, classes, group, messages, myself } = this.props;
        return (
            <Dialog
                fullScreen
                open={open}
                onClose={this.handleClose}
                TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton color="inherit" onClick={this.handleClose}>
                            <BackIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            {group ? group._attributes.name : null}
                        </Typography>
                        <MUIButton className={classes.appBarRightButton} color="inherit">
                            <AccountCircle />
                        </MUIButton>
                    </Toolbar>

                </AppBar>
                <div className={classes.chatArea}>
                    <div className={classes.messageList} ref={messageList => this.messageList = messageList}>
                        <List>
                        {
                                    group && messages.map((msg, index) => {
                                        if (msg.group === group._attributes.jid && msg.from === myself) {
                                            return (
                                                <ListItem key={index} className={classes.messageItemMe}>
                                                    <Avatar>
                                                        <PersonIcon />
                                                    </Avatar>
                                                    <div className={classes.messageBox}>
                                                        <div className={classes.messageBoxMe}>
                                                            <p className={classes.message}>{msg.body._text}</p>
                                                        </div>
                                                    </div>
                                                </ListItem>
                                            )
                                        } else if (msg.group === group._attributes.jid) {
                                            return (
                                                <ListItem key={index}>
                                                    <Avatar className={classes.avatar}>
                                                        <PersonIcon />
                                                    </Avatar>
                                                    <div className={classes.messageBox}>
                                                        <div className={classes.messageBoxFriend}>
                                                            <p className={classes.message}>{msg.body._text}</p>
                                                        </div>
                                                    </div>
                                                </ListItem>
                                            )
                                        }
                                    })
                                }
                        </List>
                    </div>
                    <div className={classes.messageSendContainer}>
                        <TextField
                            id="multiline-flexible"
                            placeholder="Write a message"
                            multiline
                            rowsMax="2"
                            name="message"
                            value={this.state.message}
                            onChange={this.handleInputMessage}
                            className={classes.messageText}
                            margin="normal"
                        />
                        <div className={classes.messageButtons}>
                            <Grid container>
                                <Grid item xs={8}>
                                    <IconButton>
                                        <PhotoCamera />
                                    </IconButton>
                                    <IconButton>
                                        <VoiceIcon />
                                    </IconButton>
                                    <IconButton>
                                        <ImageIcon />
                                    </IconButton>
                                </Grid>
                                <Grid item xs={4}>
                                    <Button variant="contained" color="primary" onClick={this.sendMessage}>
                                        Send
                                        <SendIcon className={classes.rightIcon} />
                                    </Button>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                </div>
            </Dialog>
        )
    }
}

export default withStyles(styles)(GroupChatDialog);