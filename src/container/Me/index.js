import React, { Component } from 'react';
import { xml2json, xml2js } from 'xml-js';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Badge from '@material-ui/core/Badge';
import PersonIcon from '@material-ui/icons/Person';
import ArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import red from '@material-ui/core/colors/red';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import BackIcon from '@material-ui/icons/ChevronLeft';
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/Check';
import DiscoIcon from '@material-ui/icons/Search';
import AddCircleIcon from '@material-ui/icons/AddCircleOutline';
import Slide from '@material-ui/core/Slide';
import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import Dialog from '@material-ui/core/Dialog';
import MUIButton from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import Divider from '@material-ui/core/Divider';
import SendIcon from '@material-ui/icons/Send';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import VoiceIcon from '@material-ui/icons/KeyboardVoice';
import ImageIcon from '@material-ui/icons/Image';
import AccountCircle from '@material-ui/icons/AccountCircle';
import GroupIcon from '@material-ui/icons/Group';
import PublisherIcon from '@material-ui/icons/RssFeed';
import SubscribeIcon from '@material-ui/icons/Subscriptions';
import FriendsHotIcon from '@material-ui/icons/Whatshot';
import SubscribersListIcon from '@material-ui/icons/ViewList';
import SettingsIcon from '@material-ui/icons/Settings';
import config from '../../config/app';
import { connect } from 'react-redux';
import debounce from 'lodash/debounce';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { injectIntl } from 'react-intl';
import classNames from 'classnames';
import CreateRoomIcon from '@material-ui/icons/GroupAdd';
import IntlMessage from '../../component/utility/intlMessage';
import CreateRoomDialog from './CreateRoomDialog';
import * as XMPP from '../../constant/xmpp';
import DiscoDialog from './DiscoDialog';
import NickNameDialog from './NickNameDialog';
import GroupChatDialog from './GroupChatDialog';
import CreatePublisherDialog from './CreatePublisherDialog';
import CreatePostDialog from './CreatePostDialog';
import SubscribeDialog from './SubscribeDialog';
import CreateStoryDialog from './CreateStoryDialog';
import MyAccountDialog from './MyAccountDialog';
import { getPersonlPub } from '../../services/pubsub';
import SubscribeMessages from './SubscribeMessages';
import * as authActions from '../../redux/auth/actions';
import * as subActions from '../../redux/sub/actions';
import SubscribeFriendsMessages from './SubscribeFriendsMessages';
import AddFriendDialog from './AddFriendDialog';

const uuid = require("uuid/v4");

const CLEAR_MESSAGE_INTERVAL = 60000;

const Message = {
    Domain: process.env.REACT_APP_MESSAGE_DOMAIN,
    Connection: process.env.REACT_APP_MESSAGE_CONNECTION,
    PubsubDomain: process.env.REACT_APP_MESSAGE_PUBSUB_DOMAIN
}

const MSG_CONFIG = {
    GROUP_DOMAIN: process.env.REACT_APP_GROUP_MESSAGE_DOMAIN
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: 752,
    },
    demo: {
        marginTop: -56,
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
    },
    avatar: {
        backgroundColor: red[500],
    },
    groupAvatar: {
        backgroundColor: red[300],
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    appBarRightButton: {
        minWidth: 56,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
    chatArea: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
    messageList: {
        flex: 1,
        paddingBottom: 100,
        overflow: 'scroll',
    },
    messageSendContainer: {
        minHeight: 100,
        display: 'flex',
        flexDirection: 'column',
        position: 'fixed',
        bottom: 0,
        left: 0,
        width: '100%',
        zIndex: 1000,
        background: 'rgba(249,249,249, 0.9)',
    },
    messageText: {
        flex: 1,
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
    },
    messageButtons: {
        height: 42,

    },
    rightIcon: {
        marginLeft: theme.spacing.unit
    },
    messageItemMe: {
        flexDirection: 'row-reverse',
    },
    messageBox: {
        margin: '0 15px 0 15px',
        display: 'flex',
    },
    messageBoxFriendComposing: {
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
        minHeight: 40,
        height: 'auto',
        backgroundColor: '#f9f9f9',
        borderRadius: 6,
        position: 'relative',
        fontFamily: 'Roboto',
        wordWrap: 'break-word',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        '&::before': {
            content: '""',
            width: 0,
            height: 0,
            position: 'absolute',
            borderWidth: 10,
            borderStyle: 'solid',
            borderColor: 'transparent #f9f9f9 transparent transparent',
            left: -19,
            top: '30%',
        }
    },
    messageBoxFriend: {
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
        minHeight: 40,
        height: 'auto',
        backgroundColor: '#ccc',
        borderRadius: 6,
        position: 'relative',
        fontFamily: 'Roboto',
        wordWrap: 'break-word',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        '&::before': {
            content: '""',
            width: 0,
            height: 0,
            position: 'absolute',
            borderWidth: 10,
            borderStyle: 'solid',
            borderColor: 'transparent #ccc transparent transparent',
            left: -19,
            top: '30%',
        }
    },
    messageBoxMe: {
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
        minHeight: 40,
        height: 'auto',
        backgroundColor: 'blue',
        borderRadius: 6,
        position: 'relative',
        fontFamily: 'Roboto',
        color: '#fff',
        wordWrap: 'break-word',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        '&::after': {
            content: '""',
            width: 0,
            height: 0,
            position: 'absolute',
            borderWidth: 10,
            borderStyle: 'solid',
            borderColor: 'transparent transparent transparent blue',
            right: -19,
            bottom: '30%',
        }
    },
    message: {
        margin: 0,
    },
    header: {
        minHeight: 56,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    list: {
        width: 200,
    }
});


//to run mongooseim 
//docker run -d -t -h mongooseim-1 --name mongooseim-1 -p 5222:5222 -p 5280:5280  mongooseim/mongooseim:latest

//use ejabberd for this one as it's proved to be better
//for oauth configure, refer to prefiks commented on 21 Apr 2017 on https://github.com/processone/ejabberd/issues/1661

const options = { compact: true, ignoreComment: true, alwaysChildren: true };

function Transition(props) {
    return <Slide direction="left" {...props} />;
}
class Me extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            roster: [],
            addingFriends: [],
            addOpen: false,
            snackOpen: false,
            pendingSubscribers: [],
            chatOpen: false,
            chatItem: null,
            receiveMessages: [],
            composingMessage: null,
            drawerOpen: false,
            createRoomOpen: false,
            jid: '',
            discoOpen: false,
            nickNameOpen: false,
            subscribeBack: '',
            groupsOpen: false,
            groupLoading: false,
            groups: [],
            activeGroup: null,
            groupsMessages: [],
            createPublisherOpen: false,
            pubsub: props.auth.user.pubsubs ? props.auth.user.pubsubs[0] : null,
            createPostOpen: false,
            subscribeOpen: false,
            subscribeMessages: {
                count: 0,
                messages: []
            },
            subscribeMessagesOpen: false,
            createStoryOpen: false,
            subscribeFriendsMessages: {
                count: 0,
                messages: []
            },
            subscribeFriendsMessagesOpen: false,
            myAccountOpen: false,
        };
        this.handleMessageComposing = debounce(this.handleMessageComposing, 500);
    }

    handleAddOpen = () => {
        this.setState({
            addOpen: true
        })
    }

    handleAddClose = () => {
        this.setState({
            addOpen: false
        });
    }

    handlePing = iq => {
        console.dir(iq);
        console.log(iq);

        const json = xml2js(iq.outerHTML, options);
        console.log(json);
        return true;
    }

    getUserFromJid = jid => {
        const user = jid.substring(0, jid.indexOf('@'));
        return user;
    }

    getBareJidFromJid = jid => {
        const bareJid = this.Strophe.getBareJidFromJid(jid);
        return bareJid;
    }

    handlePresence = iq => {
        console.log(iq);
        const presence = xml2js(iq.outerHTML, options);
        console.log(presence);
        var ptype = presence.presence._attributes.type;
        var from = this.getBareJidFromJid(presence.presence._attributes.from);
        if (!this.state.jid) {
            this.setState({
                jid: presence.presence._attributes.to
            })
        }

        if (presence.presence.x && presence.presence.x._attributes && presence.presence.x._attributes.xmlns === XMPP.NS_MUC_USERS) {
            //joined or create rooms

        } else if(ptype === 'subscribe') {
            const { pendingSubscribers } = this.state;
            //check if pending subscriber is already in roster with subscription == to and jid == from
            //then send subscribed approve instead of show invitations
            //but when add friend, the roster have no such friend, so we need to maintain 
            //a list when adding friends.
            let found = this.state.roster.find(item => {
                if (item._attributes.jid === from && item._attributes.subscription === "to") {
                    const presence = window.Strophe.$pres({ to: from, type: "subscribed" });
                    this.conn.send(presence);
                    return true;
                }
            });
            if (!found) {
                //look in addingFriends list
                found = this.state.addingFriends.length && this.state.addingFriends.find(item => {
                    if (item.jid === from && item.subscription === "to") {
                        const presence = window.Strophe.$pres({ to: from, type: "subscribed" });
                        this.conn.send(presence);
                        return true;
                    }
                });
            }
            if (!found) {
                pendingSubscribers.push(from);
                this.setState({
                    pendingSubscribers
                })
            }else {
                //found in roster or addingFriends
                //subscriber to from's personal pubsub
                getPersonlPub(from)
                .then(data => {
                    console.log("Found in roster or adding list", from, data);
                    this.handlePersonalSubscribe(data.node);
                });
            }

        } else if (ptype !== 'error' || !ptype) {
            const roster = this.state.roster;
            if (ptype === 'unavailable') {
                roster.forEach(item => {
                    if (item._attributes.jid === from) {
                        item._attributes['status'] = ptype;
                    }
                })
            } else {
                roster.forEach(item => {
                    if (item._attributes.jid === from) {
                        item._attributes['status'] = 'online';
                    }
                })
            }
            console.log(roster);
            this.setState({
                roster
            })
        }

        return true;
    }

    handleRoster = iq => {
        console.log(iq);
        const roster = xml2js(iq.outerHTML, options);
        console.log(roster.iq);
        //filter out the sending invitation contact
        let subscribedRoster = [];
        if (Array.isArray(roster.iq.query.item)) {
            subscribedRoster = roster.iq.query.item.filter(item => {
                if (item._attributes.subscription !== "none") {
                    if (item.group && item.group._text === 'urn:xmpp:muclight:0') {
                        item._attributes.status = "online";
                    }
                    return true;
                }
                return false;
            });
        } else if (roster.iq.query.item != null) {
            subscribedRoster = roster.iq.query.item._attributes.subscription !== "none" ? [roster.iq.query.item] : [];
        }
        this.setState({
            roster: subscribedRoster
        });
        // setup presence handler and send initial presence
        this.conn.addHandler(this.handlePresence, null, "presence");
        const presence = window.Strophe.$pres();
        this.conn.send(presence);

        return true;
    }

    handleRosterChange = iq => {
        console.log(iq);
        const change = xml2js(iq.outerHTML, options);
        console.log(change);
        const item = change.iq.query.item;
        var sub = item._attributes.subscription;
        var jid = item._attributes.jid;
        var name = item._attributes.name;
        let roster = this.state.roster;
        if (sub === "remove") {
            roster = roster.filter(item => item._attributes.jid !== jid);
        } else if (name) {
            let found = roster.find(item => item._attributes.jid === jid);
            if (!found) {
                roster.push(item);
                this.setState({
                    roster
                });
            }
        }
        return true;
    }

    handleRoomsDisco = (iq) => {
        console.log("Disco groups ", iq);
        const iqObj = xml2js(iq.outerHTML, options);
        console.log(iqObj);
        if (iqObj.iq.query.item && iqObj.iq.query.item.length) {
            //multiple rooms;
        } else if (iqObj.iq.query.item) {
            //single room, add to roster
            let roster = this.state.roster;
            let item = Object.assign({}, iqObj.iq.query.item, { group : true})
            roster.push(item);
            this.setState({
                roster
            });
        }
    }

    discoRooms = () => {
        const iq = window.Strophe.$iq({ type: "get", to: "muclight.localhost" })
                        .c("query", { xmlns: XMPP.NS_DISCO_ITEMS });
        this.conn.sendIQ(iq, this.handleRoomsDisco);
    }

    handleGroupMessage = message => {
        console.log(message);
        const msgObj = xml2js(message.outerHTML, options);
        console.log(msgObj);
        if (msgObj.message.x && msgObj.message.x._attributes.xmlns === 'urn:xmpp:muclight:0#affiliations') {
            //join a rooms, send roster iq query, this causes problems
            // const iq = window.Strophe.$iq({ type: "get" }).c("query", { xmlns: "jabber:iq:roster" });
            // this.conn.sendIQ(iq, this.handleRoster);
            //join a rooms, disco occupied rooms
            this.discoRooms();
        } else {
            //real message
            const newMsg = Object.assign({}, { ...msgObj.message._attributes }, { body: msgObj.message.body });
            console.log(newMsg);
            const from = newMsg.from;
            let parts = from.split("/");
            newMsg.group = parts[0];
            newMsg.from = parts[1];
            newMsg.timestamp = new Date().getTime();
            console.log(newMsg);
            //todo update badge
            //if has active group, active group messages status mark as read
            //calculate the status != 'read'
            let groupsMessages = this.state.groupsMessages;
            groupsMessages.push(newMsg);
            //update roster badge
            const roster = this.state.roster.map(item => {
                if (item._attributes.jid === newMsg.group) {
                    item.messageCount = item.messageCount ? parseInt(item.messageCount) + 1 : 1;
                }
                return item;
            });
            const { chatItem } = this.state;
            if (chatItem != null && chatItem._attributes.jid === newMsg.group) {
                this.setState({
                    groupsMessages,
                    roster
                });
            }
        }
        return true;
    }

    handleMessage = message => {
        console.log(message);
        const msgObj = xml2js(message.outerHTML, options);
        console.log(msgObj);
        if (msgObj.message.body) {
            const newMsg = Object.assign({}, msgObj.message._attributes);
            newMsg.body = msgObj.message.body._text;
            newMsg.timestamp = new Date().getTime();
            //update the chatItem if it has
            const { chatItem } = this.state;
            newMsg.from = this.Strophe.getBareJidFromJid(newMsg.from);
            newMsg.to = this.Strophe.getBareJidFromJid(newMsg.to);
            console.log(newMsg);
            let messages = this.state.messages;
            messages.push(newMsg);
            //update roster badge
            const roster = this.state.roster.map(item => {
                if (item._attributes.jid === newMsg.from) {
                    item.messageCount = item.messageCount ? parseInt(item.messageCount) + 1 : 1;
                }
                return item;
            });
            if (chatItem != null && chatItem._attributes.jid === newMsg.from) {
                chatItem._attributes.fullJid = msgObj.message._attributes.from;
                this.setState({
                    messages,
                    roster,
                    chatItem,
                    composingMessage: null
                });
            } else {
                this.setState({
                    messages,
                    roster,
                    composingMessage: null
                });
            }

            this.scrollChat();
        } else if (msgObj.message.composing) {
            //receive composing message
            const { chatItem } = this.state;
            const notify = Object.assign({}, msgObj.message._attributes);
            notify.fromBareJid = this.Strophe.getBareJidFromJid(notify.from);
            notify.timestamp = new Date().getTime();
            //if chatItem._attributes.jid === message._attributes.from
            if (chatItem && chatItem._attributes.jid === notify.fromBareJid) {
                console.log(notify);
                this.setState({
                    composingMessage: notify
                })
            }
        }
        return true;
    }

    scrollChat = () => {
        if (this.messageList) {
            console.log(this.messageList.scrollHeight, this.messageList.scrollTop);
            // this.messageList.scrollTop = this.messageList.scrollHeight;
            this.messageList.scrollTo(0, this.messageList.scrollHeight);
        }
    }

    clearComposingMessage = () => {
        console.log("Clear composing message", composingMessage);
        const { composingMessage, chatItem } = this.state;
        const now = new Date().getTime();
        if (chatItem && composingMessage && composingMessage.fromBareJid === chatItem._attributes.jid) {
            if (composingMessage.timestamp < now - CLEAR_MESSAGE_INTERVAL / 1000) {
                this.setState({
                    composingMessage: null
                })
            }
        } else {
            this.setState({
                composingMessage: null
            })
        }
    }

    handleIQ = iq => {
        console.log(iq);
    }

    handleSubscribeMessage = (message) => {
        const { auth, sub } = this.props;
        console.log("Subscribe message", message);
        const msg = xml2js(message.outerHTML, options);
        console.log(msg);
        if (msg.message.event.items.item) {
            const node = msg.message.event.items._attributes.node;
            const item = msg.message.event.items.item;
            const subscriber = auth.subs.find(item => item.node === node);
            if (subscriber != null) {
                //enterprise subscriber
                const subMsg = {
                    node,
                    name: subscriber ? subscriber.name : null,
                    avatar: subscriber ? subscriber.avatar : null,
                    id: item._attributes.id
                };
                item.x.field.forEach(field => {
                    subMsg[field._attributes.var] = field.value._text;
                })
                subMsg.timestamp = subMsg.timestamp ? parseInt(subMsg.timestamp) : new Date().getTime();
                console.log(subMsg);
                const {subscribeMessages, subscribeMessagesOpen} = this.state;
                if (subscribeMessagesOpen) {
                    subscribeMessages.count = 0;
                } else {
                    //check if read
                    if (sub.read[subMsg.node] === subMsg.id) {
                        subMsg.read = true;
                    } else {
                        subscribeMessages.count += 1;
                        subMsg.read = false;
                    }
                }
                subscribeMessages.messages.unshift(subMsg);
                this.setState({
                    subscribeMessages
                });
            } else {
                //personal subscriber
                console.log("Personal subscribe", item);
                const hotMsg = {
                    node: node,
                    id: item._attributes.id
                };
                item.x.field.forEach(field => {
                    hotMsg[field._attributes.var] = field.value._text;
                })
                if (!hotMsg.from) {
                    return;
                }
                //find from in roster
                const friend = this.state.roster.find(item => item._attributes.jid === hotMsg.from);
                if (!friend) {
                    //not a friend at all, do nothing
                    return;
                }
                //todo can update avatar in roster
                hotMsg.name = friend._attributes.name;
                console.log(hotMsg);
                const { subscribeFriendsMessages, subscribeFriendsMessagesOpen } = this.state;
                if (subscribeFriendsMessagesOpen) {
                    subscribeFriendsMessages.count = 0;
                } else {
                    if (sub.read[hotMsg.node] === hotMsg.id) {
                        hotMsg.read = true;
                    } else {
                        hotMsg.read = false;
                        subscribeFriendsMessages.count += 1;
                    }
                }
                subscribeFriendsMessages.messages.unshift(hotMsg);
                this.setState({
                    subscribeFriendsMessages
                })
            }
            
        } else {
            //retract what the hell is this, can we turn it off to save resource
        }
        return true;
    }

    handleSubscribeOldMessages = (sub) => {
        const itemsiq = window.Strophe.$iq({to: Message.PubsubDomain, type: "get"})
                        .c("pubsub", { xmlns: XMPP.NS_PUBSUB })
                        .c("items", { node: sub.node});
        this.conn.sendIQ(itemsiq, (iq) => {
            this.handleSubscribeOldItems(iq, sub);
        });
    }

    handleSubscribeOldItems = (iq, subscriber) => {
        console.log("Old items", iq, subscriber);
        const iqObj = xml2js(iq.outerHTML, options);
        console.log(iqObj);
        const messages = iqObj.iq.pubsub.items.item;
        const { subscribeMessages } = this.state;
        const newMessages = [];
        if (messages.length) {
            messages.forEach(item => {
                const subMsg = {
                    node: subscriber.node,
                    name: subscriber ? subscriber.name : null,
                    avatar: subscriber ? subscriber.avatar : null,
                    id: item._attributes.id,
                    history: true
                };
                item.x.field.forEach(field => {
                    subMsg[field._attributes.var] = field.value._text;
                })
                subMsg.timestamp = subMsg.timestamp ? parseInt(subMsg.timestamp) : new Date().getTime();
                // console.log(subMsg);
                newMessages.push(subMsg);
            });
            newMessages.sort((a,b) => {
                if (!a.timestamp) {
                    return -1;
                }else if (!b.timestamp) {
                    return 1;
                } else if (a.timestamp < b.timestamp) {
                    return 1;
                } else if (a.timestamp > b.timestamp) {
                    return -1;
                }
                return 0;
            })
            //fitler already existing messages 
            const notExistMessages = newMessages.filter(item => {
                const found = subscribeMessages.messages.find(msg => msg.id === item.id);
                if (found != null) {
                    return false;
                }
                return true;
            })
            //set history mode to already existing messages
            subscribeMessages.messages.forEach(item => {
                if (item.node === subscriber.node) {
                    item.history = true;
                }
            })
            //find the index of subscriber in original messages
            const reverseMessages = subscribeMessages.messages.reverse();
            let pos = reverseMessages.findIndex(item => item.node === subscriber.node);
            if (pos != -1) {
                pos = reverseMessages.length - pos;
                subscribeMessages.messages.splice(pos, 0, ...notExistMessages);
            }
            this.setState({
                subscribeMessages
            })
        }
    }

    componentDidMount() {
        const { auth, fetchSubs } = this.props;
        fetchSubs();
        const mid = atob(atob(auth.user.mid));
        const hash = atob(atob(atob(atob(atob(auth.user.md5)))));
        this.Strophe = window.Strophe.Strophe;
        // this.conn = new this.Strophe.Connection("http://localhost:5280/http-bind"); //for mongooseim
        // this.conn = new this.Strophe.Connection("ws://localhost:5280/ws-xmpp"); //for mongooseim
        // this.conn = new this.Strophe.Connection("http://localhost:5280/bosh");  //for ejabberd
        // this.conn = new this.Strophe.Connection("ws://localhost:5280/ws"); //for ejabberd ws
        // this.conn = new this.Strophe.Connection("wss://im.progetdata.com:5280/ws"); //for ejabberd ws secure
        this.conn = new this.Strophe.Connection(Message.Connection);
        this.conn.connect(mid, hash, status => {
            if (status === this.Strophe.Status.CONNECTED) {
                const iq = window.Strophe.$iq({ type: "get" }).c("query", { xmlns: "jabber:iq:roster" });
                this.conn.sendIQ(iq, this.handleRoster);
                this.conn.addHandler(this.handleRosterChange, "jabber:iq:roster", "iq", "set");
                // this.conn.addHandler(this.handleIQ, null, "iq", null);
                this.conn.addHandler(this.handleMessage, null, "message", "chat");
                //disco rooms
                this.discoRooms();
                //handle groupchat
                this.conn.addHandler(this.handleGroupMessage, null, "message", "groupchat");
                //create personal pubsub node
                if (auth.user && !auth.user.node) {
                    this.handleCreatePublisher(null);
                }
                //handle subscribe message
                this.conn.addHandler(this.handleSubscribeMessage, null, "message", null, null, Message.PubsubDomain);
            } else if (status === this.Strophe.Status.DISCONNECTED) {
                console.log("Disconnected");
            }
        });
        this.timer = setInterval(this.clearComposingMessage, CLEAR_MESSAGE_INTERVAL);
    }

    componentWillUnmount() {
        if (this.conn) {
            this.conn.disconnect();
        }
        if (this.timer) {
            clearInterval(this.timer);
        }
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        // console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    handleInputMessage = e => {
        const name = e.target.name;
        const value = e.target.value;
        console.log(name, value);
        this.setState({
            [name]: value
        })
        //send composing message
        this.handleMessageComposing(value);
    }

    handleMessageComposing = value => {
        // console.log("Composing: " + value);
        //send composing msg
        const { chatItem } = this.state;
        var notify = window.Strophe.$msg({
            to: chatItem._attributes.fullJid ? chatItem._attributes.fullJid : chatItem._attributes.jid,
            type: "chat"
        }).c("composing", { xmlns: "http://jabber.org/protocol/chatstates" });
        this.conn.send(notify);
    }

    handleAddContact = (username, nickname) => {
        if (username != null) {
            const data = {
                jid: username + '@' + Message.Domain,
                name: nickname
            };
            console.log(data);
            var iq = window.Strophe.$iq({ type: "set" }).c("query", { xmlns: "jabber:iq:roster" })
                .c("item", data);
            this.conn.sendIQ(iq);

            var subscribe = window.Strophe.$pres({ to: data.jid, type: "subscribe" });
            this.conn.send(subscribe);
            const friend = {
                jid: data.jid,
                name: data.name,
                subscription: 'to',
            };
            let addingFriends = this.state.addingFriends;
            addingFriends.push(friend);
            this.setState({
                addingFriends
            });
        }
    }

    sendMessage = (e) => {
        e.preventDefault();
        const { message, chatItem, messages } = this.state;
        if (!message) {
            return;
        }
        // var domain = this.Strophe.getDomainFromJid(this.conn.jid);
        // console.log(domain);
        var msg = window.Strophe.$msg({
            to: chatItem._attributes.fullJid ? chatItem._attributes.fullJid : chatItem._attributes.jid,
            type: "chat"
        }).c("body").t(message);
        this.conn.send(msg);
        //save the message to sendMessages
        const sendMessage = {
            to: chatItem._attributes.jid,
            body: message,
            timestamp: new Date().getTime()
        };
        messages.push(sendMessage);
        this.setState({
            messages,
            message: ''
        });
        setTimeout(() => this.scrollChat(), 300);
    }

    sendGroupMessage = (group, message, cmd = "text") => {
        var msg = window.Strophe.$msg({
            to: group,
            type: "groupchat"
        }).c("body").t(message);
        console.log(msg);
        this.conn.send(msg, (msg) => this.handleGroupMessage(msg));
    }

    handleSnackClose = () => {
        this.setState({
            snackOpen: false
        })
    }

    handleSubscribeReject = subscriber => {
        const presence = window.Strophe.$pres({ to: subscriber, type: "unsubscribed" });
        this.conn.send(presence);
        this.removePendingSubscriber(subscriber);
    }

    handleSubscribeApprove = subscriber => {
        //subscribe back your friend presence
        this.setState({
            nickNameOpen: true,
            subscribeBack: subscriber
        })
    }

    handleSubscribeBack = (username, nickname) => {
        const data = {
            jid: username,
            name: nickname
        };
        console.log("Subscribe back", data);
        const presence = window.Strophe.$pres({ to: username, type: "subscribed" });
        this.conn.send(presence);
        this.removePendingSubscriber(username);
        var iq = window.Strophe.$iq({ type: "set" }).c("query", { xmlns: "jabber:iq:roster" })
            .c("item", data);
        this.conn.sendIQ(iq);
        var subscribe = window.Strophe.$pres({ to: username, type: "subscribe" });
        this.conn.send(subscribe);
        //subscriber to to's personal pubsub
        getPersonlPub(username)
        .then(result => {
            console.log("Subscribe to personal pub", result);
            this.handlePersonalSubscribe(result.node);
        });
    }

    removePendingSubscriber = subscriber => {
        const pendingSubscribers = this.state.pendingSubscribers.filter(item => item !== subscriber);
        this.setState({
            pendingSubscribers
        });
    }

    handleGroupChatOpen = (group) => {
        console.log(group);
        // var discoItem = window.Strophe.$iq({
        //     type: "get",
        //     to: group._attributes.jid
        // }).c("query", { xmlns: XMPP.NS_MUC_LIGHT_AFFILIATIONS })
        // .c("version", group.version._text);
        // console.log(discoItem);
        // this.conn.sendIQ(discoItem, (iq) => {
        //     const iqObj = xml2js(iq.outerHTML, options);
        //     console.log("Affiliations ", iqObj);
        // });
        // this.handleDiscovery(group._attributes.jid, XMPP.NS_MUC_LIGHT_AFFILIATIONS)
        //     .then(iq => {
        //         console.log(iq);
        //         const users = iq.iq.query.user;
        //         if (users.length) {
        //             //get username from roster first, if not exist
        //             //get username from backend database
        //             let members = users.map(user => {
        //                 const found = this.state.roster.find(item => user._text === item._attributes.jid);
        //                 let member = null;
        //                 if (found != null) {
        //                     member = Object.assign({}, { ...found._attributes }, { affiliation: user._attributes.affiliation });
        //                 } else {
        //                     member = {
        //                         jid: user._text,
        //                         affiliation: user._attributes.affiliation
        //                     }
        //                 }
        //                 return member;
        //             })
        //             //filter out members not in roster
        //             let membersNotInRoster = members.filter(member => !member.name && member.jid !== atob(atob(this.props.auth.user.mid)));
        //             console.log(membersNotInRoster);
        //             this.setState({
        //                 groupMembers: members
        //             })
        //         }
        //     })
        this.setState({
            groupChatOpen: true,
            chatItem: group
        })
    }

    handleChatOpen = item => {
        //clear message count for this friend, do it when chat close
        if (item.messageCount) {
            const roster = this.state.roster.map(friend => {
                if (friend._attributes.jid === item._attributes.jid) {
                    friend.messageCount = 0;
                }
                return friend
            });
            this.setState({
                chatOpen: true,
                chatItem: item,
                roster
            });
        } else {
            this.setState({
                chatOpen: true,
                chatItem: item
            })
        }
    }

    handleChatClose = () => {
        const { chatItem } = this.state;
        const roster = this.state.roster.map(friend => {
            if (friend._attributes.jid === chatItem._attributes.jid) {
                friend.messageCount = 0;
            }
            return friend
        });
        this.setState({
            chatOpen: false,
            chatItem: null,
            roster
        })
    }

    handleGroupChatClose = () => {
        this.setState({
            groupChatOpen: false,
            chatItem: null,
        })
    }

    getChatItemMessages = () => {
        const { messages, chatItem } = this.state;
        if (!chatItem) {
            return [];
        }
        const jid = chatItem._attributes.jid;
        const chatMessages = messages.filter(msg => msg.to === jid || msg.from === jid);
        return chatMessages;
    }

    toggleDrawer = (open) => {
        this.setState({
            drawerOpen: open
        });
    }

    handleCreateRoomClose = () => {
        this.setState({
            createRoomOpen: false
        })
    }

    handleCreateRoom = (roomname, nickname, members) => {
        console.log(roomname, nickname);
        const to = roomname + config.muc_host + "/" + nickname;
        this.conn.send(window.Strophe.$pres().c('priority').t('-1'));
        this.conn.send(window.Strophe.$pres({ to: to}).c('x', {xmlns: XMPP.NS_MUC}));
        // var occupants = document.createElement('occupants');
        // members.forEach(member => {
        //     let user = document.createElement('user');
        //     user.setAttribute('affiliation', 'member');
        //     let jid = document.createTextNode(member);
        //     user.appendChild(jid);
        //     occupants.appendChild(user);
        // })
        // console.log(occupants);
        // var iq = window.Strophe.$iq({
        //     type: "set",
        //     to: uuid() + '@muclight.localhost'
        // }).c("query", { xmlns: "urn:xmpp:muclight:0#create" })
        //     .c("configuration").c("roomname").t(roomname)
        //     .up().up()
        //     .cnode(occupants);
        // console.log(iq);
        // this.conn.sendIQ(iq, (iq) => {
        //     console.log(iq)
        // });
    }

    handleDiscoClose = () => {
        this.setState({
            discoOpen: false
        })
    }

    handleDiscovery = (service, namespace) => {
        return new Promise(resolve => {
            var discoItem = window.Strophe.$iq({
                type: "get",
                to: service
            }).c("query", { xmlns: namespace });
            // console.log(discoItem);
            this.conn.sendIQ(discoItem, (iq) => {
                const iqObj = xml2js(iq.outerHTML, options);
                resolve(iqObj);
            });
        });
    }

    handleDisco = (service, item = null) => {
        return new Promise(resolve => {
            var discoInfo = window.Strophe.$iq({
                type: "get",
                to: service
            }).c("query", { xmlns: "http://jabber.org/protocol/disco#info" });
            // console.log(discoInfo);
            this.conn.sendIQ(discoInfo, (iq) => {
                console.log(iq);
            });
            if (service.indexOf('@muclight.localhost') !== -1) {
                var usersList = window.Strophe.$iq({
                    type: "get",
                    to: service
                }).c("query", { xmlns: "urn:xmpp:muclight:0#affiliations" });
                this.conn.sendIQ(usersList, (iq) => {
                    console.log(iq);
                });
                const roster = window.Strophe.$iq({ type: "get" }).c("query", { xmlns: "jabber:iq:roster" });
                // this.conn.sendIQ(iq, this.handleRoster);
                this.conn.sendIQ(roster, (iq) => {
                    console.log(iq);
                })
            }
            var discoItem = window.Strophe.$iq({
                type: "get",
                to: service
            }).c("query", { xmlns: "http://jabber.org/protocol/disco#items" });
            // console.log(discoItem);
            this.conn.sendIQ(discoItem, (iq) => {
                console.log(iq);
                resolve(iq);
            });
        })
    }

    handleNickNameClose = () => {
        this.setState({
            nickNameOpen: false
        })
    }

    handleGroupsOpen = () => {
        this.setState({
            groupsOpen: true,
            groupLoading: true,
        })
        this.discoGroups()
            .then(iq => {
                console.log(iq);
                const iqObj = xml2js(iq.outerHTML, options);
                console.log(iqObj);
                if (iqObj.iq.query.item) {
                    if (iqObj.iq.query.item.length) {
                        this.setState({
                            groupLoading: false,
                            groups: iqObj.iq.query.item
                        })
                    } else {
                        this.setState({
                            groupLoading: false,
                            groups: [iqObj.iq.query.item]
                        })
                    }
                } else {
                    this.setState({
                        groupLoading: false,
                    })
                }
            })
    }

    handleGroupsClose = () => {
        this.setState({
            groupsOpen: false
        })
    }

    discoGroups = () => {
        //list groups
        return new Promise(resolve => {
            var discoItem = window.Strophe.$iq({
                type: "get",
                to: MSG_CONFIG.GROUP_DOMAIN,
            }).c("query", { xmlns: "http://jabber.org/protocol/disco#items" });
            console.log(discoItem);
            this.conn.sendIQ(discoItem, (iq) => {
                // console.log(iq);
                resolve(iq);
            });
        })

    }

    handleCreatePublisherClose = () => {
        this.setState({createPublisherOpen: false});
    }

    handlePubsubCreated = (iq, pubsub_name) => {
        console.log(iq, pubsub_name);
        //save this to database
        const iqObj = xml2js(iq.outerHTML, options);
        console.log(iqObj);
        const { auth, createPubsub, createPersonalPub } = this.props;
        const node = iqObj.iq.pubsub.create._attributes.node;
        const pubsub = {
            node: node,
            name: pubsub_name
        }
        if (pubsub_name) {
            //enterprise pubsub
            createPubsub(pubsub);
        }else {
            //personal pubsub
            createPersonalPub(node);
        }
        
        //node creation callbacks, configure the node
        const configiq = window.Strophe.$iq({ to: Message.PubsubDomain, type: "set"})
                        .c("pubsub", { xmlns: XMPP.NS_PUBSUB_OWNER })
                        .c("configure", { node: pubsub.node })
                        .c("x", { xmlns: XMPP.NS_DATA_FORMS, type: "submit" })
                        .c("field", { "var": "FORM_TYPE" })
                        .c("value").t(XMPP.NS_PUBSUB_NODE_CONFIG)
                        .up().up()
                        .c("field", { "var": "pubsub#deliver_payloads" })
                        .c("value").t("1")
                        .up().up()
                        .c("field", { "var": "pubsub#send_last_published_item"})
                        // .c("value").t("never")
                        .c("value").t("on_sub_and_presence")
                        .up().up()
                        .c("field", { "var": "pubsub#persist_items" })
                        .c("value").t("true")
                        .up().up()
                        .c("field", { "var": "pubsub#max_items" })
                        .c("value").t("10");
        this.conn.sendIQ(configiq, this.handlePubsubConfigured, this.handlePubsubConfigureError);
    }

    handlePubsubConfigure = () => {
        const { auth } = this.props;
        const node = auth.user.pubs[0].node;
        //node creation callbacks, configure the node
        const configiq = window.Strophe.$iq({ to: Message.PubsubDomain, type: "set"})
                        .c("pubsub", { xmlns: XMPP.NS_PUBSUB_OWNER })
                        .c("configure", { node:  node})
                        .c("x", { xmlns: XMPP.NS_DATA_FORMS, type: "submit" })
                        .c("field", { "var": "FORM_TYPE" })
                        .c("value").t(XMPP.NS_PUBSUB_NODE_CONFIG)
                        .up().up()
                        .c("field", { "var": "pubsub#deliver_payloads" })
                        .c("value").t("1")
                        .up().up()
                        .c("field", { "var": "pubsub#send_last_published_item"})
                        // .c("value").t("never")
                        .c("value").t("on_sub_and_presence")
                        .up().up()
                        .c("field", { "var": "pubsub#persist_items" })
                        .c("value").t("true")
                        .up().up()
                        .c("field", { "var": "pubsub#max_items" })
                        .c("value").t("10");
        this.conn.sendIQ(configiq, this.handlePubsubConfigured, this.handlePubsubConfigureError);
    }

    handlePubsubConfigured = (iq) => {
        console.log("Configured ", iq);
        //retrieve configure
        const { auth } = this.props;
        if (auth.user.pubs.length) {
            let node = auth.user.pubs[0].node;
            const configiq = window.Strophe.$iq({ to: Message.PubsubDomain, type: "get"})
                            .c("pubsub", { xmlns: XMPP.NS_PUBSUB_OWNER })
                            .c("configure", { node: node});
            this.conn.sendIQ(configiq, (iq) => {
                console.log("Retrieve configure", iq);
            })
        }
    }

    handlePubsubConfigureError = (iq) => {
        console.log("Configure error", iq);
    }

    handlePubsubCreatedError = (iq) => {
        console.log('Create pubsub error ', iq);
    }

    handleCreatePublisher = (pubsub_name) => {
        //E73FFEA5D25A265A
        console.log(pubsub_name);
        var iq = window.Strophe.$iq({
            to: Message.PubsubDomain,
            type: 'set'
        }).c('pubsub', { xmlns: XMPP.NS_PUBSUB })
        .c('create');
        this.conn.sendIQ(iq, (iq) => {
            this.handlePubsubCreated(iq, pubsub_name);
        }, this.handlePubsubCreatedError);
    }

    handleCreatePostClose = () => {
        this.setState({
            createPostOpen: false
        })
    }

    handleCreateStoryClose = () => {
        this.setState({
            createStoryOpen: false
        })
    }

    handleCreateStory = () => {
        //create story is basically for personal pub
        //slim text, more photos, more casual post
        const { auth } = this.props;
        const story = this.storyForm.getFieldsValue();
        console.log(story);
        let iq = null;
        const node = auth.user.node;
        if (story.images && story.images.fileList && story.images.fileList.length) {
            //has pictures
            const images = story.images.fileList.map(item => {
                return item.response.url;
            });
            console.log(images);
            const imagesStr = images.join(',');
            console.log(imagesStr);
            iq = window.Strophe.$iq({ to: Message.PubsubDomain, type: "set"})
                    .c("pubsub", { xmlns: XMPP.NS_PUBSUB})
                    .c("publish", { node: node })
                    .c("item")
                    .c('x', { xmlns: XMPP.NS_DATA_FORMS, type: "result" })
                    .c("field", {"var": "highlight"})
                    .c("value").t(story.highlight)
                    .up().up()
                    .c("field", {"var": "from"})
                    .c("value").t(atob(atob(auth.user.mid)))
                    .up().up()
                    .c("field", {"var": "avatar"})
                    .c("value").t(auth.user.avatar)
                    .up().up()
                    .c("field", {"var": "timestamp"})
                    .c("value").t(new Date().getTime())
                    .up().up()
                    .c("field", {"var": "images"})
                    .c("value").t(imagesStr);
        } else {
            iq = window.Strophe.$iq({ to: Message.PubsubDomain, type: "set"})
                    .c("pubsub", { xmlns: XMPP.NS_PUBSUB})
                    .c("publish", { node: node })
                    .c("item")
                    .c('x', { xmlns: XMPP.NS_DATA_FORMS, type: "result" })
                    .c("field", {"var": "highlight"})
                    .c("value").t(story.highlight)
                    .up().up()
                    .c("field", {"var": "from"})
                    .c("value").t(atob(atob(auth.user.mid)))
                    .up().up()
                    .c("field", {"var": "avatar"})
                    .c("value").t(auth.user.avatar)
                    .up().up()
                    .c("field", {"var": "timestamp"})
                    .c("value").t(new Date().getTime());
        }
        this.conn.sendIQ(iq);
    }

    handleCreatePost = () => {
        //create post is for enterprise pub
        //well structured text and nice picture
        const { auth } = this.props;
        const post = this.postForm.getFieldsValue();
        const pubsub = auth.user.pubs[0];
        console.log(pubsub, post);
        let iq = null;
        if (post.images) {
            iq = window.Strophe.$iq({ to: Message.PubsubDomain, type: "set"})
                    .c("pubsub", { xmlns: XMPP.NS_PUBSUB})
                    .c("publish", { node: pubsub.node })
                    .c("item")
                    .c('x', { xmlns: XMPP.NS_DATA_FORMS, type: "result" })
                    .c("field", {"var": "subject"})
                    .c("value").t(post.subject)
                    .up().up()
                    .c("field", {"var": "highlight"})
                    .c("value").t(post.highlight)
                    .up().up()
                    .c("field", {"var": "detail"})
                    .c("value").t(post.detail)
                    .up().up()
                    .c("field", {"var": "timestamp"})
                    .c("value").t(new Date().getTime())
                    .up().up()
                    .c("field", {"var": "image"})
                    .c("value").t(post.images.file.response.url);
        } else {
        iq = window.Strophe.$iq({ to: Message.PubsubDomain, type: "set"})
                    .c("pubsub", { xmlns: XMPP.NS_PUBSUB})
                    .c("publish", { node: pubsub.node })
                    .c("item")
                    .c('x', { xmlns: XMPP.NS_DATA_FORMS, type: "result" })
                    .c("field", {"var": "subject"})
                    .c("value").t(post.subject)
                    .up().up()
                    .c("field", {"var": "highlight"})
                    .c("value").t(post.highlight)
                    .up().up()
                    .c("field", {"var": "detail"})
                    .c("value").t(post.detail)
                    .up().up()
                    .c("field", {"var": "timestamp"})
                    .c("value").t(new Date().getTime())
                    .up().up();
                    // .c("field", {"var": "image"})
                    // .c("value").t(post.images.file.response.url);
        }
        console.log(iq);
        this.conn.sendIQ(iq);
    }

    handleSubscribeClose = () => {
        this.setState({
            subscribeOpen: false
        })
    }

    handlePersonalSubscribe = node => {
        const { auth } = this.props;
        console.log("Subscribe to personal", node);
        //now subscribe
        var subiq = window.Strophe.$iq({ to: Message.PubsubDomain, type: "set" })
                    .c("pubsub", { xmlns: XMPP.NS_PUBSUB })
                    .c("subscribe", {node: node, jid: atob(atob(auth.user.mid))});
        console.log(subiq);
        this.conn.sendIQ(subiq, this.handlePersonalSubscribed, this.handleSubscribedError);
    }

    handlePersonalSubscribed = iq => {
        console.log("Subscribed to pernal", iq);
    }

    handleCreateSubscribe = (publisher) => {
        const { auth } = this.props;
        console.log("Subscribe to ", publisher);
        //now subscribe
        var subiq = window.Strophe.$iq({ to: Message.PubsubDomain, type: "set" })
                    .c("pubsub", { xmlns: XMPP.NS_PUBSUB })
                    .c("subscribe", {node: publisher.value, jid: atob(atob(auth.user.mid))});
        console.log(subiq);
        this.conn.sendIQ(subiq, (iq) => {
            this.handleSubscribed(iq, publisher);
        }, this.handleSubscribedError);
    }

    handleSubscribed = (iq, publisher) => {
        console.log("Subscribed", iq);
        console.log(publisher);
        //subscribe to redux
        const { createSub } = this.props;
        createSub({ node: publisher.value, name: publisher.label, avatar: publisher.avatar });
    }

    handleSubscribedError = iq => {
        console.log("Subscribed error",iq);
    }

    handleSubscribeMessagesClose = () => {
        this.setState({
            subscribeMessagesOpen: false
        })
    }

    handleSubscribeMessagesOpen = () => {
        const { readSubs } = this.props;
        const { subscribeMessages } = this.state;
        subscribeMessages.count = 0;
        this.setState({
            subscribeMessagesOpen: true,
            subscribeMessages
        });
        readSubs(subscribeMessages.messages);
    }

    handleSubscribeFriendsMessagesOpen = () => {
        const { readSubs } = this.props;
        const { subscribeFriendsMessages } = this.state;
        subscribeFriendsMessages.count = 0;
        this.setState({
            subscribeFriendsMessagesOpen: true,
            subscribeFriendsMessages
        });
        readSubs(subscribeFriendsMessages.messages);
    }

    handleSubscribeFriendsMessagesClose = () => {
        this.setState({
            subscribeFriendsMessagesOpen: false,
        })
    }

    handleRetrieveSubscribers = () => {
        const { auth } = this.props;
        const iq = window.Strophe.$iq({ to: Message.PubsubDomain, type: "get"})
                    .c("pubsub", { xmlns: XMPP.NS_PUBSUB_OWNER })
                    .c('subscriptions', { node: auth.user.node });
        this.conn.sendIQ(iq, (iq) => {
            console.log("Retrieve subscriptions ", iq);
        })
    }

    handleMyAccountClose = () => {
        this.setState({
            myAccountOpen: false
        });
    }

    render() {
        const { roster, addOpen, snackOpen, pendingSubscribers, chatOpen, chatItem, composingMessage, drawerOpen, subscribeMessages, subscribeFriendsMessages } = this.state;
        const { classes, auth, intl } = this.props;
        const messages = this.getChatItemMessages();
        return (
            <div className={classes.demo}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu"
                            onClick={() => this.toggleDrawer(true)}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            My Network
                                </Typography>
                        <Button className={classes.appBarRightButton} color="inherit" onClick={this.handleAddOpen}>
                            <PersonAddIcon />
                        </Button>
                    </Toolbar>
                </AppBar>
                <SwipeableDrawer
                    open={drawerOpen}
                    onOpen={() => this.toggleDrawer(true)}
                    onClose={() => this.toggleDrawer(false)}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={() => this.toggleDrawer(false)}
                        onKeyDown={() => this.toggleDrawer(false)}>
                        <div className={classes.header}>
                            {
                                auth.user && auth.user.avatar ?
                                    <Avatar alt="test" src={auth.user.avatar} className={classNames(classes.avatar, classes.bigAvatar)} />
                                    : intl.formatMessage({ id: "app.name" })
                            }

                        </div>
                        <Divider />
                        <div className={classes.list}>
                            <List>
                                {/* <ListItem button onClick={() => this.setState({ createRoomOpen: true })}>
                                    <ListItemIcon>
                                        <CreateRoomIcon />
                                    </ListItemIcon>
                                    <ListItemText primary={<IntlMessage id="me.topbar.createRoom" />} />
                                </ListItem> */}
                                {
                                    auth.user.pubs && auth.user.pubs.length ? 
                                    <ListItem button onClick={() => this.setState({ createPostOpen: true })}>
                                        <ListItemIcon>
                                            <PublisherIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Create Post" />
                                    </ListItem>
                                    :
                                    <ListItem button onClick={() => this.setState({ createPublisherOpen: true })}>
                                        <ListItemIcon>
                                            <PublisherIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Create Publisher" />
                                    </ListItem>
                                }
                                <ListItem button onClick={() => this.setState({ subscribeOpen: true })}>
                                    <ListItemIcon>
                                        <SubscribeIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Subscribe" />
                                </ListItem>
                                <ListItem button onClick={() => this.setState({ createStoryOpen: true })}>
                                    <ListItemIcon>
                                        <AddCircleIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Add story" />
                                </ListItem>
                                <ListItem button onClick={() => this.setState({ myAccountOpen: true })}>
                                    <ListItemIcon>
                                        <AccountCircle />
                                    </ListItemIcon>
                                    <ListItemText primary="My Account" />
                                </ListItem>
                                {/* <ListItem button onClick={this.handleRetrieveSubscribers}>
                                    <ListItemIcon>
                                        <SubscribersListIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="List Subscribers" />
                                </ListItem>
                                <ListItem button onClick={this.handlePubsubConfigure}>
                                    <ListItemIcon>
                                        <SettingsIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Configure Pubsub" />
                                </ListItem>
                                <ListItem button onClick={() => this.setState({ discoOpen: true })}>
                                    <ListItemIcon>
                                        <DiscoIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Discovery" />
                                </ListItem>*/}
                            </List>
                            <Divider />
                        </div>
                    </div>
                </SwipeableDrawer>
                {
                    pendingSubscribers.length ?
                        <List
                            component="nav"
                            subheader={<ListSubheader component="div">Invitations</ListSubheader>}
                        >
                            {
                                pendingSubscribers.map(item => {
                                    return (
                                        <ListItem key={item}>
                                            <ListItemText primary={item} />
                                            <ListItemSecondaryAction>
                                                <IconButton aria-label="Reject" onClick={() => this.handleSubscribeReject(item)}>
                                                    <CloseIcon />
                                                </IconButton>
                                                <IconButton aria-label="Approve" onClick={() => this.handleSubscribeApprove(item)}>
                                                    <CheckIcon />
                                                </IconButton>
                                            </ListItemSecondaryAction>
                                        </ListItem>
                                    );
                                })
                            }
                        </List>
                        : null
                }

                <List>
                    <ListItem key="subscribers" dense button onClick={this.handleSubscribeMessagesOpen}>
                        {
                            subscribeMessages.count ? 
                            <Badge badgeContent={subscribeMessages.count} color="primary">
                                <Avatar className={classes.groupAvatar}>
                                    <SubscribeIcon />
                                </Avatar>
                            </Badge>
                            :
                            <Avatar className={classes.groupAvatar}>
                                <SubscribeIcon />
                            </Avatar>
                        }
                        
                        <ListItemText primary="Subscribers" />
                        <ListItemSecondaryAction>
                            <IconButton onClick={this.handleSubscribeMessagesOpen}>
                                <ArrowRightIcon />
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem key="subscribers-friends" dense button onClick={this.handleSubscribeFriendsMessagesOpen}>
                        {
                            subscribeFriendsMessages.count ? 
                            <Badge badgeContent={subscribeFriendsMessages.count} color="primary">
                                <Avatar className={classes.groupAvatar}>
                                    <FriendsHotIcon />
                                </Avatar>
                            </Badge>
                            :
                            <Avatar className={classes.groupAvatar}>
                                <FriendsHotIcon />
                            </Avatar>
                        }
                        
                        <ListItemText primary="Friends Hot" />
                        <ListItemSecondaryAction>
                            <IconButton onClick={this.handleSubscribeFriendsMessagesOpen}>
                                <ArrowRightIcon />
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider />
                    {
                        roster.map(item => {
                            const name = item._attributes.name || this.getUserFromJid(item._attributes.jid);
                            return (
                                <ListItem key={item._attributes.jid} dense button onClick={() => this.handleChatOpen(item)}>

                                    {
                                        item.messageCount ?
                                            <Badge badgeContent={item.messageCount} color="primary">
                                                <Avatar className={item._attributes.status === 'online' ? classes.avatar : ''}>
                                                    <PersonIcon />
                                                </Avatar>
                                            </Badge>
                                            :
                                            <Avatar className={item._attributes.status === 'online' ? classes.avatar : ''}>
                                                <PersonIcon />
                                            </Avatar>
                                    }

                                    <ListItemText
                                        primary={name}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton onClick={() => this.handleChatOpen(item)}>
                                            <ArrowRightIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            );
                        })
                    }
                </List>
                <Dialog
                    fullScreen
                    open={chatOpen}
                    onClose={this.handlChatClose}
                    TransitionComponent={Transition}>
                    <AppBar className={classes.appBar}>
                        <Toolbar>
                            <IconButton color="inherit" onClick={this.handleChatClose}>
                                <BackIcon />
                            </IconButton>
                            <Typography variant="title" color="inherit" className={classes.flex}>
                                {this.state.chatItem ? this.state.chatItem._attributes.name : null}
                            </Typography>
                            <MUIButton className={classes.appBarRightButton} color="inherit">
                                <AccountCircle />
                            </MUIButton>
                        </Toolbar>

                    </AppBar>
                    <div className={classes.chatArea}>
                        <div className={classes.messageList} ref={messageList => this.messageList = messageList}>
                            <List>
                                {
                                    messages.map((msg, index) => {
                                        if (msg.to === chatItem._attributes.jid) {
                                            return (
                                                <ListItem key={index} className={classes.messageItemMe}>
                                                    <Avatar>
                                                        <PersonIcon />
                                                    </Avatar>
                                                    <div className={classes.messageBox}>
                                                        <div className={classes.messageBoxMe}>
                                                            <p className={classes.message}>{msg.body}</p>
                                                        </div>
                                                    </div>
                                                </ListItem>
                                            )
                                        } else if (msg.from === chatItem._attributes.jid) {
                                            return (
                                                <ListItem key={index}>
                                                    <Avatar className={classes.avatar}>
                                                        <PersonIcon />
                                                    </Avatar>
                                                    <div className={classes.messageBox}>
                                                        <div className={classes.messageBoxFriend}>
                                                            <p className={classes.message}>{msg.body}</p>
                                                        </div>
                                                    </div>
                                                </ListItem>
                                            )
                                        }
                                    })
                                }
                                {
                                    composingMessage && composingMessage.fromBareJid === chatItem._attributes.jid ?
                                        <ListItem key={composingMessage.timestamp}>
                                            <Avatar className={classes.avatar}>
                                                <PersonIcon />
                                            </Avatar>
                                            <div className={classes.messageBox}>
                                                <div className={classes.messageBoxFriendComposing}>
                                                    <p className={classes.message}>typing...</p>
                                                </div>
                                            </div>
                                        </ListItem>
                                        : null
                                }
                            </List>
                        </div>
                        <div className={classes.messageSendContainer}>
                            <TextField
                                id="multiline-flexible"
                                placeholder="Write a message"
                                multiline
                                rowsMax="2"
                                name="message"
                                value={this.state.message}
                                onChange={this.handleInputMessage}
                                className={classes.messageText}
                                margin="normal"
                            />
                            <div className={classes.messageButtons}>
                                <Grid container>
                                    <Grid item xs={8}>
                                        <IconButton>
                                            <PhotoCamera />
                                        </IconButton>
                                        <IconButton>
                                            <VoiceIcon />
                                        </IconButton>
                                        <IconButton>
                                            <ImageIcon />
                                        </IconButton>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <Button variant="contained" color="primary" onClick={this.sendMessage}>
                                            Send
                                        <SendIcon className={classes.rightIcon} />
                                        </Button>
                                    </Grid>
                                </Grid>
                            </div>
                        </div>
                    </div>
                </Dialog>
                <AddFriendDialog open={addOpen} handleClose={this.handleAddClose} handleAdd={this.handleAddContact} />
                <CreateRoomDialog roster={roster} open={this.state.createRoomOpen} handleClose={this.handleCreateRoomClose} handleCreateRoom={this.handleCreateRoom} />
                <CreatePublisherDialog open={this.state.createPublisherOpen} handleClose={this.handleCreatePublisherClose} handleCreatePublisher={this.handleCreatePublisher}/>
                <CreatePostDialog ref={form => this.postForm = form} pubsub={auth.user.pubs.length ? auth.user.pubs[0] : ''} token={auth.oauth.access_token} open={this.state.createPostOpen} handleClose={this.handleCreatePostClose} handleCreate={this.handleCreatePost}/>
                <SubscribeDialog open={this.state.subscribeOpen} handleClose={this.handleSubscribeClose} handleCreate={this.handleCreateSubscribe}/>
                <DiscoDialog open={this.state.discoOpen} handleClose={this.handleDiscoClose} handleDisco={this.handleDisco} />
                <NickNameDialog username={this.state.subscribeBack} open={this.state.nickNameOpen} handleClose={this.handleNickNameClose} handleSubscribeBack={this.handleSubscribeBack} />
                <GroupChatDialog myself={atob(atob(auth.user.mid))} open={this.state.groupChatOpen} group={this.state.chatItem} members={this.state.groupMembers} groupLoading={this.state.groupLoading} roster={roster} handleClose={this.handleGroupChatClose}
                    sendGroupMessage={this.sendGroupMessage} 
                    messages={this.state.groupsMessages}/>
                <SubscribeMessages messages={subscribeMessages.messages} open={this.state.subscribeMessagesOpen} handleClose={this.handleSubscribeMessagesClose} 
                    handleHistory={this.handleSubscribeOldMessages}/>
                <SubscribeFriendsMessages messages={subscribeFriendsMessages.messages} open={this.state.subscribeFriendsMessagesOpen} handleClose={this.handleSubscribeFriendsMessagesClose} 
                    />
                <CreateStoryDialog ref={form => this.storyForm = form} token={auth.oauth.access_token} open={this.state.createStoryOpen} handleClose={this.handleCreateStoryClose} handleCreate={this.handleCreateStory}/>
                <MyAccountDialog open={this.state.myAccountOpen} user={auth.user} handleClose={this.handleMyAccountClose}/>
            </div>
        );
    }
}

Me.propTypes = {
    classes: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    return {
        auth: state.Auth.toJS(),
        sub: state.Sub.toJS(),
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createPubsub(pubsub) {
            dispatch(authActions.createPubsub(pubsub));
        },
        createSub(sub) {
            dispatch(authActions.subCreate(sub));
        },
        fetchSubs() {
            dispatch(authActions.subsFetch());
        },
        createPersonalPub(node) {
            dispatch(authActions.personalPubCreate(node));
        },
        readSubs(subs) {
            dispatch(subActions.subsRead(subs));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(injectIntl(Me)));