import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ChevronLeft';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Transition from '../../component/Transition';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import CircularProgress from '@material-ui/core/CircularProgress';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';
import PersonIcon from '@material-ui/icons/Person';
import ArrowRightIcon from '@material-ui/icons/ChevronRight';
import red from '@material-ui/core/colors/red';
import UploadAvatarDialog from './UploadAvatarDialog';

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
        marginLeft: -48,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    container: {
        padding: 0,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
    formControl: {
        margin: theme.spacing.unit,
        width: '100%',
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit / 4,
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
    avatar: {
        backgroundColor: red[300],
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        }
    }
};

class MyAccountDialog extends Component {

    state = {
        uploadOpen: false,
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        // console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    handleClose = () => {
        const { handleClose } = this.props;
        if (handleClose != null) {
            handleClose();
        }
    }


    handleUploadAvatarClose = () => {
        this.setState({
            uploadOpen: false,
        })
    }

    handleUploadAvatarOpen = () => {
        this.setState({
            uploadOpen: true,
        })
    }

    render() {
        const { open, classes, theme, roster, groups, groupLoading, user } = this.props;
        const { uploadOpen } = this.state;

        return (
            <Dialog
                fullScreen
                open={open}
                onClose={this.handleClose}
                TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton color="inherit" onClick={this.handleClose}>
                            <BackIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            My Account
                    </Typography>

                    </Toolbar>
                </AppBar>

                <div className={classes.container}>

                    <List>
                        <ListItem key={user.mid} dense button onClick={this.handleUploadAvatarOpen}>

                            {
                                user.avatar ?
                                    <Avatar className={classes.avatar} src={user.avatar} />
                                    :
                                    <Avatar className={classes.avatar}>
                                        <PersonIcon />
                                    </Avatar>
                            }

                            <ListItemText
                                primary="ID"
                                secondary={atob(atob(user.mid)).substring(0, atob(atob(user.mid)).indexOf('@'))}
                            />
                            <ListItemSecondaryAction>
                                <IconButton>
                                    <ArrowRightIcon />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    </List>

                </div>
                <UploadAvatarDialog open={uploadOpen} handleClose={this.handleUploadAvatarClose}/>
            </Dialog>);
    }
}

MyAccountDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(MyAccountDialog);