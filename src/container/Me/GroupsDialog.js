import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ChevronLeft';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Transition from '../../component/Transition';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import CircularProgress from '@material-ui/core/CircularProgress';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';
import PersonIcon from '@material-ui/icons/Person';
import ArrowRightIcon from '@material-ui/icons/ChevronRight';
import red from '@material-ui/core/colors/red';
import GroupChatDialog from './GroupChatDialog';
import * as XMPP from '../../constant/xmpp';
import { xml2js } from 'xml-js';

const options = { compact: true, ignoreComment: true, alwaysChildren: true };

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
        marginLeft: -48,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    container: {
        padding: 0,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
    formControl: {
        margin: theme.spacing.unit,
        width: '100%',
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit/4,
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
    avatar: {
        backgroundColor: red[300],
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        }
    }
};

class GroupsDialog extends Component {
    state = {
        groupChatOpen: false,
        group: null,
        groupMembers: [],
    }
    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        // console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    handleClose = () => {
        const { handleClose } = this.props;
        if (handleClose != null) {
            handleClose();
        }
    }

    handleGroupChatOpen = (group) => {
        const { handleDiscovery, roster, myself } = this.props;
        if (handleDiscovery != null) {
            handleDiscovery(group._attributes.jid, XMPP.NS_MUC_LIGHT_AFFILIATIONS)
            .then(iq => {
                console.log(iq);
                const users = iq.iq.query.user;
                if (users.length) {
                    //get username from roster first, if not exist
                    //get username from backend database
                    let members = users.map(user => {
                        const found = roster.find(item => user._text === item._attributes.jid);
                        let member = null;
                        if (found != null) {
                            member = Object.assign({}, {...found._attributes}, {affiliation: user._attributes.affiliation});
                        } else {
                            member = {
                                jid: user._text,
                                affiliation: user._attributes.affiliation
                            }
                        }
                        return member;
                    })
                    //filter out members not in roster
                    let membersNotInRoster = members.filter(member => !member.name && member.jid !== myself);
                    console.log(membersNotInRoster);
                    this.setState({
                        groupMembers: members
                    })
                }
            })
        }
        this.setState({
            groupChatOpen: true,
            group: group
        })
    }

    handleGroupChatClose = () => {
        this.setState({
            groupChatOpen: false,
            group: null,
        })
    }

    render() {
        const { open, classes, theme, roster, groups, groupLoading, sendGroupMessage } = this.props;
        const { groupChatOpen, group, groupMembers } = this.state;
        return (
        <Dialog
            fullScreen
            open={open}
            onClose={this.handleClose}
            TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton color="inherit" onClick={this.handleClose}>
                        <BackIcon />
                    </IconButton>
                    <Typography variant="title" color="inherit" className={classes.flex}>
                        Groups
                    </Typography>
                    
                </Toolbar>
            </AppBar>

            <div className={classes.container}>
            {
                groupLoading ? 
                <CircularProgress className={classes.progress} />
                :
                <List>
                {
                    groups.map(item => {
                        const name = item._attributes.name;
                        return (
                            <ListItem key={item._attributes.jid} dense button onClick={() => this.handleGroupChatOpen(item)}>
                            
                                        {
                                            item.messageCount ? 
                                            <Badge badgeContent={item.messageCount} color="primary">
                                                <Avatar className={classes.avatar}>
                                                    <PersonIcon />
                                                </Avatar>
                                            </Badge>
                                            :
                                            <Avatar className={classes.avatar}>
                                                    <PersonIcon />
                                            </Avatar>
                                        }
                                        
                                    <ListItemText
                                        primary={name}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton onClick={() => this.handleGroupChatOpen(item)}>
                                            <ArrowRightIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                            </ListItem>
                        )
                    })
                }
                </List>
            }    
            </div>
            <GroupChatDialog open={groupChatOpen} group={group} handleClose={this.handleGroupChatClose} members={groupMembers} sendGroupMessage={sendGroupMessage} />
        </Dialog>);
    }
}

GroupsDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true})(GroupsDialog);