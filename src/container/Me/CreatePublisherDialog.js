import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ChevronLeft';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/core/styles';
import Transition from '../../component/Transition';
import PropTypes from 'prop-types';

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
        marginLeft: -48,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    container: {
        padding: 25,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
    formControl: {
        margin: theme.spacing.unit,
        width: '100%',
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit/4,
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        }
    }
};

class CreatePublisherDialog extends Component {
    state = {
        snackOpen: false,
        pubsub: '',
    }

    handleCreate = () => {
        const { handleCreatePublisher } = this.props;
        if (handleCreatePublisher != null) {
            const {pubsub} = this.state;
            if (pubsub != null) {
                handleCreatePublisher(pubsub);
                this.setState({
                    snackOpen: true
                });
            }
        }
    }

    handleSnackClose = () => {
        this.setState({
            snackOpen: false
        })
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        // console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    handleClose = () => {
        const { handleClose } = this.props;
        if (handleClose != null) {
            handleClose();
        }
    }

    render() {
        const { open, classes, theme } = this.props;
        const { snackOpen } = this.state;
        return (
        <Dialog
            fullScreen
            open={open}
            onClose={this.handleClose}
            TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton color="inherit" onClick={this.handleClose}>
                        <BackIcon />
                    </IconButton>
                    <Typography variant="title" color="inherit" className={classes.flex}>
                        Create Publisher
                    </Typography>
                    
                </Toolbar>
            </AppBar>

            <div className={classes.container}>
            <Grid container>
                <Grid item xs={12}>
                    <TextField
                        name="pubsub"
                        id="pubsub"
                        label="Publisher name"
                        placeholder="Enter publisher name"
                        className={classes.textField}
                        margin="normal"
                        onChange={this.handleUserInput}
                    />
                </Grid>
                {/* <Grid item xs={12}>
                    <TextField
                        name="alias"
                        id="alias"
                        label="Nick name"
                        placeholder="Enter nickname"
                        className={classes.textField}
                        margin="normal"
                        onChange={this.handleUserInput}
                    />
                </Grid> */}
                <Grid item xs={12}>
                    <Button variant="contained" color="primary" className={classes.button} onClick={this.handleCreate}>
                        Create
                        </Button>
                </Grid>
            </Grid>
            <Snackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                open={snackOpen}
                onClose={this.handleSnackClose}
                ContentProps={{ 'aria-describedby': 'message-id', }}
                autoHideDuration={3000}
                message={<span id="message-id">Add request has been sent!</span>}
            />
            </div>
        </Dialog>);
    }
}

CreatePublisherDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true})(CreatePublisherDialog);