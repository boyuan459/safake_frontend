import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ChevronLeft';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/core/styles';
import Transition from '../../component/Transition';
import PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
        marginLeft: -48,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    container: {
        padding: 25,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
    formControl: {
        margin: theme.spacing.unit,
        width: '100%',
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit/4,
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        }
    }
};

class CreateRoomDialog extends Component {
    state = {
        snackOpen: false,
        members: [],
    }

    handleCreateRoom = () => {
        const { handleCreateRoom } = this.props;
        if (handleCreateRoom != null) {
            const {roomname, nickname, members} = this.state;
            if (roomname != null && nickname != null) {
                handleCreateRoom(roomname, nickname, members);
                this.setState({
                    snackOpen: true
                });
            }
        }
    }

    handleSnackClose = () => {
        this.setState({
            snackOpen: false
        })
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        // console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    handleClose = () => {
        const { handleClose } = this.props;
        if (handleClose != null) {
            handleClose();
        }
    }

    render() {
        const { open, classes, theme, roster } = this.props;
        const { snackOpen, members } = this.state;
        return (
        <Dialog
            fullScreen
            open={open}
            onClose={this.handleClose}
            TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton color="inherit" onClick={this.handleClose}>
                        <BackIcon />
                    </IconButton>
                    <Typography variant="title" color="inherit" className={classes.flex}>
                        Create Room
                    </Typography>
                    
                </Toolbar>
            </AppBar>

            <div className={classes.container}>
            <Grid container>
                <Grid item xs={12}>
                    <TextField
                        name="roomname"
                        id="roomname"
                        label="Room name"
                        placeholder="Enter room name"
                        className={classes.textField}
                        margin="normal"
                        onChange={this.handleUserInput}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        name="nickname"
                        id="nickname"
                        label="Nick name"
                        placeholder="Enter nickname"
                        className={classes.textField}
                        margin="normal"
                        onChange={this.handleUserInput}
                    />
                </Grid>
                <Grid item xs={12}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="select-members">Members</InputLabel>
                        <Select
                            multiple
                            name="members"
                            value={members}
                            onChange={this.handleUserInput}
                            input={<Input id="select-members" />}
                            renderValue={selected => (
                                <div className={classes.chips}>
                                    {selected.map(value => {
                                        console.log(value);
                                        let member = roster.find(item => item._attributes.jid === value);
                                        return (
                                            member ? 
                                            <Chip key={value} label={member._attributes.name} className={classes.chip}/>
                                            : null
                                        )
                                    })}
                                </div>
                            )}
                            MenuProps={MenuProps}>
                            {
                                roster.map(member => {
                                    let chosen = members.find(item => item === member._attributes.jid);
                                    return (
                                        <MenuItem
                                            key={member._attributes.jid}
                                            value={member._attributes.jid}
                                            style={{
                                                fontWeight:
                                                    chosen ? theme.typography.fontWeightRegular
                                                        : theme.typography.fontWeightMedium
                                            }}>
                                            {member._attributes.name}
                                        </MenuItem>
                                    );
                                })
                            }
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="primary" className={classes.button} onClick={this.handleCreateRoom}>
                        Create
                        </Button>
                </Grid>
            </Grid>
            <Snackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                open={snackOpen}
                onClose={this.handleSnackClose}
                ContentProps={{ 'aria-describedby': 'message-id', }}
                autoHideDuration={3000}
                message={<span id="message-id">Add request has been sent!</span>}
            />
            </div>
        </Dialog>);
    }
}

CreateRoomDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true})(CreateRoomDialog);