import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ChevronLeft';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/core/styles';
import Transition from '../../component/Transition';
import PropTypes from 'prop-types';
import { searchUser } from '../../services/auth';
import debounce from 'lodash/debounce';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import CircularProgress from '@material-ui/core/CircularProgress';

function getSuggestionValue(suggestion) {
    return suggestion.label;
}

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
        marginLeft: -48,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    container: {
        padding: 25,
    },
    textField: {
        // marginLeft: theme.spacing.unit,
        // marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
    formControl: {
        margin: theme.spacing.unit,
        width: '100%',
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
    autoContainer: {
        flexGrow: 1,
        position: 'relative',
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
});

class AddFriendDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            snackOpen: false,
            error: {
                username: false,
                nickname: false,
            },
            message: '',
            user: null,
            users: [],
            fetching: false,
            value: '',
            suggestions: [],
            isSelected: false,
        }
        this.lastFetchId = 0;
        this.fetchUser = debounce(this.fetchUser, 300);
    }

    handleAddContact = () => {
        const { handleAdd } = this.props;
        const { user, nickname, error, value } = this.state;
        if (handleAdd != null) {
            let isError = false;
            if (!user || !value) {
                isError = true;
                error.username = true;
            }
            if (!nickname) {
                isError = true;
                error.nickname = true;
            }
            this.setState({
                error
            })
            if (isError) {
                return;
            }

            console.log(user.value, nickname);
            handleAdd(user.value, nickname);
            this.setState({
                snackOpen: true
            })
        }
    }

    handleSnackClose = () => {
        this.setState({
            snackOpen: false
        })
    }

    handleUserInput = e => {
        const { error } = this.state;
        const name = e.target.name;
        const value = e.target.value;
        error[name] = false;
        console.log(name, value);
        this.setState({
            [name]: value,
            error
        })
    }

    handleClose = () => {
        const { handleClose } = this.props;
        if (handleClose != null) {
            handleClose();
        }
    }

    fetchUser = value => {
        console.log(value);
        if (!value || this.state.isSelected) {
            return;
        }
        this.lastFetchId += 1;
        const fetchId = this.lastFetchId;
        const { error } = this.state;
        error.username = false;
        this.setState({
            users: [],
            fetching: true,
            error
        });
        searchUser(value)
            .then(result => {
                console.log(result);
                if (fetchId !== this.lastFetchId) {
                    return;
                }
                const users = result.map(item => ({
                    label: item.name + ' (' + item.mid + ')',
                    value: item.mid,
                    avatar: item.avatar
                }))
                const suggestions = this.getSuggestions(value, users);
                this.setState({
                    users: users,
                    suggestions: suggestions,
                    fetching: false
                })
            }).catch(error => {
                console.log(error);
                this.setState({
                    users: [],
                    fetching: false
                })
            });

    }

    handleUserChange = value => {
        console.log(value);
        this.setState({
            user: value,
            users: [],
            fetching: false
        });
    }

    renderSuggestion = (suggestion, { query, isHighlighted }) => {
        const matches = match(suggestion.label, query);
        const parts = parse(suggestion.label, matches);

        return (
            <MenuItem selected={isHighlighted} component="div">
                <div>
                    {parts.map((part, index) => {
                        return part.highlight ? (
                            <span key={String(index)} style={{ fontWeight: 500 }}>
                                {part.text}
                            </span>
                        ) : (
                                <strong key={String(index)} style={{ fontWeight: 300 }}>
                                    {part.text}
                                </strong>
                            );
                    })}
                </div>
            </MenuItem>
        );
    }

    renderInput = (inputProps) => {
        const { classes, ref, ...other } = inputProps;
        const { error } = this.state;

        return (
            <TextField
                fullWidth
                label='Username'
                error={error.username}
                InputProps={{
                    inputRef: ref,
                    classes: {
                        input: classes.textField,
                    },
                    ...other,
                }}
            />
        );
    }

    renderSuggestionsContainer = (options) => {
        const { containerProps, children } = options;
        const { classes } = this.props;

        return (
            <Paper {...containerProps} square>
                {
                    this.state.fetching ?
                        <CircularProgress className={classes.progress} />
                        :
                        children
                }
            </Paper>
        );
    }

    getSuggestions = (value, suggestions) => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
        let count = 0;

        return inputLength === 0 || suggestions.length === 0
            ? []
            : suggestions.filter(suggestion => {
                const keep =
                    count < 5 && suggestion.label.toLowerCase().slice(0, inputLength) === inputValue;

                if (keep) {
                    count += 1;
                }

                return keep;
            });
    }

    handleSuggestionsFetchRequested = ({ value }) => {
        const suggestions = this.state.users;
        this.setState({
            suggestions: this.getSuggestions(value, suggestions),
        });
    };

    handleSuggestionsClearRequested = () => {
        this.setState({
            suggestions: [],
        });
    };

    handleChange = (event, { newValue }) => {
        if (this.state.value != newValue) {
            this.fetchUser(newValue);
        }
        this.setState({
            value: newValue,
            isSelected: false,
        });
    };

    handleSelectSuggestion = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
        this.setState((state, props) => {
            return {
                fetching: false,
                isSelected: true,
            }
        })
        console.log(suggestion);
        this.setState({
            user: suggestion
        })
    }


    render() {
        const { open, classes, theme } = this.props;
        const { snackOpen, user, users, fetching, error } = this.state;
        return (
            <Dialog
                fullScreen
                open={open}
                onClose={this.handleClose}
                TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton color="inherit" onClick={this.handleClose}>
                            <BackIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            Add
                    </Typography>

                    </Toolbar>
                </AppBar>

                <div className={classes.container}>
                    <Autosuggest
                        theme={{
                            container: classes.autoContainer,
                            suggestionsContainerOpen: classes.suggestionsContainerOpen,
                            suggestionsList: classes.suggestionsList,
                            suggestion: classes.suggestion,
                        }}
                        renderInputComponent={this.renderInput}
                        suggestions={this.state.suggestions}
                        onSuggestionsFetchRequested={this.handleSuggestionsFetchRequested}
                        onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
                        renderSuggestionsContainer={this.renderSuggestionsContainer}
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={this.renderSuggestion}
                        onSuggestionSelected={this.handleSelectSuggestion}
                        inputProps={{
                            classes,
                            placeholder: 'Enter username',
                            value: this.state.value,
                            onChange: this.handleChange,
                        }}
                    />
                    <TextField
                                name="nickname"
                                id="nickname"
                                label="Nick name"
                                placeholder="Enter nickname"
                                className={classes.textField}
                                margin="normal"
                                error={error.nickname}
                                onChange={this.handleUserInput}
                            />
                    <Button variant="contained" color="primary" className={classes.button} onClick={this.handleAddContact}>
                        Add
                    </Button>
                    <Snackbar
                            className={classes.error}
                            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                            open={snackOpen}
                            onClose={this.handleSnackClose}
                            ContentProps={{ 'aria-describedby': 'message-id', }}
                            autoHideDuration={3000}
                            message={<span id="message-id">Add request has been sent!</span>}
                        />
                </div>
            </Dialog>);
    }
}

AddFriendDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(AddFriendDialog);