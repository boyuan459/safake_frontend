import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import BackIcon from '@material-ui/icons/ChevronLeft';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/core/styles';
import Transition from '../../component/Transition';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import classNames from 'classnames';
import { Tree } from 'antd';
import { xml2js } from 'xml-js';

const TreeNode = Tree.TreeNode;

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
        marginLeft: -48,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    container: {
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 20,
    },
    textField: {
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
});

const options = { compact: true, ignoreComment: true, alwaysChildren: true };

class DiscoDialog extends Component {
    state = {
        snackOpen: false,
        treeData: null,
        selectedItem: null,
    }

    handleDomainDisco = () => {
        console.log(this.state.domain);
        const { handleDisco } = this.props;
        if (handleDisco != null) {
            const {domain} = this.state;
            if (domain != null) {
                console.log(domain);
                handleDisco(domain)
                .then(iq => {
                    console.log(iq);
                    const iqObj = xml2js(iq.outerHTML, options);
                    console.log(iqObj);
                    let children = [];
                    if (iqObj.iq.query.item) {
                        iqObj.iq.query.item.forEach(item => {
                            children.push({
                                title: item._attributes.jid,
                                key: item._attributes.jid
                            })
                        })
                    } 
                    let treeData = [{
                        title: domain,
                        key: domain,
                        children: children
                    }];
                    this.setState({
                        treeData
                    })
                });
            }
        }
    }

    handleSnackClose = () => {
        this.setState({
            snackOpen: false
        })
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        // console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    handleClose = () => {
        const { handleClose } = this.props;
        if (handleClose != null) {
            handleClose();
        }
    }

    onSelect = (selectedKeys, info) => {
        console.log(selectedKeys, info);
        const selectedItem = selectedKeys[0];
        const { handleDisco } = this.props;
        if (handleDisco != null) {
            if (selectedItem != null) {
                console.log(selectedItem);
                handleDisco(selectedItem)
                .then(iq => {
                    // console.log(iq);
                    const iqObj = xml2js(iq.outerHTML, options);
                    console.log(iqObj);
                    let children = [];
                    console.log(this.state.treeData);
                    if (iqObj.iq.query.item) {
                        if (iqObj.iq.query.item.length != null) {
                            iqObj.iq.query.item.forEach(item => {
                                children.push({
                                    title: item._attributes.node ? item._attributes.node + "." + item._attributes.jid : item._attributes.jid,
                                    key: item._attributes.node ? item._attributes.node + "." + item._attributes.jid : item._attributes.jid
                                })
                            })
                        } else {
                            children.push({
                                title: iqObj.iq.query.item._attributes.jid,
                                key: iqObj.iq.query.item._attributes.jid
                            })
                        }
                        
                        let treeData = this.state.treeData;
                        console.log(treeData);
                        let treeNode = treeData[0].children.map(item => {
                            if (item.key === selectedItem) {
                                item.children = children;
                            }
                            return item;
                        })
                        console.log(treeNode);
                        treeData[0].children = treeNode;
                        console.log(treeData);
                        this.setState({
                            treeData
                        })
                    } 
                    
                });
            }
        }
    }

    renderTreeNodes = (data) => {
        return data.map(item => {
            if (item.children) {
                return (
                    <TreeNode title={item.title} key={item.key} dataRef={item}>
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                )
            }
            return <TreeNode {...item} />
        });
    }

    render() {
        const { open, classes } = this.props;
        const { snackOpen } = this.state;
        return (
        <Dialog
            fullScreen
            open={open}
            onClose={this.handleClose}
            TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton color="inherit" onClick={this.handleClose}>
                        <BackIcon />
                    </IconButton>
                    <Typography variant="title" color="inherit" className={classes.flex}>
                        Discovery
                    </Typography>
                    
                </Toolbar>
            </AppBar>

            <div className={classes.container}>
                <Grid container>
                    <Grid item xs={12}>
                        <FormControl className={classNames(classes.textField)}>
                            <InputLabel htmlFor="adornment-domain">Domain</InputLabel>
                            <Input
                                name="domain"
                                id="adornment-domain"
                                type='text'
                                onChange={this.handleUserInput}
                                endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                    aria-label="Toggle password visibility"
                                    onClick={this.handleDomainDisco}
                                    >
                                    <Visibility />
                                    </IconButton>
                                </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                </Grid>
                {
                    this.state.treeData ? 
                    <Tree
                        defaultExpandedKeys={this.state.treeData ? [this.state.treeData[0].key] : null}
                        onSelect={this.onSelect}>
                        {
                            this.renderTreeNodes(this.state.treeData)
                        }
                    </Tree>
                    : null
                }
            </div>
        </Dialog>);
    }
}

export default withStyles(styles)(DiscoDialog);