import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ChevronLeft';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/core/styles';
import Transition from '../../component/Transition';
import PropTypes from 'prop-types';
import { searchPubsub } from '../../services/pubsub';
import debounce from 'lodash/debounce';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import CircularProgress from '@material-ui/core/CircularProgress';

function getSuggestionValue(suggestion) {
    return suggestion.label;
}

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
        marginLeft: -48,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    container: {
        padding: 25,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
    formControl: {
        margin: theme.spacing.unit,
        width: '100%',
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
    autoContainer: {
        flexGrow: 1,
        position: 'relative',
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },

});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        }
    }
};

class SubscribeDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            snackOpen: false,
            publisher: null,
            publishers: [],
            fetching: false,
            value: '',
            suggestions: [],
            isSelected: false,
        }
        this.lastFetchId = 0;
        this.fetchPublisher = debounce(this.fetchPublisher, 300);
    }

    handleSubscribe = () => {
        const { handleCreate } = this.props;
        if (handleCreate != null) {
            const { publisher } = this.state;
            if (publisher != null) {
                handleCreate(publisher);
                this.setState({
                    snackOpen: true
                });
            }
        }
    }

    handleSnackClose = () => {
        this.setState({
            snackOpen: false
        })
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        // console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    handleClose = () => {
        const { handleClose } = this.props;
        if (handleClose != null) {
            handleClose();
        }
    }

    fetchPublisher = value => {
        console.log(value);
        if (!value || this.state.isSelected) {
            return;
        }
        this.lastFetchId += 1;
        const fetchId = this.lastFetchId;
        this.setState({
            publishers: [],
            fetching: true
        });
        searchPubsub(value)
            .then(result => {
                console.log(result);
                if (fetchId !== this.lastFetchId) {
                    return;
                }
                const publishers = result.map(item => ({
                    label: item.name,
                    value: item.node,
                    avatar: item.avatar
                }))
                const suggestions = this.getSuggestions(value, publishers);
                this.setState({
                    publishers: publishers,
                    suggestions: suggestions,
                    fetching: false
                })
            }).catch(error => {
                console.log(error);
                this.setState({
                    publishers: [],
                    fetching: false
                })
            });

    }

    handlePublisherChange = value => {
        console.log(value);
        this.setState({
            publisher: value,
            publishers: [],
            fetching: false
        });
    }

    renderSuggestion = (suggestion, { query, isHighlighted }) => {
        const matches = match(suggestion.label, query);
        const parts = parse(suggestion.label, matches);

        return (
            <MenuItem selected={isHighlighted} component="div">
                <div>
                    {parts.map((part, index) => {
                        return part.highlight ? (
                            <span key={String(index)} style={{ fontWeight: 500 }}>
                                {part.text}
                            </span>
                        ) : (
                                <strong key={String(index)} style={{ fontWeight: 300 }}>
                                    {part.text}
                                </strong>
                            );
                    })}
                </div>
            </MenuItem>
        );
    }

    renderInput = (inputProps) => {
        const { classes, ref, ...other } = inputProps;

        return (
            <TextField
                fullWidth
                InputProps={{
                    inputRef: ref,
                    classes: {
                        input: classes.input,
                    },
                    ...other,
                }}
            />
        );
    }

    renderSuggestionsContainer = (options) => {
        const { containerProps, children } = options;
        const { classes } = this.props;

        return (
            <Paper {...containerProps} square>
                {
                    this.state.fetching ?
                        <CircularProgress className={classes.progress} />
                        :
                        children
                }
            </Paper>
        );
    }

    getSuggestions = (value, suggestions) => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
        let count = 0;

        return inputLength === 0 || suggestions.length === 0
            ? []
            : suggestions.filter(suggestion => {
                const keep =
                    count < 5 && suggestion.label.toLowerCase().slice(0, inputLength) === inputValue;

                if (keep) {
                    count += 1;
                }

                return keep;
            });
    }

    handleSuggestionsFetchRequested = ({ value }) => {
        const suggestions = this.state.publishers;
        this.setState({
            suggestions: this.getSuggestions(value, suggestions),
        });
    };

    handleSuggestionsClearRequested = () => {
        this.setState({
            suggestions: [],
        });
    };

    handleChange = (event, { newValue }) => {
        if (this.state.value != newValue) {
            this.fetchPublisher(newValue);
        }
        this.setState({
            value: newValue,
            isSelected: false,
        });
    };

    handleSelectSuggestion = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
        this.setState((state, props) => {
            return {
                fetching: false,
                isSelected: true,
            }
        })
        console.log(suggestion);
        this.setState({
            publisher: suggestion
        })
    }


    render() {
        const { open, classes, theme } = this.props;
        const { snackOpen, publisher, publishers, fetching } = this.state;
        return (
            <Dialog
                fullScreen
                open={open}
                onClose={this.handleClose}
                TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton color="inherit" onClick={this.handleClose}>
                            <BackIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            Subscribe
                    </Typography>

                    </Toolbar>
                </AppBar>

                <div className={classes.container}>
                    <Autosuggest
                        theme={{
                            container: classes.autoContainer,
                            suggestionsContainerOpen: classes.suggestionsContainerOpen,
                            suggestionsList: classes.suggestionsList,
                            suggestion: classes.suggestion,
                        }}
                        renderInputComponent={this.renderInput}
                        suggestions={this.state.suggestions}
                        onSuggestionsFetchRequested={this.handleSuggestionsFetchRequested}
                        onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
                        renderSuggestionsContainer={this.renderSuggestionsContainer}
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={this.renderSuggestion}
                        onSuggestionSelected={this.handleSelectSuggestion}
                        inputProps={{
                            classes,
                            placeholder: 'Search here',
                            value: this.state.value,
                            onChange: this.handleChange,
                        }}
                    />
                    <Button variant="contained" color="primary" className={classes.button} onClick={this.handleSubscribe}>
                        Subscribe
                    </Button>
                    <Snackbar
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                        open={snackOpen}
                        onClose={this.handleSnackClose}
                        ContentProps={{ 'aria-describedby': 'message-id', }}
                        autoHideDuration={3000}
                        message={<span id="message-id">Subscribe request has been sent!</span>}
                    />
                </div>
            </Dialog>);
    }
}

SubscribeDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(SubscribeDialog);