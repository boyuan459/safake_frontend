import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ChevronLeft';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/core/styles';
import Transition from '../../component/Transition';
import PropTypes from 'prop-types';
import { Button, Form, Input, Select, Spin, DatePicker, InputNumber, Upload, Icon, message } from 'antd';
import * as Constants from '../../constant/common';

const FormItem = Form.Item;
const Option = Select.Option;
const TextArea = Input.TextArea;

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
        marginLeft: -48,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    container: {
        padding: 25,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,

        '&:hover': {
            backgroundColor: theme.palette.primary.main,
        }
    },
    formControl: {
        margin: theme.spacing.unit,
        width: '100%',
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit/4,
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        }
    }
};

class CreateStoryDialog extends Component {
    state = {
        snackOpen: false,
        pubsub: '',
        fileList: []
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.open !== prevProps.open) {
            this.setState({
                fileList: []
            })
        }
    }

    handleCreatePost = (event) => {
        event.preventDefault();
        const { handleCreate } = this.props;
        if (handleCreate != null) {
            handleCreate();
            this.setState({
                snackOpen: true
            });
        }
    }

    handleSnackClose = () => {
        this.setState({
            snackOpen: false
        })
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        // console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    handleClose = () => {
        const { handleClose } = this.props;
        if (handleClose != null) {
            handleClose();
        }
    }

    handleFileChange = info => {
        const { intl } = this.props;
        if (info.file.status === 'error') {
           console.log(`${info.file.name} upload image failed`)
        }
        let fileList = info.fileList;
        
        //filter successfully uploaded files
        fileList = fileList.filter(file => {
            if (file.status === "done" || file.status === "uploading") {
                return true
            }
            return false;
        });
        this.setState({fileList});
    }

    render() {
        const { open, classes, theme, pubsub, form, token } = this.props;
        const { snackOpen } = this.state;
        const { getFieldDecorator } = form;
        
        const uploadConfig = {
            name: 'images',
            action: Constants.API_BASE_URL + '/api/upload/image',
            listType: 'picture',
            multiple: true,
            onChange: this.handleFileChange,
            headers: {
                authorization: 'Bearer ' + token,
            }
        }
        return (
        <Dialog
            fullScreen
            open={open}
            onClose={this.handleClose}
            TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton color="inherit" onClick={this.handleClose}>
                        <BackIcon />
                    </IconButton>
                    <Typography variant="title" color="inherit" className={classes.flex}>
                        New Story
                    </Typography>
                    
                </Toolbar>
            </AppBar>

            <div className={classes.container}>
                <Form layout="vertical" onSubmit={this.handleCreatePost}>
                        <FormItem label="Highlight">
                            {
                                getFieldDecorator('highlight', {
                                    rules: [{ required: true, message: "Please enter highlight"}]
                                })(
                                    <TextArea 
                                        autosize={{minRows: 3, maxRows: 6}}
                                        placeholder="Please enter highlight here"
                                        />
                                )
                            }
                        </FormItem>
                        <FormItem label="Images">
                            {
                                getFieldDecorator('images', {
                                    rules: [{ required: false, message: "Please upload images"}]
                                })(
                                    <Upload {...uploadConfig} fileList={this.state.fileList}>
                                        <Button>
                                            <Icon type="upload" /> Upload Photos
                                        </Button>
                                    </Upload>
                                )
                            }
                        </FormItem>
                    <FormItem>
                        <Button type="primary" size="large" htmlType="submit">Create</Button>
                    </FormItem>
                </Form>
            <Snackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                open={snackOpen}
                onClose={this.handleSnackClose}
                ContentProps={{ 'aria-describedby': 'message-id', }}
                autoHideDuration={3000}
                message={<span id="message-id">Add request has been sent!</span>}
            />
            </div>
        </Dialog>);
    }
}

CreateStoryDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

const CreateStoryForm = Form.create()(withStyles(styles, {withTheme: true})(CreateStoryDialog));
export default CreateStoryForm;
// export default withStyles(styles, {withTheme: true})(CreatePostDialog);