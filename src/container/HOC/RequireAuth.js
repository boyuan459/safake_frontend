import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

export default function(ComposedComponent) {
  class Authentication extends Component {
    // static contextTypes = {
    //   router: React.PropTypes.object
    // };

    componentWillMount() {
      if (!this.props.authenticated) {
        this.props.history.push('/login');
      }
      
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.authenticated) {
        this.props.history.push('/login');
      }
    }

    render() {
      return <ComposedComponent {...this.props}/>;
    }
  }

  //class level property
//   Authentication.contextTypes = {
//     router: PropTypes.object
//   };

  function mapStateToProps(state) {
    return {
      authenticated: !!state.Auth.toJS().oauth
    };
  }

  return connect(mapStateToProps)(Authentication);
}

//In some other locaiton ... Not in this filter
//we want to use this HOC
// import Authentication //this is the HOC
// import Resources //this is the component i want to wrap
//
// const ComposedComponent = Authentication(Resources);

//in some render method...
// <ComposedComponent resources={resources}/>
