import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import HomeIcon from '@material-ui/icons/Home';
import RestaurantIcon from '@material-ui/icons/Restaurant';
import ActivityIcon from '@material-ui/icons/Games';
import MeIcon from '@material-ui/icons/Message';
import Badge from '@material-ui/core/Badge';
import { Navbar } from 'react-bootstrap';
import IntlMessage from '../../component/utility/intlMessage';
import { connect } from 'react-redux';
import * as authActions from '../../redux/auth/actions';

const styles = theme => ({
    root: {
        width: '100%',
        position: 'fixed',
        right: 0,
        left: 0,
        bottom: 0,
        zIndex: 1030,
        borderTop: '1px solid #e7e7e7',
    },
    label: {
        fontSize: theme.typography.pxToRem(20),
        // '&$selected': {
        //     fontSize: theme.typography.pxToRem(24)
        // }
    },
    badgeBadge: {
        top: -10,
        width: 20,
        height: 20,
        fontSize: '1rem',
    }
});

class BottomNavbar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: props.tab
        }
    }

    componentWillReceiveProps(props) {
        if (this.state.value !== props.tab) {
            this.setState({
                value: props.tab
            });
        }
    }

    navigate = nav => {
        const { history } = this.props;
        switch (nav) {
            case 'host':
                history.push('/dashboard');
                break;
            case 'food':
                history.push('/dashboard/food');
                break;
            case 'fun':
                history.push('/dashboard/fun');
                break;
            case 'me':
                history.push('/dashboard/me')
                break;
            default:
                history.push('/dashboard');
                break;
        }
    }

    handleChange = (event, value) => {
        console.log(value);
        const { tabChange, authenicated } = this.props;
        this.navigate(value)
        if (authenicated) {
            tabChange(value);
        } else {
            value != 'me' && tabChange(value);
        }
    }

    render() {
        const { classes } = this.props;
        const { value } = this.state;

        return (
            // <Navbar fluid fixedBottom className="app-navbar-fixed-bottom">
            <BottomNavigation
                value={value}
                onChange={this.handleChange}
                showLabels
                className={classes.root}
            >
                <BottomNavigationAction value="host" label={<IntlMessage id="bottombar.home" />} icon={<HomeIcon />}
                    classes={{ label: classes.label }} />
                <BottomNavigationAction value="food" label={<IntlMessage id="bottombar.food" />} icon={<RestaurantIcon />}
                    classes={{ label: classes.label }} />
                <BottomNavigationAction value="fun" label={<IntlMessage id="bottombar.fun" />} icon={<ActivityIcon />}
                    classes={{ label: classes.label }} />
                {/* <BottomNavigationAction value="me" label={<IntlMessage id="bottombar.me" />} icon={
                        <Badge classes={{
                            badge: classes.badgeBadge,
                          }} badgeContent={4} color="secondary"><MeIcon /></Badge>} 
                        classes={{ label: classes.label }} /> */}
                <BottomNavigationAction value="me" label={<IntlMessage id="bottombar.me" />} icon={<MeIcon />}
                    classes={{ label: classes.label }} />
            </BottomNavigation>
            // </Navbar>
        );
    }
}

BottomNavbar.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    return {
        tab: state.Auth.toJS().tab,
        authenicated: !!state.Auth.toJS().user
    };
}

function mapDispatchToProps(dispatch) {
    return {
        tabChange(tab) {
            dispatch(authActions.tabChange(tab));
        }
    };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(BottomNavbar)));