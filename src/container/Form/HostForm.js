import React, { Component } from 'react';
import { Button, Form, Input, Select, Spin, DatePicker, InputNumber, Upload, Icon, message } from 'antd';
import { injectIntl } from 'react-intl';
import debounce from 'lodash/debounce';
import moment from 'moment';
import IntlMessage from '../../component/utility/intlMessage';
import { citySearch } from '../../services/geolocation';
import PriceInput from '../../component/PriceInput';

const FormItem = Form.Item;
const Option = Select.Option;
const TextArea = Input.TextArea;

message.config({
    top: 100,
    duration: 2,
    maxCount: 3,
  });

const UploadConfig = {
    baseUrl: process.env.REACT_APP_API_BASE_URL,
}
class HostForm extends Component {
    state = {
        cities: [],
        fetching: false,
        fileList: []
    }
    constructor(props) {
        super(props);
        this.lastFetchId = 0;
        this.fetchCity = debounce(this.fetchCity, 300);
    }

    componentWillReceiveProps(props) {
        if (this.props.item !== props.item) {
            if (props.item && props.item.images) {
                this.setState({
                    fileList: props.item.images
                })
            }
        }
    }
    fetchCity = value => {
        if (!value) {
            return;
        }
        this.lastFetchId += 1;
        const fetchId = this.lastFetchId;
        this.setState({
            cities: [],
            fetching: true
        });
        citySearch(value)
        .then(result => {
            console.log(result);
            if (fetchId !== this.lastFetchId) {
                return;
            }
            this.setState({
                cities: result,
                fetching: false
            })
        }).catch(error => {
            console.log(error);
            this.setState({
                cities: [],
                fetching: false
            })
        });

    }

    handleCityChange = value => {
        this.setState({
            cities: [],
            fetching: false
        })
    }

    checkPrice = (rule, value, callback) => {
        if (value.number > 0) {
            callback();
            return;
        }
        const { intl } = this.props;
        callback(intl.formatMessage({ id: "form.host.price.message.gt0" }));
    }

    handleSubmit = event => {
        const { handleSave } = this.props;
        event.preventDefault();
        handleSave();
    }

    handleFileChange = info => {
        const { intl } = this.props;
        if (info.file.status === 'error') {
            message.error(`${info.file.name} ${intl.formatMessage({ id: "form.host.images.upload.error"})}`)
        }
        let fileList = info.fileList;
        
        //filter successfully uploaded files
        fileList = fileList.filter(file => {
            if (file.status === "done" || file.status === "uploading") {
                return true
            }
            return false;
        });
        this.setState({fileList});
    }

    render() {
        const { form, intl, token, item } = this.props;
        const { getFieldDecorator } = form;
        const { cities, fetching } = this.state;
        const categories = [
            {
                "value": "host",
                "text": intl.formatMessage({ id: "form.host.category.option.host" })
            },
            {
                "value": "food",
                "text": intl.formatMessage({ id: "form.host.category.option.food" })
            },
            {
                "value": "fun",
                "text": intl.formatMessage({ id: "form.host.category.option.fun" })
            }
        ];
        const uploadConfig = {
            name: 'images',
            action: UploadConfig.baseUrl + '/api/upload/image',
            listType: 'picture',
            multiple: true,
            onChange: this.handleFileChange,
            headers: {
                authorization: 'Bearer ' + token,
            }
        }

        return (
            <div className="container" style={{marginTop: 20}}>
            <Form layout="vertical" onSubmit={this.handleSubmit}>
                {
                    item ? 
                    <FormItem label="ID">
                    {
                        getFieldDecorator('id', {
                            initialValue: item ? item._id : null
                        })(
                            <Input disabled />
                        )
                    }
                    </FormItem>
                    : null
                }
                <FormItem label={<IntlMessage id="form.host.category" />}>
                    {
                        getFieldDecorator('category', {
                            initialValue: item ? item.category : null,
                            rules: [
                                { required: true, message: intl.formatMessage({ id: "form.host.category.message" }) }
                            ]
                        })(
                            <Select
                                placeholder={<IntlMessage id="form.host.category.message" />}>
                                {
                                    categories.map(item => <Option key={item.value}>{item.text}</Option>)
                                }
                            </Select>
                        )
                    }
                </FormItem>
                <FormItem label={<IntlMessage id="form.host.city" />}>
                    {
                        item ? 
                        getFieldDecorator('address', {
                            initialValue: item ? 
                                {
                                    key: `${item.address.city},${item.address.state},${item.address.postcode}`,
                                    label: `${item.address.city} ${item.address.state} ${item.address.postcode}`,
                                    value: `${item.address.city},${item.address.state},${item.address.postcode}`
                                } : null,
                            rules: [
                                { required: true, message: intl.formatMessage({ id: "form.host.city.message" })}
                            ]
                        })(
                            <Select
                                showSearch
                                labelInValue
                                notFoundContent={fetching? <Spin size="small" />: null}
                                placeholder={<IntlMessage id="form.host.city.message"/>}
                                filterOption={false}
                                onChange={this.handleCityChange}
                                onSearch={this.fetchCity}>
                                {
                                    cities.map(city => <Option key={city.value}>{city.label}</Option>)
                                }
                            </Select>
                        ):
                        getFieldDecorator('address', {
                            rules: [
                                { required: true, message: intl.formatMessage({ id: "form.host.city.message" })}
                            ]
                        })(
                            <Select
                                showSearch
                                labelInValue
                                notFoundContent={fetching? <Spin size="small" />: null}
                                placeholder={<IntlMessage id="form.host.city.message"/>}
                                filterOption={false}
                                onChange={this.handleCityChange}
                                onSearch={this.fetchCity}>
                                {
                                    cities.map(city => <Option key={city.value}>{city.label}</Option>)
                                }
                            </Select>
                        )
                    }
                </FormItem>
                <FormItem label={<IntlMessage id="form.host.price"/>}>
                    {
                        getFieldDecorator('price', {
                            initialValue: item ? {number: item.price.number, currency: item.price.currency}: { number: 0, currency: 'rmb' },
                            rules: [{ validator: this.checkPrice }, { required: true, message: intl.formatMessage({ id: "form.host.price.message.required"})}],
                        })(
                            <PriceInput />
                        )
                    }
                </FormItem>
                <FormItem label={<IntlMessage id="form.host.contact" />}>
                    {
                        getFieldDecorator('contact', {
                            initialValue: item ? item.contact : null,
                            rules: [ { required: true, message: intl.formatMessage({ id: "form.host.contact"})} ]
                        })(
                            <Input />
                        )
                    }
                </FormItem>
                <FormItem label={<IntlMessage id="form.host.highlight" />}>
                    {
                        getFieldDecorator('highlight', {
                            initialValue: item ? item.highlight : null,
                            rules: [{ required: true, message: intl.formatMessage({ id: "form.host.highlight.message"}) }]
                        })(
                            <TextArea 
                                placeholder={intl.formatMessage({ id: "form.host.highlight.message" })}
                                autosize/>
                        )
                    }
                </FormItem>
                <FormItem label={<IntlMessage id="form.host.detail" />}>
                    {
                        getFieldDecorator('detail', {
                            initialValue: item ? item.detail : null,
                            rules: [{ required: true, message: intl.formatMessage({id: "form.host.detail.message"})}]
                        })(
                            <TextArea 
                                autosize={{minRows: 2, maxRows: 6}}
                                placeholder={intl.formatMessage({ id: "form.host.detail.message"})}
                                />
                        )
                    }
                </FormItem>
                <FormItem label={<IntlMessage id="form.host.images"/>}>
                    {
                        getFieldDecorator('images', {
                            rules: [{ required: false, message: intl.formatMessage({ id: "form.host.images.message"})}]
                        })(
                            <Upload {...uploadConfig} fileList={this.state.fileList}>
                                <Button>
                                    <Icon type="upload" /> Upload Photos
                                </Button>
                            </Upload>
                        )
                    }
                </FormItem>
                <FormItem label={<IntlMessage id="form.host.maxPersons"/>}>
                    {
                        getFieldDecorator('maxPersons', {
                            initialValue: item ? item.maxPersons : null,
                        })(
                            <InputNumber min={1} style={{ width: '100%'}}/>
                        )
                    }
                </FormItem>
                <FormItem label={<IntlMessage id="form.host.startDate" />}>
                    {
                        getFieldDecorator('startDate', {
                            initialValue: item && item.startDate? moment(item.startDate) : null,
                        })(
                            <DatePicker style={{ width: '100%'}} 
                             />
                        )
                    }
                </FormItem>
                <FormItem label={<IntlMessage id="form.host.endDate" />}>
                    {
                        getFieldDecorator('endDate', {
                            initialValue: item && item.endDate ? moment(item.endDate) : null,
                        })(
                            <DatePicker style={{ width: '100%'}} 
                                />
                        )
                    }
                </FormItem>
                <FormItem>
                    <Button type="primary" size="large" htmlType="submit">{intl.formatMessage({ id: "form.host.submit" })}</Button>
                </FormItem>
            </Form>
            </div>
        );
    }
}

const HostFormWrap = Form.create()(injectIntl(HostForm));
export default HostFormWrap;