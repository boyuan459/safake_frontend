import React, { Component } from 'react';
import { Button, Form, Input, Select, Spin, DatePicker, InputNumber, Upload, Icon, message } from 'antd';
import { injectIntl } from 'react-intl';
import debounce from 'lodash/debounce';
import moment from 'moment';
import IntlMessage from '../../component/utility/intlMessage';
import { citySearch } from '../../services/geolocation';
import PriceInput from '../../component/PriceInput';

const FormItem = Form.Item;
const Option = Select.Option;
const TextArea = Input.TextArea;

message.config({
    top: 100,
    duration: 2,
    maxCount: 3,
  });

const UploadConfig = {
    baseUrl: process.env.REACT_APP_API_BASE_URL,
}
class PostForm extends Component {
    state = {
        cities: [],
        fetching: false,
        fileList: []
    }
    constructor(props) {
        super(props);
        this.lastFetchId = 0;
        this.fetchCity = debounce(this.fetchCity, 300);
    }

    componentWillReceiveProps(props) {
        if (this.props.item !== props.item) {
            if (props.item && props.item.images) {
                this.setState({
                    fileList: props.item.images
                })
            }
        }
    }
    fetchCity = value => {
        if (!value) {
            return;
        }
        this.lastFetchId += 1;
        const fetchId = this.lastFetchId;
        this.setState({
            cities: [],
            fetching: true
        });
        citySearch(value)
        .then(result => {
            console.log(result);
            if (fetchId !== this.lastFetchId) {
                return;
            }
            this.setState({
                cities: result,
                fetching: false
            })
        }).catch(error => {
            console.log(error);
            this.setState({
                cities: [],
                fetching: false
            })
        });

    }

    handleCityChange = value => {
        this.setState({
            cities: [],
            fetching: false
        })
    }

    checkPrice = (rule, value, callback) => {
        if (value.number > 0) {
            callback();
            return;
        }
        const { intl } = this.props;
        callback(intl.formatMessage({ id: "form.host.price.message.gt0" }));
    }

    handleSubmit = event => {
        const { handleSave } = this.props;
        event.preventDefault();
        handleSave();
    }

    handleFileChange = info => {
        const { intl } = this.props;
        if (info.file.status === 'error') {
            message.error(`${info.file.name} ${intl.formatMessage({ id: "form.host.images.upload.error"})}`)
        }
        let fileList = info.fileList;
        
        //filter successfully uploaded files
        fileList = fileList.filter(file => {
            if (file.status === "done" || file.status === "uploading") {
                return true
            }
            return false;
        });
        this.setState({fileList});
    }

    render() {
        const { form, intl, token, item } = this.props;
        const { getFieldDecorator } = form;
        const { cities, fetching } = this.state;
        const categories = [
            {
                "value": "host",
                "text": intl.formatMessage({ id: "form.host.category.option.host" })
            },
            {
                "value": "food",
                "text": intl.formatMessage({ id: "form.host.category.option.food" })
            },
            {
                "value": "fun",
                "text": intl.formatMessage({ id: "form.host.category.option.fun" })
            }
        ];
        const uploadConfig = {
            name: 'images',
            action: UploadConfig.baseUrl + '/api/upload/image',
            listType: 'picture',
            multiple: true,
            onChange: this.handleFileChange,
            headers: {
                authorization: 'Bearer ' + token,
            }
        }

        return (
            <div className="container" style={{marginTop: 20}}>
            <Form layout="vertical" onSubmit={this.handleSubmit}>
            <FormItem label="subject">
                        {
                            getFieldDecorator('subject', {
                                rules: [{ required: true, message: "Please enter subject" }]
                            })(
                                <TextArea 
                                    placeholder="Subject"
                                    autosize/>
                            )
                        }
                    </FormItem>
                    <FormItem label="detail">
                        {
                            getFieldDecorator('detail', {
                                rules: [{ required: true, message: "Please enter detail"}]
                            })(
                                <TextArea 
                                    autosize={{minRows: 2, maxRows: 6}}
                                    placeholder="Please enter details here"
                                    />
                            )
                        }
                    </FormItem>
                    <FormItem label="images">
                        {
                            getFieldDecorator('images', {
                                rules: [{ required: false, message: "Please upload images"}]
                            })(
                                <Upload {...uploadConfig} fileList={this.state.fileList}>
                                    <Button>
                                        <Icon type="upload" /> Upload Photos
                                    </Button>
                                </Upload>
                            )
                        }
                    </FormItem>
                <FormItem>
                    <Button type="primary" size="large" htmlType="submit">{intl.formatMessage({ id: "form.host.submit" })}</Button>
                </FormItem>
            </Form>
            </div>
        );
    }
}

const PostFormWrap = Form.create()(injectIntl(PostForm));
export default PostFormWrap;