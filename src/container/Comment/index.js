import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';

class Comment extends Component {
    state = {
        comment: ''
    }
    handleClose = () => {
        const { handleClose } = this.props;
        handleClose();
    }

    handleComment = () => {
        const { handleComment } = this.props;
        const { comment } = this.state;
        if (comment) {
            handleComment(comment);
            this.setState({
                comment: ''
            })
        }
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name] : value
        });
    }

    render() {
        const { open } = this.props;

        return (
            <Dialog
                open={open}
                fullWidth={true}
                onClose={this.handleClose}
                aria-labelledby="comment-title">
                <DialogContent>
                    <TextField 
                        autoFocus
                        margin="dense"
                        id="comment"
                        name="comment"
                        label="Comment"
                        fullWidth
                        multiline
                        rowsMax="4"
                        onChange={this.handleUserInput}
                        />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleComment} color="primary">
                        Comment
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default Comment;