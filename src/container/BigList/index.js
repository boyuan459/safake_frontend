import * as React from 'react';
import { InfiniteLoader, AutoSizer, List } from 'react-virtualized';
import { getClient } from '../../helpers/utility';
import { connect } from 'react-redux';
import Item from '../../component/List/Item';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as authActions from '../../redux/auth/actions';
import debounce from 'lodash/debounce';
import HostDetail from '../../component/HostDetail';
import { getFavorites, getHosts, getHostRefreshing, getHostError, getHostTotalRows, getLikes } from '../../selectors';
import * as favoriteActions from '../../redux/favorite/actions';
import * as hostsActions from '../../redux/host/actions';
import Comment from '../Comment';

const styles = {
    container: {
        overscrollBehavior: 'contain',
    },
    spinner: {
        display: 'flex',
        justifyContent: 'center',
    },
};

class BigList extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            city: props.search.get('city').key,
            sort: props.search.get('sort'),
            detailOpen: false,
            host: null,
            commentOpen: false,
            commentHost: null,
        };

        this._isRowLoaded = this._isRowLoaded.bind(this);
        this._loadMoreRows = this._loadMoreRows.bind(this);
        this._rowRenderer = this._rowRenderer.bind(this);
    }

    componentWillReceiveProps(props) {
        if (this.props.search !== props.search) {
            console.log(props.search.get('city').key, props.search.get('sort'));
            this.setState({
                city: props.search.get('city').key,
                sort: props.search.get('sort'),
            }, () => this.fetchData());
        }
    }

    componentDidMount() {
        this.fetchData();
        const { auth, category, likesFetch } = this.props;
        if (auth.oauth != null) {
            likesFetch(category);
        }
        this._touchStart = e => {
            this._startY = e.touches[0].pageY;
          }
        this.container.addEventListener('touchstart', this._touchStart, { passive: true});
        this._debounce = debounce(e => {
            const y = e.touches[0].pageY;
            console.log(y, this._startY);
            // Activate custom pull-to-refresh effects when at the top of the container
            // and user is scrolling up.
            if (document.scrollingElement.scrollTop === 0 && y - 80 > this._startY && !this.state.refreshing) {
              console.log('refreshing');
              this.handleRefresh();
            }
          }, 50);
        this.container.addEventListener('touchmove', this._debounce, { passive: true});
        
      }
    
      componentWillUnmount() {
        this.container.removeEventListener('touchstart', this._touchStart);
        this.container.removeEventListener('touchmove', this._debounce);
      }
    

    handleDetailClose = () => {
        const { history, tabChange } = this.props;
        // history.push('/dashboard/me')
        // tabChange("me");
        this.setState({
            detailOpen: false
        });
    }

    handleRefresh = () => {
        console.log("scroll to top");
        this.fetchData();
    }

    render() {
        const { classes, loading, totalRows, hosts, refreshing  } = this.props;
        const { detailOpen, host, commentOpen } = this.state;
        const height = getClient().height;

        return (
            <div ref={container => this.container = container} className={classes.container}>
                {
                    refreshing ? 
                    <div className={classes.spinner}>
                        <CircularProgress />
                    </div>
                    :null
                }
                <InfiniteLoader
                    isRowLoaded={this._isRowLoaded}
                    loadMoreRows={this._loadMoreRows}
                    rowCount={totalRows}>
                    {({ onRowsRendered, registerChild }) => (
                        <AutoSizer disableHeight>
                            {({ width }) => (
                                <List
                                    ref={registerChild}
                                    height={height}
                                    onRowsRendered={onRowsRendered}
                                    rowCount={hosts.length}
                                    rowHeight={385}
                                    rowRenderer={this._rowRenderer}
                                    width={width}
                                />)}
                        </AutoSizer>
                    )}
                </InfiniteLoader>
                {
                    host ?
                        <HostDetail 
                            host={host} 
                            handleDetailClose={this.handleDetailClose} detailOpen={detailOpen} 
                            />
                        : null
                }
                <Comment open={commentOpen} handleClose={this.handleCommentClose} handleComment={this.handleComment}/>
            </div>
        );
    }

    _isRowLoaded({ index }) {
        const { hosts } = this.props;
        return !!hosts[index]; // STATUS_LOADING or STATUS_LOADED
    }

    _loadMoreRows({ startIndex, stopIndex }) {
        console.log(startIndex, stopIndex);
        const { city, sort } = this.state;
        const { category = 'host', hostsSearch } = this.props;
        hostsSearch({ startIndex, stopIndex, city, sort, category });
    }

    fetchData = () => {
        const { category = 'host', hostsSearch } = this.props;
        const { page, city, sort } = this.state;
        hostsSearch({ page, city, sort, category });
    }

    handleItemClick = host => {
        this.setState({
            detailOpen: true,
            host: host
        })
    }

    handleFavorite = (host_id) => {
        const { favorite } = this.props;
        favorite(host_id);
    }

    handleUnfavorite = (host_id) => {
        const { unfavorite } = this.props;
        unfavorite(host_id);
    }

    handleLike = (host_id) => {
        const { likeHost } = this.props;
        likeHost(host_id);
    }

    handleDislike = (host_id) => {
        const { dislikeHost } = this.props;
        dislikeHost(host_id);
    }

    handleCommentShow = (host) => {
        this.setState({
            commentOpen: true,
            commentHost: host
        })
    }

    handleCommentClose = () => {
        this.setState({
            commentOpen: false,
            commentHost: null,
        })
    }

    handleComment = (comment) => {
        const { commentHost } = this.state;
        const { commentOnHost } = this.props;
        console.log("Save comment", commentHost, comment);
        commentOnHost({host_id: commentHost._id, comment});
        this.handleCommentClose();
    }

    _rowRenderer({ index, key, style }) {
        const { favorites, hosts, likes, auth } = this.props;

        const isLoggedIn = !!auth.oauth;

        const row = hosts[index];
        let content;

        if (row) {
            content = <Item isLoggedIn={isLoggedIn} data={row} favorites={favorites} likes={likes}
                        handleFavorite={this.handleFavorite} 
                        handleUnfavorite={this.handleUnfavorite}
                        handleLike={this.handleLike}
                        handleDislike={this.handleDislike}
                        handleComment={this.handleCommentShow}
                        onClick={this.handleItemClick} />
        } else {
            content = (
                <div className="placehoder" />
            );
        }

        return (
            <div className={styles.row} key={key} style={style}>
                {content}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        search: state.Search,
        auth: state.Auth.toJS(),
        favorites: getFavorites(state),
        hosts: getHosts(state),
        refreshing: getHostRefreshing(state),
        error: getHostError(state),
        totalRows: getHostTotalRows(state),
        likes: getLikes(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        tabChange(tab) {
            dispatch(authActions.tabChange(tab));
        },
        favorite(host_id) {
            dispatch(favoriteActions.favoriteCreate(host_id));
        },
        unfavorite(host_id) {
            dispatch(favoriteActions.favoriteDestroy(host_id));
        },
        hostsSearch(params) {
            dispatch(hostsActions.hostsSearch(params));
        },
        likesFetch(category) {
            dispatch(hostsActions.likesFetch(category));
        },
        likeHost(host_id) {
            dispatch(hostsActions.hostLike(host_id));
        },
        dislikeHost(host_id) {
            dispatch(hostsActions.hostDislike(host_id));
        },
        commentOnHost(comment) {
            dispatch(hostsActions.hostComment(comment));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(BigList));