import * as React from 'react';
import cn from 'classnames';
import styles from './ContentBox.css';
import debounce from 'lodash/debounce';

class ContentBox extends React.Component {
  state = {
    refreshing: false
  };

  componentDidMount() {
    const { onRefresh } = this.props;
    this.container.addEventListener('touchstart', e => {
      this._startY = e.touches[0].pageY;
    }, { passive: true});
    this.container.addEventListener('touchmove', debounce(e => {
      const y = e.touches[0].pageY;
      // Activate custom pull-to-refresh effects when at the top of the container
      // and user is scrolling up.
      if (document.scrollingElement.scrollTop === 0 && y > this._startY && !this.state.refreshing) {
        console.log('refreshing');
        this.setState({
          refreshing: true
        });
        onRefresh();
      }
    }, 30), { passive: true});
  }

  componentWillUnmount() {
    this.container.removeEventListener('touchstart');
    this.container.removeEventListener('touchmove');
  }

  render() {
    const { children } = this.props;

    return (
      <div ref={container => this.container = container} className={cn('ContentBox')}>
        {children}
      </div>
    );
  }
  
}

export default ContentBox;