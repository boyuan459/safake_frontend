import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import IntlMessage from '../../../component/utility/intlMessage';
import * as authActions from '../../../redux/auth/actions';
import classNames from 'classnames'; 

const styles = {
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
    },
    container: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
    },
    login: {
        "@media (min-width:769px)": {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        }
    }
};

class LoginBS extends Component {
    state = {
        remember: false,
        logged: false,
    }
    handleChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        console.log(name, e.target.checked);
        this.setState({
            [name]: e.target.checked
        })
    }

    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        const { login } = this.props;
        const { username, password } = this.state;
        login({ username, password });
        this.setState({
            logged: true
        })
    }
    render() {
        const { classes, auth } = this.props;
        const { logged } = this.state;
        return (
            <div className={classes.root}>
                <div className={classes.container}>
                    <h2 className="text-center">
                        <Link to="/"><IntlMessage id="app.name" /></Link>
                    </h2>
                    {
                        logged && auth.error && auth.error.message ? 
                        (
                            <div className="alert alert-danger" role="alert">
                                {auth.error.message}
                            </div>
                        ): null
                    }
                    <div className={classNames("row", classes.login)}>
                        <div className="col-md-8">
                            <form method="POST" onSubmit={this.handleSubmit}>
                                <div className="form-group row">
                                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right"><IntlMessage id="register.email"/></label>

                                    <div className="col-md-6">
                                        <input id="email" type="email" className="form-control" name="username" onChange={this.handleUserInput} required />


                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="password" className="col-md-4 col-form-label text-md-right"><IntlMessage id="register.password"/></label>

                                    <div className="col-md-6">
                                        <input id="password" type="password" className="form-control" name="password" onChange={this.handleUserInput} required />


                                    </div>
                                </div>

                                <FormGroup row>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={this.state.remember}
                                                onChange={this.handleChange}
                                                name="remember"
                                            />
                                        }
                                        label={<IntlMessage id="login.remember"/>}
                                    /></FormGroup>

                                <div className="form-group row mb-0">
                                    <div className="col-md-8 offset-md-4">
                                        <button type="submit" className="btn btn-primary">
                                            <IntlMessage id="login.submit"/>
                                        </button>

                                        <Link className="btn btn-link" to="/password/forgot">
                                            <IntlMessage id="login.forgot" />
                                        </Link>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        auth: state.Auth.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        login(user) {
            dispatch(authActions.login(user));
        }
    };
}

export default injectSheet(styles)(connect(mapStateToProps, mapDispatchToProps)(LoginBS));