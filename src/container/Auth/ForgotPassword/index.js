import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { Link } from 'react-router-dom';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import IntlMessage from '../../../component/utility/intlMessage';
import { sendResetPasswordLink } from '../../../services/auth';

const styles = {
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
    },
    container: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
    }
};

class ForgotPassword extends Component {
    state = {
        result: null,
        loading: false,
    }
    handleUserInput = e => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value,
            result: null
        });
        return;
    }

    handleSubmit = e => {
        e.preventDefault();
        const { email } = this.state;
        console.log("Send reset password link", email);
        if (!email) {
            return;
        }
        this.setState({
            loading: true
        });
        sendResetPasswordLink(email)
        .then(result => {
            console.log("Send reset link", result)
            this.setState({
                result,
                loading: false,
            })
        }).catch(error => {
            console.log("Send reset link error", error);
            this.setState({
                loading: false
            })
        })
    }
    render() {
        const { classes } = this.props;
        const { result, loading } = this.state;
        
        return (
            <div className={classes.root}>
                <div className={classes.container}>
                    <h2 className="text-center">
                        <Link to="/"><IntlMessage id="app.name" /></Link>
                    </h2>
                    {
                        result ? 
                            (result.status === 200 ? 
                            <div class="alert alert-success" role="alert">
                                {result.message}
                            </div>
                            :
                            <div className="alert alert-danger" role="alert">
                                {result.message}
                            </div>)
                            : null
                    }
                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <form method="POST" onSubmit={this.handleSubmit}>
                                <div className="form-group row">
                                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right"><IntlMessage id="register.email"/></label>

                                    <div className="col-md-6">
                                        <input onChange={this.handleUserInput} id="email" type="email" className="form-control" name="email" required />
                                    </div>
                                </div>

                                <div className="form-group row mb-0">
                                    <div className="col-md-8 offset-md-4">
                                        <button type="submit" className="btn btn-primary" disabled={loading}>
                                            <IntlMessage id="forgot.submit"/>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default injectSheet(styles)(ForgotPassword);