import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { Link } from 'react-router-dom';
import IntlMessage from '../../../component/utility/intlMessage';
import classNames from 'classnames'; 
import { resetPassword } from '../../../services/auth';

const styles = {
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
    },
    container: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
    },
    login: {
        "@media (min-width:769px)": {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        }
    },
};

class ResetPassword extends Component {
    state = {
        result: null,
        loading: false,
    }
    handleUserInput = event => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value
        });
    }

    handleSubmit = e => {
        e.preventDefault();
        const { email, password, password_confirmation } = this.state;
        const { match } = this.props;
        const token = match.params.token;
        console.log("Send reset password link", email);
        if (!email || !password || !password_confirmation || !token) {
            return;
        }
        this.setState({
            loading: true
        });
        resetPassword({email, password, password_confirmation, token})
        .then(result => {
            console.log("Send reset link", result)
            this.setState({
                result,
                loading: false,
            })
        }).catch(error => {
            console.log("Send reset link error", error);
            this.setState({
                loading: false
            })
        })
    }

    render() {
        const { classes } = this.props;
        const { result } = this.state;

        return (
            <div className={classes.root}>
                <div className={classes.container}>
                    <h2 className="text-center">
                        <Link to="/"><IntlMessage id="app.name" /></Link>
                    </h2>
                    {
                        result ? 
                            (result.status === 200 ? 
                            <div class="alert alert-success" role="alert">
                                {result.message}
                            </div>
                            :
                            <div className="alert alert-danger" role="alert">
                                {result.message}
                            </div>)
                            : null
                    }
                    <div className={classNames("row", classes.login)}>
                        <div className="col-md-8">
                            <form method="POST" onSubmit={this.handleSubmit}>
                                <div className="form-group row">
                                    <label htmlFor="email" className="col-md-4 col-form-label text-md-right"><IntlMessage id="register.email"/></label>

                                    <div className="col-md-6">
                                        <input onChange={this.handleUserInput} id="email" type="email" className="form-control" name="email" required />


                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="password" className="col-md-4 col-form-label text-md-right"><IntlMessage id="register.password"/></label>

                                    <div className="col-md-6">
                                        <input onChange={this.handleUserInput} id="password" type="password" className="form-control" name="password" required />


                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="password-confirm" className="col-md-4 col-form-label text-md-right"><IntlMessage id="register.confirmPassword" /></label>

                                    <div className="col-md-6">
                                        <input onChange={this.handleUserInput} id="password-confirm" type="password" className="form-control" name="password_confirmation" required />
                                    </div>
                                </div>


                                <div className="form-group row mb-0">
                                    <div className="col-md-8 offset-md-4">
                                        <button type="submit" className="btn btn-primary">
                                            <IntlMessage id="reset.password.submit"/>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default injectSheet(styles)(ResetPassword);