import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { Link } from 'react-router-dom';
import IntlMessage from '../../../component/utility/intlMessage';
import { signup } from '../../../services/auth';
import classNames from 'classnames'; 

const styles = {
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
    },
    container: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
    },
    login: {
        "@media (min-width:769px)": {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        }
    }
};

class SignUpBS extends Component {
    state = {
        error: null,
        user: null
    }
    handleUserInput = event => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            [name]: value
        });
    }

    hasError = name => {
        if (this.state.error && this.state.error.errors && this.state.error.errors[name]) {
            return true;
        }
        return false;
    }

    errorMessage = name => {
        return this.state.error.errors[name][0];
    }

    getFormControlClasses = (name) => {
        if (this.hasError(name)) {
            return "form-group row has-error";
        }
        return "form-group row";
    }

    handleSubmit = event => {
        event.preventDefault();
        // console.log(this.state);
        const { name, email, password, password_confirmation } = this.state;
        signup({ name, email, password, password_confirmation })
        .then(result => {
            console.log(result);
            this.setState({
                error: null,
                user: result
            })
        })
        .catch(err => {
            console.log(err);
            this.setState({
                error: err.data
            })
        })
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <div className={classes.container}>
                    <h2 className="text-center">
                        <Link to="/"><IntlMessage id="app.name"/></Link>
                    </h2>
                    {
                        this.state.user ? 
                        (
                            <div className="alert alert-success" role="alert">
                                <IntlMessage id="register.welcome" /><Link to="/login"><IntlMessage id="login.submit"/></Link>
                            </div>
                        ):null
                    }
                    <div className={classNames('row', classes.login)}>
                        <div className="col-md-8">
                            <form method="POST" onSubmit={this.handleSubmit}>

                                <div className={this.getFormControlClasses('name')}>
                                    <label htmlFor="name" className="col-md-4 control-label text-md-right"><IntlMessage id="register.name" /></label>

                                    <div className="col-md-6">
                                        <input id="name" type="text" className="form-control " name="name" onChange={this.handleUserInput} required autoFocus />

                                        {
                                            this.hasError('name')
                                            ? (
                                                <span className="help-block">
                                                    <strong>{this.errorMessage('name')}</strong>
                                                </span>
                                            ): null
                                        }
                                        
                                    </div>
                                </div>

                                <div className={this.getFormControlClasses('email')}>
                                    <label htmlFor="email" className="col-md-4 control-label text-md-right"><IntlMessage id="register.email"/></label>

                                    <div className="col-md-6">
                                        <input id="email" type="email" className="form-control" name="email" onChange={this.handleUserInput} required />

                                        {
                                            this.hasError('email')
                                            ? (
                                                <span className="help-block">
                                                    <strong>{this.errorMessage('email')}</strong>
                                                </span>
                                            ): null
                                        }
                                    </div>
                                </div>

                                <div className={this.getFormControlClasses('password')}>
                                    <label htmlFor="password" className="col-md-4 control-label text-md-right"><IntlMessage id="register.password"/></label>

                                    <div className="col-md-6">
                                        <input id="password" type="password" className="form-control" name="password" onChange={this.handleUserInput} required />

                                        {
                                            this.hasError('password')
                                            ? (
                                                <span className="help-block">
                                                    <strong>{this.errorMessage('password')}</strong>
                                                </span>
                                            ): null
                                        }
                                    </div>
                                </div>

                                <div className={this.getFormControlClasses('password_confirmation')}>
                                    <label htmlFor="password-confirm" className="col-md-4 control-label text-md-right"><IntlMessage id="register.confirmPassword" /></label>

                                    <div className="col-md-6">
                                        <input id="password-confirm" type="password" className="form-control" name="password_confirmation" onChange={this.handleUserInput} required />
                                        {
                                            this.hasError('password_confirmation')
                                            ? (
                                                <span className="help-block">
                                                    <strong>{this.errorMessage('password_confirmation')}</strong>
                                                </span>
                                            ): null
                                        }
                                    </div>
                                </div>

                                <div className="form-group row mb-0">
                                    <div className="col-md-6 offset-md-4">
                                        <button type="submit" className="btn btn-primary">
                                            <IntlMessage id="register.submit" />
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default injectSheet(styles)(SignUpBS);