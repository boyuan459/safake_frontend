import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import AccountCircle from '@material-ui/icons/AccountCircle';
import LockCircle from '@material-ui/icons/Lock';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
    loginForm: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh'
    },
    margin: {
        margin: theme.spacing.unit,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.primary,
    },
});

class SignUp extends React.Component {
    state = {
        amount: '',
        password: '',
        passwordConfirm: '',
        weight: '',
        weightRange: '',
        showPassword: false,
        showPasswordConfirm: false,
    };

    handleChange = prop => event => {
        this.setState({ [prop]: event.target.value });
    };

    handleMouseDownPassword = event => {
        event.preventDefault();
    };

    handleClickShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
    };

    handleClickShowPasswordConfirm = () => {
        this.setState({ showPasswordConfirm: !this.state.showPasswordConfirm });
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.loginForm}>
            <div>
                <Grid container>
                    <Grid item xs={12}>
                        <Paper className={classes.paper} elevation={0}>
                            <Typography component={Link} variant="title" to="/">Safake</Typography>
                        </Paper>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12}>
                        <FormControl className={classes.margin}>
                            <InputLabel htmlFor="adornment-username">Username</InputLabel>
                            <Input
                                id="adornment-username"
                                startAdornment={
                                    <InputAdornment position="start">
                                        <AccountCircle />
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12}>
                        <FormControl className={classNames(classes.margin, classes.textField)}>
                            <InputLabel htmlFor="adornment-password">Password</InputLabel>
                            <Input
                                id="adornment-password"
                                type={this.state.showPassword ? 'text' : 'password'}
                                value={this.state.password}
                                onChange={this.handleChange('password')}
                                startAdornment={
                                    <InputAdornment position="start">
                                        <LockCircle />
                                    </InputAdornment>
                                }
                                endAdornment={
                                    <InputAdornment position="start">
                                        <IconButton
                                            aria-label="Toggle password visibility"
                                            onClick={this.handleClickShowPassword}
                                            onMouseDown={this.handleMouseDownPassword}
                                        >
                                            {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12}>
                        <FormControl className={classNames(classes.margin, classes.textField)}>
                            <InputLabel htmlFor="adornment-password-confirm">Confirm</InputLabel>
                            <Input
                                id="adornment-password-confirm"
                                type={this.state.showPasswordConfirm ? 'text' : 'password'}
                                value={this.state.passwordConfirm}
                                onChange={this.handleChange('password')}
                                startAdornment={
                                    <InputAdornment position="start">
                                        <LockCircle />
                                    </InputAdornment>
                                }
                                endAdornment={
                                    <InputAdornment position="start">
                                        <IconButton
                                            aria-label="Toggle password visibility"
                                            onClick={this.handleClickShowPasswordConfirm}
                                            onMouseDown={this.handleMouseDownPassword}
                                        >
                                            {this.state.showPasswordConfirm ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                </Grid>
                <Grid container>
                <Button variant="raised" color="primary" className={classes.margin}>
                    Sign Up
                </Button>
                </Grid>
                </div>
            </div>
        );
    }
}
SignUp.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignUp);
