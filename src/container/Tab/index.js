import * as React from 'react';
import { connect } from 'react-redux';
import BigList from '../BigList';
import GridList from '../GridList';
import * as authActions from '../../redux/auth/actions';
import * as rosterActions from '../../redux/roster/actions';
import * as favoriteActions from '../../redux/favorite/actions';

class Tab extends React.PureComponent {

    state = {
        roster: []
    }

    componentDidMount() {
        console.log("Mount");
        const { tabChange, category, auth, rosterFetch, favoriteFetch } = this.props;
        tabChange(category);
        //handle roster message stanza in rest way rather then websocket
        //sad pubsub doesn't work in rest api way
        if (auth.oauth != null) {
            rosterFetch();
            favoriteFetch();
        }
    }
    render() {
        const { app, category } = this.props;
        
        let content = null;
        if (app.view === "Mobile") {
            content = <BigList category={category} />;
        } else {
            content = <GridList category={category} />
        }
        return (
            <React.Fragment>
                {
                    content
                }
                </React.Fragment>
        );
    }
    
}

function mapDispatchToProps(dispatch) {
    return {
        tabChange(tab) {
            dispatch(authActions.tabChange(tab));
        },
        rosterFetch() {
            dispatch(rosterActions.rosterFetch());
        },
        favoriteFetch() {
            dispatch(favoriteActions.favoriteFetch());
        }
    };
}

export default connect(state => ({
    app: state.App.toJS(),
    auth: state.Auth.toJS(),
}), mapDispatchToProps)(Tab);