import styled from 'styled-components';

const AppHolder = styled.div`
    .content {
        padding: 50px 0 56px 0;
    }
`;

export default AppHolder;