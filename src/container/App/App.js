import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Tabs } from 'antd';
import Topbar from '../Topbar';
import Header from '../Header';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '../BottomNavigation';
import AppRouter from './AppRouter';
import * as authActions from '../../redux/auth/actions';
import CssBaseline from '@material-ui/core/CssBaseline';

const TabPane = Tabs.TabPane;

const styles = theme => ({
    content: {
        padding: '56px 0 56px 0',
    }
});

function mapStateToProps(state, props) {
    return {
        // app: state.App.toJS(),
        // auth: state.Auth.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        logout() {
            dispatch(authActions.logout());
        }
    };
}
class App extends Component {
    state = {
        city: null,
        collapsed: true
    };

    render() {
        const { isLoggedIn, logout, classes } = this.props;

        return (
            <React.Fragment>
                <CssBaseline />
                <div className={classes.content}>
                    <Header logout={logout} />
                    <div className="fluid-container content">
                        <AppRouter isLoggedIn={isLoggedIn} />
                    </div>
                    <BottomNavigation />
                </div>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(App));