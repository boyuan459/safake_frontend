import React from 'react';
import { Switch, Route } from 'react-router-dom';
import asyncComponent from '../../helpers/AsyncFunc';
import Dashboard from '../Dashboard';
import { RestrictedRoute } from '../../router';
import requireAuth from '../HOC/RequireAuth';

export default class AppRouter extends React.Component {
    render() {
        const { isLoggedIn } = this.props;
        return (
            <Switch>
                <Route 
                    exact
                    path={'/dashboard'}
                    component={Dashboard}/>
                <Route 
                    exact
                    path={'/dashboard/food'}
                    component={asyncComponent(() => import('../Food'))}/>
                <Route 
                    exact
                    path={'/dashboard/fun'}
                    component={asyncComponent(() => import('../Fun'))}/>
                <Route 
                    exact
                    path={'/dashboard/me'}
                    component={requireAuth(asyncComponent(() => import('../Me')))}/>
                <Route 
                    exact
                    path={'/dashboard/publish'}
                    isLoggedIn={isLoggedIn}
                    component={requireAuth(asyncComponent(() => import('../Publish/PublishHost')))}/>
                <Route 
                    exact
                    path={'/dashboard/hosts/:id/edit'}
                    isLoggedIn={isLoggedIn}
                    component={requireAuth(asyncComponent(() => import('../Publish/PublishHost')))}/>
                <Route 
                    exact
                    path={'/dashboard/hosts'}
                    isLoggedIn={isLoggedIn}
                    component={requireAuth(asyncComponent(() => import('../HostList')))}/>
                <Route 
                    exact
                    path={'/dashboard/myfavorite'}
                    isLoggedIn={isLoggedIn}
                    component={requireAuth(asyncComponent(() => import('../MyFavorite')))}/>
            </Switch>
        );
    }
}