import React, { Component } from 'react';
import { List, Avatar, Icon, Button, Popconfirm } from 'antd';
import { Link } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getHosts, getHostLoading, getHostError } from '../../selectors';
import * as hostsActions from '../../redux/host/actions';
import HostDetail from '../../component/HostDetail';

const ListItem = List.Item;

const IconText = ({type, text}) => (
    <span>
        <Icon type={type} style={{ marginRight: 8 }} />
        {text}
    </span>
);

class HostList extends Component {
    state = {
        host: null,
        detailOpen: false,
    }
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const { hostsFetch } = this.props;
        hostsFetch();
    }

    handleDeleteConfirm = (id) => {
        console.log('Delete ', id);
        const { hostDelete } = this.props;
        hostDelete({id});
    }

    handleDeleteCancel = () => {

    }

    handleDetail = host => {
        this.setState({
            host,
            detailOpen: true,
        })
    }

    handleDetailClose = () => {
        this.setState({
            host: null,
            detailOpen: false
        })
    }

    render() {
        const { hosts, loading, user, app } = this.props;
        const { host, detailOpen } = this.state;

        return (
            <div style={{marginTop: 20}}>
            <List 
                itemLayout="vertical"
                size="large"
                dataSource={hosts}
                loading={loading}
                renderItem={item => (
                    <ListItem
                        style={{paddingLeft: 5}}
                        key={item._id}
                        actions={[
                            <Link to={`/dashboard/hosts/${item._id}/edit`}><IconText type="edit" text="Edit"/></Link>, 
                            <Popconfirm title="Are your sure to delete this?" onConfirm={() => this.handleDeleteConfirm(item._id)} onCancel={this.handleDeleteCancel} okText="Yes" cancelText="No"><Button type="dashed" shape="circle" icon="close" size="small" /></Popconfirm>,
                            <Button type="primary" size="small" icon="ellipsis" onClick={() => this.handleDetail(item)}>More</Button>
                        ]}
                        extra={item.images && item.images.length ? <img width={app.view === "Mobile" ? 120 : 350} alt="Image" src={item.images[0].url} /> : null}
                        >
                        <ListItem.Meta 
                            title={item.highlight}
                            description={`${item.address.city} ${item.address.state} ${item.address.postcode}`}/>
                            {item.detail}
                    </ListItem>
                )}
            />
            {
                    host ?
                        <HostDetail user={user} host={host} handleDetailClose={this.handleDetailClose} detailOpen={detailOpen} />
                        : null
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.Auth.toJS().user,
        hosts: getHosts(state),
        loading: getHostLoading(state),
        error: getHostError(state),
        app: state.App.toJS(),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        hostsFetch() {
            dispatch(hostsActions.hostsFetch());
        },
        hostDelete({id}) {
            dispatch(hostsActions.hostDelete(id));
        }
    };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HostList));