import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { message } from 'antd';
import { injectIntl } from 'react-intl';
import HostForm from '../Form/HostForm';
import { createHost, getHost, saveHost } from '../../services/host';

class PublishHost extends Component {
    state = {
        host: null
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        if (id != null) {
            getHost({id})
            .then(host => {
                console.log(host);
                this.setState({host});
            });
        }
    }
    prepareImages = images => {
        if (!images || !images.fileList) {
            return undefined;
        }
        const fileList = images.fileList;
        const validImages = fileList.filter(file => file.status === 'done');
        const newImages = validImages.map(file => {
            let image = {};
            image.uid = file.uid;
            image.name = file.name;
            image.url = (file.response && file.response.url) || file.url;
            image.status = file.status;
            image.thumbUrl = (file.response && file.response.url) || file.url;
            return image;
        })
        return newImages;
    }

    prepareAddress = addressStr => {
        const address = {};
        const addressArr = addressStr.split(",");
        address.postcode = addressArr[addressArr.length - 1];
        address.state = addressArr[addressArr.length - 2];
        address.city = addressArr[0];
        return address;
    }
    handleSave = () => {
        const { intl } = this.props;
        const form = this.hostForm;
        form.validateFields((error, values) => {
            if (error) {
                return;
            }
            console.log(values);
            const images = this.prepareImages(values['images']);
            const address = this.prepareAddress(values['address'].key);
            const startDate = values['startDate'] ? values['startDate'].format('YYYY-MM-DD') : undefined;
            const endDate = values['endDate'] ? values['endDate'].format('YYYY-MM-DD') : undefined;
            const id = values['id'];
            const host = Object.assign({}, values, { images, address, startDate, endDate });
            console.log(host);
            if (id != null) {
                //update
                saveHost(host)
                .then(host => {
                    message.success(`${intl.formatMessage({id: "form.host.publish.success"})}`)
                    console.log(host);
                }).catch(error => {
                    message.error(`${intl.formatMessage({id: "form.host.publish.failed"})}`)
                    console.log(error);
                });
                return;
            }
            createHost(host)
            .then(result => {
                message.success(`${intl.formatMessage({id: "form.host.publish.success"})}`)
                console.log(result);
            }).catch(error => {
                message.error(`${intl.formatMessage({id: "form.host.publish.failed"})}`)
                console.log(error);
            });
        });
    }
    render() {
        const { auth } = this.props;
        const access_token = auth.oauth && auth.oauth.access_token;
        const { host } = this.state;

        return (
            <HostForm item={host} ref={form => this.hostForm = form} handleSave={this.handleSave} token={access_token} />
        );
    }
}

export default withRouter(connect(state => ({
    auth: state.Auth.toJS()
}))(injectIntl(PublishHost)));