import styled from 'styled-components';
import { palette, font } from 'styled-theme';

const DashAppHoder = styled.div`
    font-family: ${font('primary', 0)};
    h1, h2, h3, h4, h5, h6, p, div, ul, li {
        
    }
`;

export default DashAppHoder;