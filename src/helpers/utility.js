import { Map, List } from 'immutable';

export function clearToken() {
    localStorage.removeItem('id_token');
}

export function getToken() {
    try {
        const idToken = localStorage.getItem('id_token');
        return new Map({ idToken });
    } catch (err) {
        clearToken();
        return new Map();
    }
}

export function getLinkFromMenu(item, url) {
    let link = '';
    if (item.link) {
        if (item.parentLink) {
            link = `${url}/${item.parentLink}/${item.link}`;
        } else {
            link = `${url}/${item.link}`;
        }
    } else {
        if (item.parentLink) {
            link = `${url}/${item.parentLink}`;
        } else {
            link = url;
        }
    }
    return link;
}

export const loadState = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) {
            return undefined;
        }
        const state = JSON.parse(serializedState);
        // console.log('Local state', state);
        if (Object.keys(state).length === 0 && state.constructor === Object) {
            return undefined;
        }
        // const menusState = new Map({
        //     menus: state.Menus.menus,
        //     menusLoading: state.Menus.menusLoading
        // });
        const authState = new Map({
            oauth: state.Auth.oauth,
            user: state.Auth.user,
            error: null,
            loading: false,
            subs: List(state.Auth.subs),
            subsFetched: state.Auth.subsFetched,
            tab: state.Auth.tab,
        });
        const subState = new Map({
            read: state.Sub && state.Sub.read ? Map(state.Sub.read) : Map(),
        });
        // const appState = new Map({
        //     tab: state.App.tab,
        //     view: state.App.view,
        //     height: state.App.height
        // });
        return {
            // Menus: menusState,
            Auth: authState,
            // App: appState,
            Sub: subState,
        };
    } catch (err) {
        console.log('load state error', err);
        return undefined;
    }
}

export const saveState = (state) => {
    try {
        if (state) {
            const obj = {};
            obj['Auth'] = state['Auth'];
            obj['App'] = state['App'];
            obj['Sub'] = state['Sub'];
            const serializedState = JSON.stringify(obj);
            localStorage.setItem('state', serializedState);
        }
    } catch (err) {
        console.error('save state error', err);
    }
}

export const clearState = () => {
    try {
        localStorage.removeItem('state');
    } catch (err) {
        console.log('clear state error', err);
    }
}

export const saveTokens = tokens => {
    try {
        localStorage.setItem('tokens', JSON.stringify(tokens));
    } catch (err) {
        console.error('save tokens error', err);
    }
}

export const getTokens = () => {
    try {
        const tokens = localStorage.getItem('tokens');
        return tokens ? JSON.parse(tokens) : undefined;
    } catch (err) {
        return undefined;
    }
}

export const clearTokens = () => {
    try {
        localStorage.removeItem('tokens');
    } catch (err) {
        console.error('clear tokens error', err);
    }
}

export const buildQueryString = params => {
    let index = 0;
    let queryString = '';
    for(var key in params) {
        index++;
        if (index === 1) {
            queryString += key + '=' + encodeURIComponent(params[key]);
        } else {
            queryString += '&' + key + '=' + encodeURIComponent(params[key]);
        }
    }
    return queryString;
}

export function getScroll() {
    if (window.pageYOffset != null) {
        //IE9+ and other browsers
        return {
            left: window.pageXOffset,
            top: window.pageYOffset
        };
    } else if (document.compatMode === 'CSS1Compat') {
        // detect quirk mode, have DTD <!DOCTYPE html> 
        return {
            left: document.documentElement.scrollLeft,
            top: document.documentElement.scrollTop
        };
    }
    return {
        left: document.body.scrollLeft,
        top: document.body.scrollTop
    };
}

export function getClient() {
    if (window.innerWidth != null) {
        //IE9+
        return {
            width: window.innerWidth,
            height: window.innerHeight
        };
    } else if (document.compatMode === "CSS1Compat") {
        // have DTD, not quirk mode
        return {
            width: window.documentElement.clientWidth,
            height: window.documentElement.clientHeight
        };
    }
    return {
        width: document.body.clientWidth,
        height: document.body.clientHeight
    };
}

export function getStyle(dom, attr) {
    if (dom.currentStyle) {
        //ie
        return dom.currentStyle[attr];
    } else {
        //standard, w3c
        return window.getComputedStyle(dom, null)[attr];
    }
}

export function throttle(fn, delay) {
    var timer = null;
    return function() {
        clearTimeout(timer);
        timer= setTimeout(fn, delay);
    }
}
