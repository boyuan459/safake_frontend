import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { connect } from 'react-redux';

import App from './container/App/App';
import Login from './container/Auth/Login/LoginBS';
import SignUp from './container/Auth/SignUp/SignUpBS';
import ForgotPassword from './container/Auth/ForgotPassword';
import ResetPassword from './container/Auth/ResetPassword';
import asyncComponent from './helpers/AsyncFunc';

const DefaultRoute = ({ ...rest }) =>
  <Route
    {...rest}
    render={props =>
       <Redirect
            to={{
              pathname: '/dashboard',
              state: { from: props.location }
            }}
            {...props}
          />}
  />;

export const RestrictedRoute = ({ component: Component, ...rest, isLoggedIn }) =>
  <Route
    {...rest}
    render={props =>
      isLoggedIn
        ? <Component {...props} />
        : <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location }
            }}
          />}
  />;
const PublicRoutes = ({ history, isLoggedIn }) => {
  return (
    <ConnectedRouter history={history}>
      <div>
        <DefaultRoute
          exact
          path={'/'}
          isLoggedIn={isLoggedIn}
        />
        <Route
          path="/dashboard"
          component={App}
          isLoggedIn={isLoggedIn}
        />
        <Route 
          path="/login"
          component={Login}
        />
        <Route 
          path="/signup"
          component={SignUp}/>
        <Route 
          path="/password/forgot"
          component={ForgotPassword}/>
        <Route 
          path="/password/reset/:token"
          component={ResetPassword}/>
      </div>
    </ConnectedRouter>
  );
};

export default connect(state => ({
  isLoggedIn: !!state.Auth.toJS().oauth
}))(PublicRoutes);
