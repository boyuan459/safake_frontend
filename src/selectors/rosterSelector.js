import _ from 'lodash';
import { createSelector } from 'reselect';

export const getRosterData = state => _.get(state, 'Roster', {});

export const getRoster = createSelector([getRosterData], item => {
    return item.get('roster').toArray();
});