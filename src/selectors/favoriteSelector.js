import _ from 'lodash';
import { createSelector } from 'reselect';

export const getFavoritesData = state => _.get(state, "Favorites", {});

export const getFavorites = createSelector([getFavoritesData], item => {
    return item.get('favorites').toArray();
});