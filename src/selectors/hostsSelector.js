import _ from 'lodash';
import { createSelector } from 'reselect';

export const getHostsData = state => _.get(state, "Hosts", {});

export const getHosts = createSelector([getHostsData], item => {
    return item.get('hosts').toArray();
});

export const getHostLoading = createSelector([getHostsData], item => {
    return item.get('loading');
});

export const getHostRefreshing = createSelector([getHostsData], item => {
    return item.get('refreshing');
});

export const getHostError = createSelector([getHostsData], item => {
    return item.get('error');
});

export const getHostTotalRows = createSelector([getHostsData], item => {
    return item.get('totalRows');
});

export const getHostPage = createSelector([getHostsData], item => {
    return item.get('page');
});

export const getHostHasMore = createSelector([getHostsData], item => {
    return item.get('hasMore');
});

export const getLikes = createSelector([getHostsData], item => {
    return item.get('likes').toArray();
});