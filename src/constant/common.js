export const VALID_IMAGE_TYPES = ["image/gif", "image/png", "image/jpeg", "image/bmp", "image/webp", "image/jpg"];
export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;
