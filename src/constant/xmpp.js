export const NS_MUC = 'http://jabber.org/protocol/muc';
export const NS_MUC_USERS = 'http://jabber.org/protocol/muc#user';
export const NS_MUC_LIGHT_AFFILIATIONS = 'urn:xmpp:muclight:0#affiliations';
export const NS_DATA_FORMS = 'jabber:x:data';
export const NS_PUBSUB = 'http://jabber.org/protocol/pubsub';
export const NS_PUBSUB_OWNER = 'http://jabber.org/protocol/pubsub#owner';
export const NS_PUBSUB_ERRORS = 'http://jabber.org/protocol/pubsub#errors';
export const NS_PUBSUB_NODE_CONFIG = 'http://jabber.org/protocol/pubsub#node_config';
export const NS_DISCO_ITEMS = 'http://jabber.org/protocol/disco#items';
export const Message = {
    Domain: process.env.REACT_APP_MESSAGE_DOMAIN,
    Connection: process.env.REACT_APP_MESSAGE_CONNECTION,
    PubsubDomain: process.env.REACT_APP_MESSAGE_PUBSUB_DOMAIN
};