import axiosInstance from './axiosInstance';

export function createHost(host) {
    return axiosInstance.post(`/api/hosts`, host)
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function getHosts() {
    return axiosInstance.get(`/api/hosts`)
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function getHost({id}) {
    return axiosInstance.get(`/api/hosts/${id}`)
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function saveHost(host) {
    return axiosInstance.put(`/api/hosts/${host.id}`, host)
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function deleteHost({id}) {
    return axiosInstance.delete(`/api/hosts/${id}`)
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function searchHost({city, sort, page, startIndex, stopIndex, category}) {
    return axiosInstance.get(`/api/search/${category}`, { 
        params: {
            page: page,
            city: city,
            sort: sort,
            startIndex: startIndex,
            stopIndex: stopIndex
        } })
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function getLikes(category) {
    return axiosInstance.get(`/api/likes?category=${category}`)
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function likeHost(host_id) {
    return axiosInstance.post('/api/likes', {host_id})
        .then(result => {
            // console.log('Service like', result);
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        })
}

export function dislikeHost(host_id) {
    return axiosInstance.delete(`/api/likes/${host_id}`)
        .then(result => {
            // console.log('Service like', result);
            return Promise.resolve(result.data);
        }).catch(error => {
            return Promise.reject(error.data);
        })
}

export function commentHost({host_id, comment}) {
    return axiosInstance.post('/api/comments', { host_id, comment })
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        })
}