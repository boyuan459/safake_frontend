import axiosInstance from './axiosInstance';

export function createPubsub(pubsub) {
    return axiosInstance.post(`/api/pubs`, pubsub)
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function createPersonalPub(node) {
    return axiosInstance.post('/api/user/pub', {node})
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function getPersonlPub(jid) {
    return axiosInstance.get(`/api/user/pub?jid=${encodeURIComponent(jid)}`)
            .then(result => {
                return Promise.resolve(result.data);
            })
            .catch(error => {
                return Promise.reject(error.data);
            });
}

export function searchPubsub(name) {
    return axiosInstance.get(`/api/pubs?name=${encodeURIComponent(name)}`)
            .then(result => {
                return Promise.resolve(result.data);
            })
            .catch(error => {
                return Promise.reject(error.data);
            });
}

export function createSub(sub) {
    return axiosInstance.post('/api/subs', sub)
    .then(result => {
        return Promise.resolve(result.data);
    })
    .catch(error => {
        return Promise.reject(error.data);
    });
}

export function getSubs() {
    return axiosInstance.get('/api/subs')
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error);
        });
}

export function shareHost({to, stanza}) {
    return axiosInstance.post('/api/im/share', {to, stanza})
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error);
        });
}