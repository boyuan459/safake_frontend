import axiosInstance from './axiosInstance';

const OAUTH = {
    CLIENT_ID: process.env.REACT_APP_CLIENT_ID,
    CLIENT_SECRET: process.env.REACT_APP_CLIENT_SECRET
};

export function signup(user) {
    return axiosInstance.post('/api/register', user)
    .then(result => {
        console.log(result);
        // if (result.status !== 200) {
        //     return Promise.reject(result);
        // }
        return Promise.resolve(result.data);
    })
    .catch(error => {
        console.log(error);
        return Promise.reject(error)
    });
}

export function signin({username, password}) {
    var form_params = {
        grant_type: 'password',
        client_id: OAUTH.CLIENT_ID,
        client_secret: OAUTH.CLIENT_SECRET,
        username: username,
        password: password,
        scope: '*'
    };
    console.log(form_params);
    return axiosInstance.post('/oauth/token', {
        ...form_params
    }).then(result => {
        console.log(result);
        if (result.status !== 200) {
            return Promise.reject(result);
        }
        return Promise.resolve(result.data);
    }).catch(error => {
        console.log(error);
        return Promise.reject(error.data);
    });
}

export function getUser(token) {
    return axiosInstance.get('/api/user', { headers: {"Authorization" : `Bearer ${token}`} })
            .then(result => {
                if (result.status !== 200) {
                    return Promise.reject(result);
                }
                return Promise.resolve(result.data);
            }).catch(error => {
                return Promise.reject(error.data);
            });
}

export function searchUser(username) {
    return axiosInstance.get(`/api/user/search?username=${encodeURIComponent(username)}`)
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function sendResetPasswordLink(email) {
    return axiosInstance.post('/api/password/email', { email })
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        });
}

export function resetPassword({email, password, password_confirmation, token}) {
    return axiosInstance.post('/api/password/reset', {email, password, password_confirmation, token})
        .then(result => {
            return Promise.resolve(result.data);
        }).catch(error => {
            return Promise.reject(error.data);
        })
}