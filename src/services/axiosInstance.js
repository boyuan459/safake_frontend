import { apiConfig } from '../config';
import axios from 'axios';
import { store } from '../redux/store';

const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_API_BASE_URL
});

function select(state) {
    const auth = state.Auth.toJS();
    return auth.oauth && auth.oauth.access_token;
}
// function listener() {
//     let access_token = select(store.getState());
//     axiosInstance.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;

// }
// store.subscribe(listener);

// axiosInstance.defaults.headers.common['Authorization'] = jwtToken;
axiosInstance.defaults.headers.post['Content-Type'] = 'application/json';
axiosInstance.interceptors.request.use(function(config) {
    const jwtToken = select(store.getState());
    // const jwtToken = 'eyJraWQiOiI1aU1wUHI2Vk9YN1F1YkNyem5YRlN1OHZzaTZmY0tyUXh6VUpjT0U4bDNZPSIsImFsZyI6IlJTMjU2In0.eyJhdF9oYXNoIjoiRWRScmtDZXZFTFhfWTZOWThpcXFHUSIsInN1YiI6Ijk5MjgzN2NmLTAwOGEtNDBiZS05ZmVjLTE4YmZjMTFhYTUwNSIsImF1ZCI6IjRscW50MDg5cjg3czUwOThpMXBjdTFwdGUyIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImV2ZW50X2lkIjoiNDhmMzFhMTYtMzQxYy0xMWU4LTg4M2ItOGQxN2NmNWI4YjMyIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE1MjI0MTU2NzIsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aGVhc3QtMi5hbWF6b25hd3MuY29tXC9hcC1zb3V0aGVhc3QtMl83V0ZoS3BReWciLCJjb2duaXRvOnVzZXJuYW1lIjoiYm95dWFuIiwiZXhwIjoxNTIyNDE5MjcyLCJpYXQiOjE1MjI0MTU2NzIsImVtYWlsIjoiYm8ueXVhbkBzaG9wcGVybWVkaWEuY29tLmF1In0.QPdbuiO29nOmQor-Uf9ZV-a8suULaCQVsGZbHpppyRrfICwxnZIEe3YxtWR_wThdIx8jLMtyJlnGZtBACMEm60-mBVkbYeHMRaFAa1IYgsXt6QC_igtWZtSXQRFaWU8F5Py-HsSL7WytdIV-PNz6YykS5n3R_INQYa0Rxr2xNhQaOnEl3thcFm4cO5pLG0iWu2TgLwH0XDE_Iu7Gmu8xCcHvm5tWC3_aVPr5CK-MnVL0RoRIiwQjKdj6LyvfO4VO1QK4sIRP2dvK8t0fmgX42ZYr84Zxo0Ho1wQ3EvqP1OfDPZS3rVzjqCVk-EuEGtFFjIyXjWAg84TT8VJijLlpSQ';
    // console.log("Oauth token", jwtToken);
    if (jwtToken != null) {
        config.headers.Authorization = 'Bearer ' + jwtToken;
    }
    return config;
}, function(err) {
    return Promise.reject(err);
});
axiosInstance.interceptors.response.use((response) => {
    return response;
}, function (error) {
    // Do something with response error
    //process.env.REACT_APP_DEV_MODE && console.log('error', JSON.stringify(error));
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        //process.env.REACT_APP_DEV_MODE && console.log('error response', JSON.stringify(error.response));
    }
    if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        //process.env.REACT_APP_DEV_MODE && console.log('error request', JSON.stringify(error.request));
    }
    if (error.message) {
        // Something happened in setting up the request that triggered an Error
        //process.env.REACT_APP_DEV_MODE && console.log('error message', JSON.stringify(error.message));
    }
    if (error.config) {
        //process.env.REACT_APP_DEV_MODE && console.log('error config', JSON.stringify(error.config));
    }

    if ((error.response && error.response.status === 401) || error.message === "Network Error") {
        //https://github.com/axios/axios/issues/204 axios does not resolve/reject 401 error
        //because the browser hide this
        console.log('unauthorized, logging out ...');
        // alert("Network error, please refresh page to solve this problem, if problem persists, please contact the support!");
        // logout();
        // window.location.reload();
        // router.replace('/auth/login');
    }
    return Promise.reject(error.response);
});

export default axiosInstance;