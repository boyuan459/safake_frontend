import axiosInstance from './axiosInstance';

export function getHome() {
    return axiosInstance.get(`/api/home`)
        .then(result => {
            if (result.status !== 200) {
                return Promise.reject(result);
            }
            return Promise.resolve(result.data);
        })
        .catch(error => {
            console.log(error);
            return Promise.reject(error.data);
        });
}