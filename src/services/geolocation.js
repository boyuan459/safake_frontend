import axiosInstance from './axiosInstance';

export function citySearch(city) {
    return axiosInstance.get(`api/geo/search/${encodeURIComponent(city)}`)
        .then(result => {
            if (result.status !== 200) {
                return Promise.reject(result);
            }
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        })
}