import axiosInstance from './axiosInstance';

export function getFavorites() {
    return axiosInstance.get('/api/favorite')
        .then(result => {
            return Promise.resolve(result.data);
        }).catch(error => {
            return Promise.reject(error.data);
        })
}

export function createFavorite(host_id) {
    return axiosInstance.post('/api/favorite', { host_id })
        .then(result => {
            return Promise.resolve(result.data);
        }).catch(error => {
            return Promise.reject(error.data);
        })
}

export function unfavorite(host_id) {
    return axiosInstance.delete(`/api/favorite/${host_id}`)
        .then(result => {
            return Promise.resolve(result.data);
        }).catch(error => {
            return Promise.reject(error.data);
        })
}