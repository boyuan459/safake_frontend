import axiosInstance from './axiosInstance';

export function getRoster() {
    return axiosInstance.get('/api/im/roster')
        .then(result => {
            return Promise.resolve(result.data);
        })
        .catch(error => {
            return Promise.reject(error.data);
        })
}

export function createRoster({user, usernick, group}) {
    return axiosInstance.post('/api/im/roster', {
        user, usernick, group
    }).then(result => {
        return Promise.resolve(result.data);
    }).catch(error => {
        return Promise.reject(error.data);
    })
}

export function sendMessage({to, body}) {
    return axiosInstance.post('/api/im/message', {to, body})
        .then(result => {
            return Promise.resolve(result.data);
        }).catch(error => {
            return Promise.reject(error.data);
        });
}