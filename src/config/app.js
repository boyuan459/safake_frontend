const app = {
    message_domain: '@localhost',
    muc_host: '@muc.localhost',
    pubsub_host: '@pubsub.localhost',
}

export default app;