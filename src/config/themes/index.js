import themedefault from './themedefault';
import theme2 from './theme2';
import theme from './theme';

const themes = {
  themedefault,
  theme2,
  theme
};
export default themes;
