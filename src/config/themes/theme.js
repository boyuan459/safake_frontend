import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#2196f3',
        },
        type: 'light',
    },
    typography: {
        // In Japanese the characters are usually larger.
        fontSize: 16,
        fontWeightMedium: 500,
        // htmlFontSize: 16,
        body1: {
            // fontWeight: 500,
            fontSize: 12,
        }
    },
    overrides: {
        MuiBottomNavigationAction: {
            selected: {
                fontSize: '1.5rem !important',
            }
        }
    },
    props: {
        MuiButtonBase: {
            // disableRipple: true,
        }
    }
});

export default theme;