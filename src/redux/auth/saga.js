import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { signin, getUser } from '../../services/auth';
import { createPubsub, createSub, getSubs, createPersonalPub } from '../../services/pubsub';

import * as actions from './actions';

export function* login() {
    yield takeEvery(actions.LOGIN, function*(action) {
        try {
            const result = yield call(signin, action.user);
            // yield put(actions.userFetch());
            const user = yield call(getUser, result.access_token);
            yield put(actions.userFetchSuccess(user));
            yield put(actions.loginSuccess(result));
        } catch(error) {
            yield put(actions.loginFailed(error));
        }
    });
}

export function* loginSuccess() {
    yield takeEvery(actions.LOGIN_SUCCESS, function*(action) {
        console.log("Login success, redirect to home");
        yield put(push('/dashboard'));
        //change tab to home
        yield put(actions.tabChange('host'));
    });
}

export function* logout() {
    yield takeEvery(actions.LOGOUT, function*(action) {
        yield put(push('/'));
    });
}

export function* fetchUser() {
    yield takeEvery(actions.USER_FETCH, function*(action) {
        try {
            const user = yield call(getUser);
            yield put(actions.userFetchSuccess(user));
        } catch(error) {
            yield put(actions.userFetchFailed(error));
        }
    });
}

export function* handleCreatePubsub() {
    yield takeEvery(actions.CREATE_PUBSUB, function*(action) {
        try {
            const pubsub = yield call(createPubsub, action.pubsub);
            console.log('SAGA create pubsub', pubsub);
            yield put(actions.createPubsubSuccess(pubsub));
        } catch(error) {
            yield put(actions.createPubsubFailed(error));
        }
    });
}

export function* handleCreatePersonalPub() {
    yield takeEvery(actions.PERSONAL_PUB_CREATE, function*(action) {
        try {
            const user = yield call(createPersonalPub, action.node);
            console.log('SAGA create personal pub', user);
            yield put(actions.personalPubCreateSuccess(user.node));
        } catch(error) {
            yield put(actions.personalPubCreateFailed(error));
        }
    });
}

export function* handleSubCreate() {
    yield takeEvery(actions.SUB_CREATE, function*(action) {
        try {
            const sub = yield call(createSub, action.sub);
            console.log('SAGA create sub', sub);
            yield put(actions.subCreateSuccess(sub));
        } catch(error) {
            yield put(actions.subCreateFailed(error));
        }
    });
}

export function* handleSubsFetch() {
    yield takeEvery(actions.SUBS_FETCH, function*(action) {
        try {
            const subs = yield call(getSubs);
            console.log('SAGA fetch subs', subs);
            yield put(actions.subsFetchSuccess(subs));
        } catch(error) {
            yield put(actions.subsFetchFailed(error));
        }
    });
}

export default function* rootSaga() {
    yield all([
        fork(login),
        fork(loginSuccess),
        fork(fetchUser),
        fork(handleCreatePubsub),
        fork(handleSubCreate),
        fork(handleSubsFetch),
        fork(handleCreatePersonalPub),
    ]);
}