export const LOGIN='LOGIN';
export const LOGIN_SUCCESS='LOGIN_SUCCESS';
export const LOGIN_FAILED='LOGIN_FAILED';
export const LOGOUT='LOGOUT';
export const USER_FETCH='USER_FETCH';
export const USER_FETCH_SUCCESS='USER_FETCH_SUCCESS';
export const USER_FETCH_FAILED='USER_FETCH_FAILED';
export const CREATE_PUBSUB = 'CREATE_PUBSUB';
export const CREATE_PUBSUB_SUCCESS = 'CREATE_PUBSUB_SUCCESS';
export const CREATE_PUBSUB_FAILED = 'CREATE_PUBSUB_FAILED';
export const PERSONAL_PUB_CREATE = 'PERSONAL_PUB_CREATE';
export const PERSONAL_PUB_CREATE_SUCCESS = 'PERSONAL_PUB_CREATE_SUCCESS';
export const PERSONAL_PUB_CREATE_FAILED = 'PERSONAL_PUB_CREATE_FAILED';
export const SUBS_FETCH = 'SUBS_FETCH';
export const SUBS_FETCH_SUCCESS = 'SUBS_FETCH_SUCCESS';
export const SUBS_FETCH_FAILED = 'SUBS_FETCH_FAILED';
export const SUB_CREATE = 'SUB_CREATE';
export const SUB_CREATE_SUCCESS = 'SUB_CREATE_SUCCESS';
export const SUB_CREATE_FAILED = 'SUB_CREATE_FAILED';
export const TAB_CHANGE = 'TAB_CHANGE';
export const AVATAR_CHANGE = 'AVATAR_CHANGE';

export const login = user => ({
    type: LOGIN,
    user
});

export const loginSuccess = result => ({
    type: LOGIN_SUCCESS,
    result
});

export const loginFailed = error => ({
    type: LOGIN_FAILED,
    error
});

export const logout = () => ({
    type: LOGOUT
});

export const userFetch = () => ({
    type: USER_FETCH
});

export const userFetchSuccess = user => ({
    type: USER_FETCH_SUCCESS,
    user
});

export const userFetchFailed = error => ({
    type: USER_FETCH_FAILED,
    error
});

export const createPubsub = pubsub => ({
    type: CREATE_PUBSUB,
    pubsub
});

export const createPubsubSuccess = pubsub => ({
    type: CREATE_PUBSUB_SUCCESS,
    pubsub
});

export const createPubsubFailed = error => ({
    type: CREATE_PUBSUB_FAILED,
    error
});

export const personalPubCreate= node => ({
    type: PERSONAL_PUB_CREATE,
    node
});

export const personalPubCreateSuccess = node => ({
    type: PERSONAL_PUB_CREATE_SUCCESS,
    node
});

export const personalPubCreateFailed = error => ({
    type: PERSONAL_PUB_CREATE_FAILED,
    error
});

export const subCreate = sub => ({
    type: SUB_CREATE,
    sub
});

export const subCreateSuccess = sub => ({
    type: SUB_CREATE_SUCCESS,
    sub
});

export const subCreateFailed = error => ({
    type: SUB_CREATE_FAILED,
    error
});

export const subsFetch = () => ({
    type: SUBS_FETCH
});

export const subsFetchSuccess = subs => ({
    type: SUBS_FETCH_SUCCESS,
    subs
});

export const subsFetchFailed = error => ({
    type: SUBS_FETCH_FAILED,
    error
});

export const tabChange = tab => ({
    type: TAB_CHANGE,
    tab
});

export const avatarChange = avatar => ({
    type: AVATAR_CHANGE,
    avatar
});