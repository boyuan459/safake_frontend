import { Map, List } from 'immutable';
import * as actions from './actions';

const initState = new Map({
    oauth: null,
    user: null,
    error: null,
    loading: false,
    subsFetched: false,
    subs: List(),
    tab: 'host'
});

export default function authReducer(state = initState, action) {
    switch (action.type) {
        case actions.LOGIN:
            return state.set('loading', true);
        case actions.LOGIN_SUCCESS:
            return state.set('loading', false)
                    .set('error', null)
                    .set('oauth', action.result);
        case actions.LOGIN_FAILED:
            return state.set('loading', false)
                    .set('error', action.error)
        case actions.LOGOUT:
            return state.set('oauth', null)
                    .set('user', null)
                    .set('error', null)
                    .set('subsFetched', false)
                    .set('subs', List());
        case actions.USER_FETCH:
            return state.set('loading', true);
        case actions.USER_FETCH_SUCCESS:
            return state.set('user', action.user)
                    .set('loading', false);
        case actions.USER_FETCH_FAILED:
            return state.set('loading', false)
                    .set('error', action.error);
        case actions.AVATAR_CHANGE:
            const avatarUser = state.get('user');
            avatarUser.avatar = action.avatar;
            return state.set('user', avatarUser);
        case actions.CREATE_PUBSUB:
            return state.set('loading', true);
        case actions.CREATE_PUBSUB_SUCCESS:
            const user = state.get('user');
            user.pubsubs = [action.pubsub];
            return state.set('user', user)
                    .set('loading', false);
        case actions.CREATE_PUBSUB_FAILED:
            return state.set('loading', false)
                    .set('error', action.error);
        case actions.SUB_CREATE:
            return state.set('loading', true);
        case actions.SUB_CREATE_SUCCESS:
            let subs = null;
            if (state.get('subs').isEmpty()) {
                subs = List([action.sub]);
            } else {
                let found = state.get('subs').find(item => item.node === action.sub.node);
                if (found != null) {
                    return state.set('loading', false);
                }
                subs = state.get('subs').push(action.sub);
            }
            return state.set('subs', subs)
                    .set('loading', false);
        case actions.SUB_CREATE_FAILED:
            return state.set('loading', false)
                    .set('error', action.error);
        case actions.SUBS_FETCH:
            return state.set('loading', true);
        case actions.SUBS_FETCH_SUCCESS:
            return state.set('loading', false)
                    .set('subs', List(action.subs));
        case actions.SUBS_FETCH_FAILED:
            return state.set('loading', false)
                    .set('error', action.error);
        case actions.PERSONAL_PUB_CREATE:
            return state;
        case actions.PERSONAL_PUB_CREATE_SUCCESS:
            const newUser = state.get('user');
            newUser.node = action.node;
            return state.set('user', newUser);
        case actions.PERSONAL_PUB_CREATE_FAILED:
            return state.set('error', action.error);
        case actions.TAB_CHANGE:
            return state.set('tab', action.tab);
        default:
            return state;
    }
}