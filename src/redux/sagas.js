import { all } from 'redux-saga/effects';
import authSagas from './auth/saga';
import hostSagas from './host/saga';
import rosterSagas from './roster/saga';
import favoriteSagas from './favorite/saga';

export default function* rootSaga(getState) {
    yield all([
        authSagas(),
        hostSagas(),
        rosterSagas(),
        favoriteSagas()
    ]);
}