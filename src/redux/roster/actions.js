export const ROSTER_FETCH = 'ROSTER_FETCH';
export const ROSTER_FETCH_SUCCESS = 'ROSTER_FETCH_SUCCESS';
export const ROSTER_FETCH_FAILED = 'ROSTER_FETCH_FAILED';
export const ROSTER_CREATE = 'ROSTER_CREATE';
export const ROSTER_CREATE_SUCCESS = 'ROSTER_CREATE_SUCCESS';
export const ROSTER_CREATE_FAILED = 'ROSTER_CREATE_FAILED';

export const rosterFetch = () => ({
    type: ROSTER_FETCH
});

export const rosterFetchSuccess = result => ({
    type: ROSTER_FETCH_SUCCESS,
    result
});

export const rosterFetchFailed = error => ({
    type: ROSTER_FETCH_FAILED,
    error
});

export const rosterCreate = item => ({
    type: ROSTER_CREATE,
    item
});

export const rosterCreateSuccess = result => ({
    type: ROSTER_CREATE_SUCCESS,
    result
});

export const rosterCreateFailed = error => ({
    type: ROSTER_CREATE_FAILED,
    error
});