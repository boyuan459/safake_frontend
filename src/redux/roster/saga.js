import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import * as actions from './actions';
import { getRoster, createRoster } from '../../services/im';

export function* handleRosterFetch() {
    yield takeEvery(actions.ROSTER_FETCH, function*(action) {
        try {
            const roster = yield call(getRoster);
            yield put(actions.rosterFetchSuccess(roster));
        } catch(error) {
            yield put(actions.rosterFetchFailed(error));
        }
    });
}

export function* handleRosterCreate() {
    yield takeEvery(actions.ROSTER_CREATE, function*(action) {
        try {
            const result = yield call(createRoster, action.item);
            yield put(actions.rosterCreateSuccess(action.item));
        } catch(error) {
            yield put(actions.rosterCreateFailed(error));
        }
    });
}

export default function* rootSaga() {
    yield all([
        fork(handleRosterFetch),
        fork(handleRosterCreate),
    ]);
}