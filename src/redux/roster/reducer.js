import { Map, List } from 'immutable';
import * as Actions from './actions';

const initState = new Map({
    roster: List(),
    error: null,
    loading: false
});

export default function rosterReducer(state = initState, action) {
    switch(action.type) {
        case Actions.ROSTER_FETCH:
            return state.set('loading', true);
        case Actions.ROSTER_FETCH_SUCCESS:
            return state.set('roster', List(action.result))
                    .set('loading', false);
        case Actions.ROSTER_FETCH_FAILED:
            return state.set('error', action.error)
                    .set('loading', false);
        case Actions.ROSTER_CREATE:
            return state.set('loading', true);
        case Actions.ROSTER_CREATE_SUCCESS:
            const item = {
                jid: action.result.user,
                nick: action.result.usernick,
                group: action.result.group,
                subscription: 'both'
            };
            return state.set('roster', state.get('roster').push(item))
                .set('loading', false);
        case Actions.ROSTER_CREATE_FAILED:
            return state.set('error', action.error)
                .set('loading', false);
        default:
            return state;
    }
}