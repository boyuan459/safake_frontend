import { all, takeEvery, takeLatest, put, fork, call } from 'redux-saga/effects';
import * as actions from './actions';
import { getHosts, deleteHost, searchHost, getLikes, likeHost, dislikeHost, commentHost } from '../../services/host';

export function* handleHostsFetch() {
    yield takeEvery(actions.HOSTS_FETCH, function*(action) {
        try {
            const hosts = yield call(getHosts);
            yield put(actions.hostsFetchSuccess(hosts));
        } catch(error) {
            yield put(actions.hostsFetchFailed(error));
        }
    });
}

export function* handleHostDelete() {
    yield takeEvery(actions.HOST_DELETE, function*(action) {
        try {
            const result = yield call(deleteHost, {id: action.id})
            yield put(actions.hostDeleteSuccess(result));
        } catch(error) {
            yield put(actions.hostDeleteFailed(error));
        }
    });
}

export function* handleHostsSearch() {
    yield takeEvery(actions.HOSTS_SEARCH, function*(action) {
        try {
            const data = yield call(searchHost, action.params);
            const result = { data: data, params: action.params };
            console.log("Search hosts saga", result);
            yield put(actions.hostsSearchSuccess(result));
        } catch(error) {
            yield put(actions.hostsSearchFailed(error));
        }
    });
}

export function* handleLikesFetch() {
    yield takeEvery(actions.LIKES_FETCH, function*(action) {
        try {
            const likes = yield call(getLikes, action.category);
            yield put(actions.likesFetchSuccess(likes));
        } catch(error) {
            yield put(actions.likesFetchFailed(error));
        }
    });
}

export function* handleLike() {
    yield takeEvery(actions.HOST_LIKE, function*(action) {
        try {
            const like = yield call(likeHost, action.host_id);
            yield put(actions.hostLikeSuccess(like));
        } catch(error) {
            yield put(actions.hostLikeFailed(error));
        }
    });
}

export function* handleDislike() {
    yield takeEvery(actions.HOST_DISLIKE, function*(action) {
        try {
            const dislike = yield call(dislikeHost, action.host_id);
            yield put(actions.hostDislikeSuccess(dislike));
        } catch(error) {
            yield put(actions.hostDislikeFailed(error));
        }
    });
}

export function* handleComment() {
    yield takeEvery(actions.HOST_COMMENT, function*(action) {
        try {
            const host = yield call(commentHost, action.comment);
            yield put(actions.hostCommentSuccess(host));
        } catch(error) {
            yield put(actions.hostCommentFailed(error));
        }
    });
}

export default function* rootSaga() {
    yield all([
        fork(handleHostsFetch),
        fork(handleHostDelete),
        fork(handleHostsSearch),
        fork(handleLikesFetch),
        fork(handleLike),
        fork(handleDislike),
        fork(handleComment)
    ]);
}