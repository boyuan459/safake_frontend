import { Map, List } from 'immutable';
import * as Actions from './actions';

const initState = new Map({
    hosts: List(),
    loading: false,
    error: null,
    totalRows: 0,
    page: 1,
    hasMore: false,
    refreshing: false,
    likes: List(),
});

export default function hostReducer(state = initState, action) {
    switch(action.type) {
        case Actions.HOSTS_FETCH:
            return state.set('loading', true);
        case Actions.HOSTS_FETCH_SUCCESS:
            return state.set('hosts', new List(action.hosts))
                    .set('loading', false);
        case Actions.HOSTS_FETCH_FAILED:
            return state.set('error', action.error)
                    .set('loading', false);
        case Actions.HOST_DELETE:
            return state.set('loading', true);
        case Actions.HOST_DELETE_SUCCESS:
            const hosts = state.get('hosts').filter(item => item._id !== action.result._id)
            return state.set('hosts', hosts)
                    .set('loading', false);
        case Actions.HOST_DELETE_FAILED:
            return state.set('error', action.error)
                    .set('loading', false);
        case Actions.HOSTS_SEARCH:
            if (action.params.page === 1) {
                return state.set('refreshing', true);
            }
            return state.set('loading', true);
        case Actions.HOSTS_SEARCH_SUCCESS:
            const page = action.result.params.page;
            if (page != null) {
                //first query for big list or query for grid list
                if (page === 1) {
                    return state.set('hosts', new List(action.result.data.data))
                            .set('totalRows', action.result.data.total)
                            .set('refreshing', false)
                            .set('page', 2)
                            .set('hasMore', action.result.data.data.length < action.result.data.total);
                } else {
                    let hosts = state.get('hosts').concat(action.result.data.data);
                    let nextPage = page + 1;
                    return state.set('hosts', hosts)
                            .set('totalRows', action.result.data.total)
                            .set('loading', false)
                            .set('page', nextPage)
                            .set('hasMore', hosts.size < action.result.data.total)
                }
            } else {
                //Big List not first query
                let startIndex = action.result.params.startIndex;
                let stopIndex = action.result.params.stopIndex;
                let hosts = state.get('hosts').toArray();
                for(let i=startIndex;i<stopIndex || i < action.result.data.length+startIndex;i++) {
                    hosts[i] = action.result.data[i-startIndex];
                }
                return state.set('hosts', new List(hosts))
                        .set('loading', false);
            }
        case Actions.LIKES_FETCH:
            return state.set('loading', true);
        case Actions.LIKES_FETCH_SUCCESS:
            return state.set('likes', new List(action.likes))
                .set('loading', false);
        case Actions.LIKES_FETCH_FAILED:
            return state.set('error', action.error)
                .set('loading', false);
        case Actions.HOST_LIKE_SUCCESS:
            //find the host
            let likeHosts = state.get('hosts');
            let index = likeHosts.findIndex(item => item._id === action.like.host_id);
            console.log('Like host index', index);
            likeHosts = likeHosts.update(index, host => {
                host.likes = host.likes ? host.likes + 1 : 1;
                return host;
            });
            return state.set('likes', state.get('likes').push(action.like))
                    .set('hosts', likeHosts);
        case Actions.HOST_LIKE_FAILED:
            return state.set('error', action.error);
        case Actions.HOST_DISLIKE_SUCCESS:
            //find the host
            let dislikeHosts = state.get('hosts');
            let dislikeIndex = dislikeHosts.findIndex(item => item._id === action.dislike.host_id);
            console.log('Dislike host index', dislikeIndex);
            dislikeHosts = dislikeHosts.update(dislikeIndex, host => {
                host.likes = host.likes ? host.likes - 1 : 0;
                return host;
            });
            return state.set('hosts', dislikeHosts)
                    .set('likes', state.get('likes').filter(item => item.host_id !== action.dislike.host_id));
        case Actions.HOST_DISLIKE_FAILED:
            return state.set('error', action.error);
        case Actions.HOST_COMMENT_SUCCESS:
            //find the host
            let commentHosts = state.get('hosts');
            let commentIndex = commentHosts.findIndex(item => item._id === action.host._id);
            commentHosts = commentHosts.update(commentIndex, host => action.host);
            return state.set('hosts', commentHosts);
        case Actions.HOST_COMMENT_FAILED:
            return state.set('error', action.error);
        default:
            return state;
    }
}