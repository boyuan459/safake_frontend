export const HOST_DELETE = 'HOST_DELETE';
export const HOST_DELETE_SUCCESS = 'HOST_DELETE_SUCCESS';
export const HOST_DELETE_FAILED = 'HOST_DELETE_FAILED';
export const HOSTS_FETCH = 'HOSTS_FETCH';
export const HOSTS_FETCH_SUCCESS = 'HOSTS_FETCH_SUCCESS';
export const HOSTS_FETCH_FAILED = 'HOSTS_FETCH_FAILED';
export const HOSTS_SEARCH = 'HOSTS_SEARCH';
export const HOSTS_SEARCH_SUCCESS = 'HOSTS_SEARCH_SUCCESS';
export const HOSTS_SEARCH_FAILED = 'HOSTS_SEARCH_FAILED';
export const LIKES_FETCH = 'LIKES_FETCH';
export const LIKES_FETCH_SUCCESS = 'LIKES_FETCH_SUCCESS';
export const LIKES_FETCH_FAILED = 'LIKES_FETCH_FAILED';
export const HOST_LIKE = 'HOST_LIKE';
export const HOST_LIKE_SUCCESS = 'HOST_LIKE_SUCCESS';
export const HOST_LIKE_FAILED = 'HOST_LIKE_FAILED';
export const HOST_DISLIKE = 'HOST_DISLIKE';
export const HOST_DISLIKE_SUCCESS = 'HOST_DISLIKE_SUCCESS';
export const HOST_DISLIKE_FAILED = 'HOST_DISLIKE_FAILED';
export const HOST_COMMENT = 'HOST_COMMENT';
export const HOST_COMMENT_SUCCESS = 'HOST_COMMENT_SUCCESS';
export const HOST_COMMENT_FAILED = 'HOST_COMMENT_FAILED';

export const hostDelete = id => ({
    type: HOST_DELETE,
    id
});

export const hostDeleteSuccess = result => ({
    type: HOST_DELETE_SUCCESS,
    result
});

export const hostDeleteFailed = error => ({
    type: HOST_DELETE_FAILED,
    error
});

export const hostsFetch = () => ({
    type: HOSTS_FETCH
});

export const hostsFetchSuccess = hosts => ({
    type: HOSTS_FETCH_SUCCESS,
    hosts
});
export const hostsFetchFailed = error => ({
    type: HOSTS_FETCH_FAILED,
    error
});

export const hostsSearch = params => ({
    type: HOSTS_SEARCH,
    params
});

export const hostsSearchSuccess = result => ({
    type: HOSTS_SEARCH_SUCCESS,
    result
});

export const hostsSearchFailed = error => ({
    type: HOSTS_SEARCH_FAILED,
    error
});

export const likesFetch = category => ({
    type: LIKES_FETCH,
    category
});

export const likesFetchSuccess = likes => ({
    type: LIKES_FETCH_SUCCESS,
    likes
});

export const likesFetchFailed = error => ({
    type: LIKES_FETCH_FAILED,
    error
});

export const hostLike = host_id => ({
    type: HOST_LIKE,
    host_id
});

export const hostLikeSuccess = like => ({
    type: HOST_LIKE_SUCCESS,
    like
});

export const hostLikeFailed = error => ({
    type: HOST_LIKE_FAILED,
    error
});

export const hostDislike = host_id => ({
    type: HOST_DISLIKE,
    host_id
});

export const hostDislikeSuccess = dislike => ({
    type: HOST_DISLIKE_SUCCESS,
    dislike
});

export const hostDislikeFailed = error => ({
    type: HOST_DISLIKE_FAILED,
    error
});

export const hostComment = comment => ({
    type: HOST_COMMENT,
    comment
});

export const hostCommentSuccess = host => ({
    type: HOST_COMMENT_SUCCESS,
    host
});

export const hostCommentFailed = error => ({
    type: HOST_COMMENT_FAILED,
    error
});