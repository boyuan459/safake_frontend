import { Map } from 'immutable';
import * as actions from './actions';

const initState = new Map({
    view: actions.getView(window.innerWidth),
    height: window.innerHeight,
});

export default function appReducer(state = initState, action) {
    switch(action.type) {
        default:
            return state;
    }
}