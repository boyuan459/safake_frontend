export function getView(width) {
    let newView = 'Mobile';
    if (width > 1220) {
        newView = 'Desktop';
    } else if (width > 767) {
        newView = 'Tab';
    }
    return newView;
}
