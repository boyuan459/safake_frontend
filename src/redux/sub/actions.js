export const SUBS_READ = 'SUBS_READ';

export const subsRead = subs => ({
    type: SUBS_READ,
    subs
});