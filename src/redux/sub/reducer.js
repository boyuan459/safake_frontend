import { Map, List } from 'immutable';
import * as actions from './actions'

const initState = new Map({
    read: Map(),
});

export default function subReducer(state = initState, action) {
    switch(action.type) {
        case actions.SUBS_READ:
            const read = {};
            // console.log("Read Subs", action.subs);
            action.subs.forEach(item => {
                // console.log("read item", item);
                read[item.node] = item.id;
            });
            // console.log("Finish read", read);
            return state.set("read", state.get("read").merge(read));
        default:
            return state;
    }
}