import { Map, List } from 'immutable';
import * as Actions from './actions';

const initState = new Map({
    favorites: List(),
    loading: false,
    error: null
});

export default function favoriteReducer(state = initState, action) {
    switch(action.type) {
        case Actions.FAVORITE_FETCH:
            return state.set('loading', true);
        case Actions.FAVORITE_FETCH_SUCCESS:
            return state.set('favorites', new List(action.favorites))
                    .set('loading', false);
        case Actions.FAVORITE_FETCH_FAILED:
            return state.set('error', action.error)
                    .set('loading', false);
        case Actions.FAVORITE_CREATE:
            return state.set('loading', true);
        case Actions.FAVORITE_CREATE_SUCCESS:
            return state.set('loading', false)
                .set('favorites', state.get('favorites').push(action.favorite));
        case Actions.FAVORITE_CREATE_FAILED:
            return state.set('loading', false)
                    .set('error', action.error);
        case Actions.FAVORITE_DESTROY:
            return state.set('loading', true);
        case Actions.FAVORITE_DESTROY_SUCCESS:
            const favorites = state.get('favorites').filter(item => item._id != action.favorite.host_id);
            return state.set('favorites', favorites)
                .set('loading', false);
        case Actions.FAVORITE_DESTROY_FAILED:
            return state.set('loading', false)
                .set('error', action.error);
        default:
            return state;
    }
}