export const FAVORITE_FETCH = 'FAVORITE_FETCH';
export const FAVORITE_FETCH_SUCCESS = 'FAVORITE_FETCH_SUCCESS';
export const FAVORITE_FETCH_FAILED = 'FAVORITE_FETCH_FAILED';
export const FAVORITE_CREATE = 'FAVORITE_CREATE';
export const FAVORITE_CREATE_SUCCESS = 'FAVORITE_CREATE_SUCCESS';
export const FAVORITE_CREATE_FAILED = 'FAVORITE_CREATE_FAILED';
export const FAVORITE_DESTROY = 'FAVORITE_DESTROY';
export const FAVORITE_DESTROY_SUCCESS = 'FAVORITE_DESTROY_SUCCESS';
export const FAVORITE_DESTROY_FAILED = 'FAVORITE_DESTROY_FAILED';

export const favoriteFetch = () => ({
    type: FAVORITE_FETCH
});

export const favoriteFetchSuccess = favorites => ({
    type: FAVORITE_FETCH_SUCCESS,
    favorites
});

export const favoriteFetchFailed = error => ({
    type: FAVORITE_FETCH_FAILED,
    error
});

export const favoriteCreate = (host_id) => ({
    type: FAVORITE_CREATE,
    host_id
});

export const favoriteCreateSuccess = (favorite) => ({
    type: FAVORITE_CREATE_SUCCESS,
    favorite
});

export const favoriteCreateFailed = (error) => ({
    type: FAVORITE_CREATE_FAILED,
    error
});

export const favoriteDestroy = (host_id) => ({
    type: FAVORITE_DESTROY,
    host_id
});

export const favoriteDestroySuccess = (favorite) => ({
    type: FAVORITE_DESTROY_SUCCESS,
    favorite
});

export const favoriteDestroyFailed = (error) => ({
    type: FAVORITE_DESTROY_FAILED,
    error
});