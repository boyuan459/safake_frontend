import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import * as actions from './actions';
import { createFavorite, unfavorite, getFavorites } from '../../services/favorite';

export function* handleFavoriteFetch() {
    yield takeEvery(actions.FAVORITE_FETCH, function*(action) {
        try {
            const favorites = yield call(getFavorites);
            yield put(actions.favoriteFetchSuccess(favorites));
        } catch(error) {
            yield put(actions.favoriteFetchFailed(error));
        }
    });
}

export function* handleFavorite() {
    yield takeEvery(actions.FAVORITE_CREATE, function*(action) {
        try {
            const favorite = yield call(createFavorite, action.host_id);
            yield put(actions.favoriteCreateSuccess(favorite));
        } catch(error) {
            yield put(actions.favoriteCreateFailed(error));
        }
    });
}

export function* handleUnfavorite() {
    yield takeEvery(actions.FAVORITE_DESTROY, function*(action) {
        try {
            const favorite = yield call(unfavorite, action.host_id);
            yield put(actions.favoriteDestroySuccess(favorite));
        } catch(error) {
            yield put(actions.favoriteDestroyFailed(error));
        }
    });
}

export default function* rootSaga() {
    yield all([
        fork(handleFavoriteFetch),
        fork(handleFavorite),
        fork(handleUnfavorite)
    ]);
}