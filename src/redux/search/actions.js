export const CITY_CHANGE = 'CITY_CHANGE';
export const SORT_CHANGE = 'SORT_CHANGE';

export const cityChange = city => ({
    type: CITY_CHANGE,
    city
});

export const sortChange = sort => ({
    type: SORT_CHANGE,
    sort
});