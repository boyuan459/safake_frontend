import { Map } from 'immutable';
import * as actions from './actions';

const initState = new Map({
    city: '',
    sort: 'date-desc'
})

export default function searchReducer(state = initState, action) {
    switch(action.type) {
        case actions.CITY_CHANGE:
            return state.set('city', action.city);
        case actions.SORT_CHANGE:
            return state.set('sort', action.sort);
        default:
            return state;
    }
}