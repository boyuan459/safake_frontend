import App from './app/reducer';
import Auth from './auth/reducer';
import Hosts from './host/reducer';
import Search from './search/reducer';
import Sub from './sub/reducer';
import Roster from './roster/reducer';
import Favorites from './favorite/reducer';

export default {
    App,
    Auth,
    Hosts,
    Search,
    Sub,
    Roster,
    Favorites
};