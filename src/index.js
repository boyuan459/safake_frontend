import React from 'react';
import ReactDOM from 'react-dom';
import { LocaleProvider } from 'antd';
import { IntlProvider } from 'react-intl';
// import { ThemeProvider } from 'styled-components';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { getLocale } from './services/locale';
import AppLocale from './languageProvider';
import themes from './config/themes';
import 'safake-bootstrap';
import './App.css';
import DashApp from './dashApp';
import registerServiceWorker from './registerServiceWorker';
// import 'react-virtualized/styles.css';
window.Strophe = require('./strophe.js');

const locale = getLocale();
const currentAppLocale = AppLocale[locale] || AppLocale.en;

ReactDOM.render(
    <LocaleProvider locale={currentAppLocale.antd}>
        <IntlProvider locale={currentAppLocale.locale} messages={currentAppLocale.messages}>
            <MuiThemeProvider theme={themes.theme}>
                <DashApp />
            </MuiThemeProvider>
        </IntlProvider>
    </LocaleProvider>, 
    document.getElementById('root'));
registerServiceWorker();
