import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/ChevronLeft';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import red from '@material-ui/core/colors/red';
import CardContent from '@material-ui/core/CardContent';
import Slide from '@material-ui/core/Slide';
import Grid from '@material-ui/core/Grid';
import Carousel from '../../component/Carousel';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {List} from 'antd';
import Comment from '../../container/Comment';
import CommentItem from '../List/CommentItem';
import MessageDialog from '../Message';
import { connect } from 'react-redux';
import { getRoster, getFavorites } from '../../selectors';
import * as Constants from '../../constant/xmpp';
import * as rosterActions from '../../redux/roster/actions';
import * as favoriteActions from '../../redux/favorite/actions';
import * as hostActions from '../../redux/host/actions';

const styles = {
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
        textAlign: 'center',
    },
    card: {
        minWidth: 275,
    },
    avatar: {
        backgroundColor: red[500],
    },
    commentPanel: {
        padding: 0,
        boxShadow: 'none',
        '&:before': {
            height: 0,
        },
    },
    expanded: {
        margin: 5,
    },
    commentArea: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
    },
    commentList: {
        flex: 1,
        paddingBottom: 100,
        overflow: 'scroll',
    },
};

export function Transition(props) {
    return <Slide direction="left" {...props} />;
}

class HostDetail extends Component {
    state = {
        anchorEl: null,
        messageOpen: false,
        commentOpen: false,
        comments: [],
        expanded: false,
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.host != prevProps.host) {
            this.setState({
                comments: [],
                expanded: false,
            })
        }
    }
    handleMenu = event => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleMenuClose = () => {
        this.setState({
            anchorEl: null
        });
    }

    handleSendMessageOpen = () => {
        const { roster, host, createRoster } = this.props;
        this.setState({
            messageOpen: true,
            anchorEl: null,
        });
        //check if in roster, not add in roster
        const to = host.user.mid + '@' + Constants.Message.Domain;
        let found = roster.find(item => item.jid === to || item.jid === host.user.mid);
        if (!found) {
            //add to roster
            console.log("Add to roster", to);
            createRoster({user: host.user.mid, usernick: host.user.name, group: host.category});
        }
    }

    handleSendMessageClose = () => {
        this.setState({
            messageOpen: false
        })
    }

    handleFavorite = () => {
        const { host, favorite } = this.props;
        const host_id = host._id;
        console.log("Favorite");
        favorite(host_id);
        this.setState({
            anchorEl: null,
        })
    }

    handleUnfavorite = () => {
        const { host, unfavorite } = this.props;
        const host_id = host._id;
        unfavorite(host_id);
        this.setState({
            anchorEl: null,
        })
    }

    handleCommentShow = () => {
        this.setState({
            commentOpen: true,
            anchorEl: null,
        })
    }

    handleCommentClose = () => {
        this.setState({
            commentOpen: false
        })
    }

    handleComment = (comment) => {
        console.log(comment);
        const { host, commentHost, auth } = this.props;
        commentHost({host_id: host._id, comment});
        const commentObj = {
            user_id: auth.user.id,
            user_name: auth.user.name,
            user_avatar: auth.user.avatar,
            comment: comment
        };
        const { comments } = this.state;
        comments.push(commentObj)
        this.setState({
            commentOpen: false,
            comments
        })
    }

    toggleComments = (event, expanded) => {
        this.setState({
            expanded
        })
    }
    
    render() {
        const { detailOpen, handleDetailClose, host, classes, user, auth, favorites } = this.props;
        const { anchorEl, messageOpen } = this.state;

        const open = Boolean(anchorEl);
        const anchorOrigin = {
            vertical: 'top',
            horizontal: 'right',
        };

        var favorited = favorites.find(item => item._id === host._id);

        const comments = host.comments ? host.comments.concat(this.state.comments) : this.state.comments;

        return (
            <Dialog
                fullScreen
                open={detailOpen}
                onClose={handleDetailClose}
                TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton color="inherit" onClick={handleDetailClose}>
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            Detail
                        </Typography>
                        {
                            auth.user != null ? 
                                <div>
                                    <IconButton 
                                        aria-owns={open ? 'menu-appbar' : null}
                                        aria-haspopup="true"
                                        onClick={this.handleMenu}
                                        color="inherit">
                                        <MoreVertIcon />
                                    </IconButton>
                                    <Menu
                                        id="menu-appbar"
                                        anchorEl={anchorEl}
                                        anchorOrigin={anchorOrigin}
                                        transformOrigin={anchorOrigin}
                                        open={open}
                                        onClose={this.handleMenuClose}>
                                        {/* <MenuItem onClick={this.handleMenuClose}>Share</MenuItem> */}
                                        {
                                            favorited ? 
                                            <MenuItem onClick={this.handleUnfavorite}>Unfavorite</MenuItem>
                                            :
                                            <MenuItem onClick={this.handleFavorite}>Favorite</MenuItem>
                                        }
                                        {
                                            (host.user && auth.user.id != host.user.id) && (
                                                <MenuItem onClick={this.handleCommentShow}>Comment</MenuItem>
                                            )
                                        }
                                        {
                                            (host.user && auth.user.id != host.user.id) && (
                                                <MenuItem onClick={this.handleSendMessageOpen}>Leave Message</MenuItem>
                                            )
                                        }
                                    </Menu>
                                </div>
                                : <IconButton />
                        }
                    </Toolbar>
                </AppBar>
                {
                    host.images ?
                        <Carousel images={host.images} />
                        : null
                }
                <div className={classes.card}>
                    {
                        user ? 
                        null
                        :
                        <CardHeader
                            avatar={
                                host.user && host.user.avatar ?
                                    <Avatar aria-label="Recipe" className={classes.avatar} src={host.user.avatar} />
                                    :
                                    <Avatar aria-label="Recipe" className={classes.avatar}>
                                        {host.user ? host.user.name.substr(0, 1).toUpperCase() : ''}
                                    </Avatar>
                            }
                            title={host.user.name}
                            subheader={host.updated_at}
                        />
                    }
                    
                    <CardContent>
                        <Typography variant="headline" component="h2">
                            {host.highlight}
                        </Typography>
                        <Typography gutterBottom component="p">
                            {host.detail}
                        </Typography>
                        <Grid container>
                            <Grid item xs={6}>
                                <Typography>Price</Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography color="secondary">{host.price.number} {host.price.currency}</Typography>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={6}>
                                <Typography>Contact No.</Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography>{host.contact}</Typography>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={6}>
                                <Typography>Address</Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography>{`${host.address.city} ${host.address.state}`}</Typography>
                            </Grid>
                        </Grid>
                        {
                            host.maxPersons ?
                                <Grid container>
                                    <Grid item xs={6}>
                                        <Typography>Allowed Persons</Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography>{host.maxPersons}</Typography>
                                    </Grid>
                                </Grid>
                                : null
                        }
                        {
                            host.startDate ?
                                <Grid container>
                                    <Grid item xs={6}>
                                        <Typography>Start Date</Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography>{host.startDate}</Typography>
                                    </Grid>
                                </Grid>
                                : null
                        }
                        {
                            host.endDate ?
                                <Grid container>
                                    <Grid item xs={6}>
                                        <Typography>End Date</Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography>{host.endDate}</Typography>
                                    </Grid>
                                </Grid>
                                : null
                        }
                    </CardContent>
                    
                </div>
                <ExpansionPanel className={classnames(classes.commentPanel, classes.expanded)} onChange={this.toggleComments}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Comments</Typography>
                    </ExpansionPanelSummary>
                </ExpansionPanel>
                <div className={classes.commentArea}>
                            <div className={classes.commentList}>
                            {
                                this.state.expanded ? 
                                <List
                                    dataSource={comments}
                                    renderItem={item => (
                                        <List.Item>
                                            <CommentItem comment={item} />
                                        </List.Item>
                                    )}>
                            </List>
                            : null
                            }
                            </div>
                        </div>
                <MessageDialog open={messageOpen} host={host} auth={auth}
                    handleClose={this.handleSendMessageClose}/>
                <Comment open={this.state.commentOpen} handleClose={this.handleCommentClose} handleComment={this.handleComment} />
            </Dialog>
        );
    }
}

function mapStateToProps(state) {
    return {
        auth: state.Auth.toJS(),
        roster: getRoster(state),
        favorites: getFavorites(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        createRoster(item) {
            dispatch(rosterActions.rosterCreate(item));
        },
        favorite(host_id) {
            dispatch(favoriteActions.favoriteCreate(host_id));
        },
        unfavorite(host_id) {
            dispatch(favoriteActions.favoriteDestroy(host_id));
        },
        commentHost(comment) {
            dispatch(hostActions.hostComment(comment));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(HostDetail));