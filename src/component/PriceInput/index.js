import React, { Component } from 'react';
import { Input, Select } from 'antd';
const Option = Select.Option;

/**
 * Usage: <PriceInput size={default} currencies={[{'value': 'rmb', 'label': 'RMB'}]}
 */
class PriceInput extends Component {
    constructor(props) {
        super(props);
        const value = props.value || {};
        this.state = {
            number: value.number || 0,
            currency: value.currency || 'rmb',
        };
    }

    componentWillReceiveProps(nextProps) {
        if ('value' in nextProps) {
            const value = nextProps.value;
            this.setState(value);
        }
    }

    handleNumberChange = e => {
        const number = parseInt(e.target.value || 0, 10);
        if (isNaN(number)) {
            return;
        }
        if (!('value' in this.props)) {
            this.setState({ number });
        }
        this.triggerChange({ number });
    }

    handleCurrencyChange = currency => {
        if (!('value' in this.props)) {
            this.setState({ currency });
        }
        this.triggerChange({ currency });
    }

    triggerChange = changedValue => {
        // should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(Object.assign({}, this.state, changedValue));
        }
    }

    render() {
        const defaultCurrencies = [
            {value: 'rmb', label: 'RMB'},
            {value: 'usd', label: 'USD'},
            {value: 'aud', label: 'AUD'},
        ];
        const { size = 'default', currencies = defaultCurrencies } = this.props;
        const state = this.state;
        return (
            <span>
                <Input 
                    type="text"
                    size={size}
                    value={state.number}
                    onChange={this.handleNumberChange}
                    style={{ width: '65%', marginRight: '3%'}}/>
                <Select
                    value={state.currency}
                    size={size}
                    style={{ width: '32%'}}
                    onChange={this.handleCurrencyChange}>
                    {
                        currencies.map(currency => <Option key={currency.value}>{currency.label}</Option>)
                    }
                </Select>
            </span>
        );
    }
}

export default PriceInput;