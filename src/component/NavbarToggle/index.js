import React, { Component } from 'react';
import NavbarToggleHolder from './navbarToggle.style';
import {Navbar} from 'react-bootstrap';
import { Icon } from 'antd';

class NavbarToggle extends Component {
    state = {
        spin: false
    };
    handleClick = e => {
        this.setState({
            spin: true
        });
        setTimeout(() => {
            this.setState({
                spin: false
            })
        }, 150);
    }
    render() {
        const { collapsed } = this.props;
        const { spin } = this.state;
        
        return (
            <Navbar.Toggle onClick={this.handleClick} style={{ margin: 0, padding: 0, backgroundColor: 'transparent', borderColor: 'transparent' }}>
                <Icon spin={spin} type={collapsed ? "down" : "up"} style={{ fontSize: 16, color: '#ccc' }} />
            </Navbar.Toggle>
        );
    }
}

export default NavbarToggle;