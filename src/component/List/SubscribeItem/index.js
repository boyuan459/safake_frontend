import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Collapse from '@material-ui/core/Collapse';
import red from '@material-ui/core/colors/red';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import HistoryIcon from '@material-ui/icons/History';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Link } from 'react-router-dom';
import moment from 'moment';

const styles = theme => ({
    card: {
        maxWidth: 400,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
        backgroundSize: 'contain',
    },
    actions: {
        display: 'flex',
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
});

class Item extends React.Component {
    state = { expanded: false };

    handleExpandClick = () => {
        this.setState({ expanded: !this.state.expanded });
    };

    handleClick = event => {
        const { onClick, data } = this.props;
        onClick(data);
    }

    handleFavorite = event => {
        event.preventDefault();
        console.log('share');
    }

    handleShare = event => {
        event.preventDefault();
    }

    handleHistorySub = () => {
        const { data, handleHistory } = this.props;
        console.log("get history subs", data);
        handleHistory(data);
    }

    render() {
        const { classes, data } = this.props;

        return (
            <div>
                <Card className={classes.card}>
                    <div onClick={this.handleClick}>
                        <CardHeader
                            avatar={
                                data.avatar ?
                                    <Avatar aria-label="Recipe" className={classes.avatar} src={data.avatar} />
                                    :
                                    <Avatar aria-label="Recipe" className={classes.avatar}>
                                        {data.name.substr(0, 1).toUpperCase()}
                                    </Avatar>
                            }
                            title={data.name}
                            subheader={data.timestamp ? moment(data.timestamp).fromNow() : ''}
                        />
                        {
                            data.image ? 
                            <CardMedia
                            className={classes.media}
                            image={data.image ? data.image : null}
                            title={data.image ? data.name : ''}
                            />
                            : null
                        }
                        
                        <CardContent>
                            <Typography gutterBottom variant="headline" component="h2">
                                {data.subject}
                            </Typography>
                            <Typography component="p">
                                {data.detail}
                            </Typography>
                        </CardContent>
                    </div>
                    <CardActions className={classes.actions} disableActionSpacing>
                        <IconButton aria-label="Add to favorites" onClick={this.handleFavorite}>
                            <FavoriteIcon />
                        </IconButton>
                        <IconButton aria-label="Share" onClick={this.handleShare}>
                            <ShareIcon />
                        </IconButton>
                        {
                            data.history ? 
                            null
                            :
                            <IconButton aria-label="History" onClick={this.handleHistorySub}>
                                <HistoryIcon />
                            </IconButton>
                        }
                        <IconButton
                            className={classnames(classes.expand, {
                                [classes.expandOpen]: this.state.expanded
                            })}
                            onClick={this.handleExpandClick}
                            aria-expanded={this.state.expanded}
                            aria-label="Show more">
                            <ExpandMoreIcon />
                        </IconButton>
                    </CardActions>
                    <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
                        <CardContent>
                        <Typography component="p">
                            {data.detail}
                        </Typography>
                        </CardContent>
                    </Collapse>
                </Card>
            </div>
        );
    }
}

Item.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Item);
