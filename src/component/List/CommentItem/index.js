import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import moment from 'moment';

const styles = theme => ({
  card: {
    maxWidth: 400,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
    backgroundSize: 'contain',
  },
  content: {
    paddingBottom: '0 !important',
  },
  actions: {
    display: 'flex',
    justifyContent: 'space-around',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  button: {
    margin: theme.spacing.unit,
    textTransform: 'none',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  }
});

class CommentItem extends React.Component {
    
  render() {
    const { classes, comment } = this.props;
    return (
        <div className={classes.card}>
            <CardHeader
                avatar={
                    comment.user_avatar ?
                    <Avatar aria-label="Recipe" className={classes.avatar} src={comment.user_avatar} />
                    :
                    <Avatar aria-label="Recipe" className={classes.avatar}>
                        {comment.user_name && comment.user_name.substr(0, 1).toUpperCase()}
                    </Avatar>
                }
                title={comment.user_name}
                subheader={comment.updated_at ? (comment.updated_at.$date ? moment(parseInt(comment.updated_at.$date.$numberLong)).fromNow() : moment(comment.updated_at).fromNow()) : moment().fromNow()}
                />
            <CardContent className={classes.content}>
              <Typography component="p">
                {comment.comment}
              </Typography>
            </CardContent>
        </div>
    );
  }
}

CommentItem.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CommentItem);
