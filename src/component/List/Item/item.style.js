import styled from 'styled-components';

const ItemWrapper = styled.div`
a {
    text-decoration: none;
}

.list-item {
  display: flex;
  flex-direction: row;
  padding: 10px 10px;
  background: #fff;
  border-bottom: 1px solid #e1e1e1;

  .item-img-container {
    width: 120px;
    height: 90px;

    img {
      width: 100%;
      height: 100%;
    }
  }

  .item-content {
    flex: 1;
    padding-left: 20px;

    .item-title-container {
      width: 100%;
      height: 25px;

      h3 {
        margin: 0;
        padding: 0;
        font-size: 18px;
        line-height: 1;
        color: #333;
      }

      span {
        font-size: 13px;
        line-height: 18px;
        color: #999;
      }
    }

    .item-sub-title {
      font-size: 14px;
      color: #666;
      line-height: 1.5;
    }

    .item-price-container {
      width: 100%;
      margin-top: 10px;

      .price {
        font-size: 18px;
        line-height: 1;
        font-weight: 900;
        color: rgb(233, 32, 61);
      }

      .member {
          font-size: 13px;
          line-height: 18px;
          color: #999;
      }
    }
  }
}
`;

export default ItemWrapper;