import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import LikeIcon from '@material-ui/icons/ThumbUp';
import CommentIcon from '@material-ui/icons/Comment';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';

const styles = theme => ({
  card: {
    maxWidth: 400,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
    backgroundSize: 'contain',
  },
  content: {
    paddingBottom: '0 !important',
  },
  actions: {
    display: 'flex',
    justifyContent: 'space-around',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  button: {
    margin: theme.spacing.unit,
    textTransform: 'none',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  }
});

class Item extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      expand: false,
      favorited: this.getFavorited(props.favorites, props.data),
      liked: this.getLiked(props.likes, props.data),
      likes: props.data.likes ? props.data.likes : 0,
      comments: props.data.comments ? props.data.comments.length : 0,
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.favorites != this.props.favorites || prevProps.likes != this.props.likes || prevProps.data.comments != this.props.data.comments) {
      this.setState({
        favorited: this.getFavorited(this.props.favorites, this.props.data),
        liked: this.getLiked(this.props.likes, this.props.data),
        comments: this.props.data.comments ? this.props.data.comments.length : 0,
      })
    }
  }

  getLiked = (likes, data) => {
    const liked = likes && likes.length ? likes.find(item => item.host_id === data._id) : undefined;
    return !!liked;
  }

  getFavorited = (favorites, data) => {
    const favorited = favorites.find(item => item._id === data._id);
    return !!favorited;
  }

  handleExpandClick = () => {
    this.setState({ expanded: !this.state.expanded });
  };

  handleClick = event => {
    const { onClick, data } = this.props;
    onClick(data);
  }

  handleFavorite = event => {
    const { data, handleFavorite, isLoggedIn, history } = this.props;
    if (!isLoggedIn) {
      console.log("login");
      history.push('/login');
      return;
    }
    event.preventDefault();
    console.log('favorite');
    handleFavorite(data._id);
    this.setState({
      favorited: !this.state.favorited
    })
  }

  handleUnfavorite = event => {
    event.preventDefault();
    const { data, handleUnfavorite } = this.props;
    handleUnfavorite(data._id);
    this.setState({
      favorited: !this.state.favorited
    })
  }

  handleLike = event => {
    event.preventDefault();
    const { data, handleLike, isLoggedIn, history } = this.props;
    if (!isLoggedIn) {
      history.push('/login');
      return;
    }
    handleLike(data._id);
    this.setState({
      liked: !this.state.liked,
      likes: this.state.likes + 1,
    })
  }

  handleDislike = event => {
    event.preventDefault();
    const { data, handleDislike } = this.props;
    handleDislike(data._id);
    this.setState({
      liked: !this.state.liked,
      likes: this.state.likes - 1,
    })
  }

  handleComment = event => {
    event.preventDefault();
    const { data, handleComment, isLoggedIn, history } = this.props;
    if (!isLoggedIn) {
      history.push('/login');
      return;
    }
    handleComment(data);
  }

  handleShare = event => {
    event.preventDefault();
  }

  render() {
    const { classes, data, favorites, isLoggedIn } = this.props;

    const { favorited, liked, likes, comments } = this.state;

    return (
      <div>
        <Card className={classes.card}>
          <div onClick={this.handleClick}>
            <CardHeader
              avatar={
                data.user && data.user.avatar ?
                  <Avatar aria-label="Recipe" className={classes.avatar} src={data.user.avatar} />
                  :
                  <Avatar aria-label="Recipe" className={classes.avatar}>
                    {data.user && data.user.name.substr(0, 1).toUpperCase()}
                  </Avatar>
              }
              title={data.user && data.user.name}
              subheader={data.updated_at}
            />
            <CardMedia
              className={classes.media}
              image={data.images ? data.images[0].url : null}
              title={data.images ? data.images[0].name : ''}
            />
            <CardContent className={classes.content}>
              <Typography component="p">
                {data.highlight}
              </Typography>
              <Typography component="p">
                {likes > 0 ? `${likes} Likes` : ''}&nbsp;&nbsp;&nbsp;{ comments > 0 ? `${comments} Comments` : '' }
              </Typography>
            </CardContent>
          </div>
          <CardActions className={classes.actions} disableActionSpacing>
          {
              favorited ?
                <Button aria-label="Remove from favorites" onClick={this.handleUnfavorite} className={classes.button}>
                  <FavoriteIcon color="secondary" className={classnames(classes.leftIcon, classes.iconSmall)} />Favorite
              </Button>
                :
                <Button aria-label="Add to favorites" onClick={this.handleFavorite} className={classes.button}>
                  <FavoriteIcon className={classnames(classes.leftIcon, classes.iconSmall)} />Favorite
              </Button>
            }
            {
              liked ?
                <Button aria-label="Like" onClick={this.handleDislike} className={classes.button}>
                  <LikeIcon color="secondary" className={classnames(classes.leftIcon, classes.iconSmall)} />Like
              </Button>
                :
                <Button aria-label="Like" onClick={this.handleLike} className={classes.button}>
                  <LikeIcon className={classnames(classes.leftIcon, classes.iconSmall)} />Like
              </Button>
            }

            <Button aria-label="Comment" onClick={this.handleComment} className={classes.button}>
              <CommentIcon className={classnames(classes.leftIcon, classes.iconSmall)} />Comment
              </Button>

            {/* <IconButton aria-label="Share" onClick={this.handleShare}>
                <ShareIcon />
              </IconButton> */}
          </CardActions>
        </Card>
      </div>
    );
  }
}

Item.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(Item));
