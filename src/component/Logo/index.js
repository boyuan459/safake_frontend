import React, { Component } from 'react';
import logo from '../../logo.svg';
import './logo.css';

class Logo extends Component {
    render() {
        return (
            <img alt="Safake" src={logo} className="app-logo"/>
        );
    }
}

export default Logo;