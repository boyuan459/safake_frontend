import styled from 'styled-components';

const CarouselWrapper = styled.div`
  background-color: #ffffff;
  position: relative;
  padding: 0;
  margin: 0;

  .carousel-item {
    width: 100%;
    height: 200px;
    padding: 0;
    margin: 0;
  }

  .indicator-container {
    bottom: 5px;
    position: absolute;
    display: block;
    width: 100%;

    ul {
      width: 100%;
      height: auto;
      text-align: center;
      padding: 0;

      li {
        list-style: none;
        display: inline-block;
        height: 10px;
        width: 10px;
        border-radius: 50%;
        background-color: #fff;
        margin: 0 3px;
      }

      li.selected {
        background-color: rgba(233, 32, 61, 0.8);
      }
    }
  }
`;

export default CarouselWrapper;