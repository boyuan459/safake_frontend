import React, { Component } from 'react'
import ReactSwipe from 'react-swipe'
import CarouselWrapper from './carousel.style';

class Carousel extends Component {

  constructor(props) {
    super(props)
    this.state = {
      index: 0
    }
  }

  render() {
    let images = this.props.images
    let options = {
      auto: 2000,
      callback: function(index) {
        this.setState({
          index: index
        })
      }.bind(this)
    }

    return (
      <CarouselWrapper>
        <ReactSwipe key={images.length} className="carousel" swipeOptions={options}>
          {
            images.map((image, i) => {
              return (
                <img key={'image' + i} className="carousel-item" src={image.url} />
              )
            })
          }
        </ReactSwipe>
        <div className="indicator-container">
          <ul>
            {
              images.map((image, i) => {
                return (
                  <li key={i} className={this.state.index % images.length === i ? 'selected' : ''}></li>
                )
              })
            }
          </ul>
        </div>
      </CarouselWrapper>
      )
  }
}

export default Carousel