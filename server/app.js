'use strict';
const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const appDir = path.resolve(__dirname, "../build");

app.use(express.static(path.join(__dirname, '../build')));

app.get('*', function(req, res) {
    res.sendFile(path.resolve(appDir, "index.html"));
});

server.listen(8080, () => {
    console.log("Server up on port 8080!");
});

io.on('connection', function(socket) {
    socket.on('chat.message', function(message) {
        console.log('New Message: ' + message);
        io.emit('chat.message', message);
    });
});
