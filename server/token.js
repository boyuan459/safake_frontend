const hmacSHA256 = require('crypto-js/hmac-sha256');
const Base64 = require('crypto-js/enc-base64');
const uuidv4 = require('uuid/v4');

let _appId = "/FeLy5y5hsYb5HzsQ2AwpXmD/CSpZOY0gT7AtNa7sFIVUXR/6MQ9t+/fe7qy6wTpEcVHaLnreA3tf26va8tV9UNiSq47ARAhAI+IvLcBRTZ8fkF42FLSvJMGOtL8T2b/uAjq2baEq+M5/MA/9JAj6uxiRvqAsfEjNmsBDTd91L6iicu+PDgAzY2oUXsvxeqYzi4X7fCZJn9OaZkYkJH6ttQMbQKEFbQM4iKqUW0vLlo8M/wnW4Oe+yeu";
let _apiKey = "nE1XSb/ZR/HA3VQXVBYKPGUqVMPkQk3L0AfkH8kHQxGN85OzInhLjPGwA9Aaogdm+28rU7MYV0DsjBG5Cj7KAzD4lna4N6rp2tMNGHixiWBU8An2v0OC0ltt73tVsZUxpV2AcdScP2hRSDSEeac9zKjIY9Bx0cuIcIVmVAnrkNal2wuNFjVkDDkyFKPY/pOjJYhDYMW5jZvMcOApl97uT+bQIteQ1ZCkBzAwosPJYpRbm9tEsKubeknT";
let requestContentBase64String = '';
let time = new Date().getTime();
let nonce = uuidv4();
let encodedUrl = encodeURIComponent((decodeURIComponent('https://smg.ayudacloud.com/ayuda.bms.api/v2/activities')));
let httpMethod = 'GET';

let signatureRawData = _appId + httpMethod + encodedUrl + time + nonce + requestContentBase64String;

var secretKeyByteArray = new Buffer(_apiKey).toString('base64');

// console.log(hmacSHA256(signatureRawData, _apiKey));
var requestSignatureBase64String = Base64.stringify(hmacSHA256(signatureRawData, _apiKey));

var token = 'amx ' + _appId + ":" + requestSignatureBase64String + ":" + nonce + ":" + time;

console.log(token);
